rem スクリプト更新
git pull

rem 仮想環境実行
call venv\scripts\activate

rem ライブラリ構築
pip install --use-pep517 --upgrade -r requirements_sdp.txt

pause