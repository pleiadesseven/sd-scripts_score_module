rem 仮想環境作成
python -m venv venv

rem 仮想環境実行
call venv\scripts\activate

rem ライブラリ構築
pip install torch==2.0.1+cu118 torchvision==0.15.2+cu118 --index-url https://download.pytorch.org/whl/cu118
pip install --upgrade -r requirements.txt
pip install -e git+https://github.com/openai/CLIP.git@main#egg=clip
pip install lpips==0.1.4
rem Xformersを導入したい場合はtorchのバージョンに対応したものを入れる（動作確認してない
rem pip install xformers-0.0.20+6425fd0.d20230505-cp310-cp310-win_amd64.whl
rem 8bit Adamなどは導入する必要性を感じなかったので手動で

accelerate config

pause