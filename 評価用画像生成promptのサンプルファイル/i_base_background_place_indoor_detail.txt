# リビングルーム（明るい雰囲気）
Living Room (Bright Atmosphere)
# カフェスタイルのキッチン
Cafe-Style Kitchen
# モダンなオフィス空間
Modern Office Space
# ラグジュアリーな寝室
Luxurious Bedroom
# レトロなリーディングコーナー
Retro Reading Corner
# ミニマリストなダイニングエリア
Minimalist Dining Area
# アーティスティックなスタジオ
Artistic Studio
# ナチュラルなリゾートスパ
Natural Resort Spa
# シックな書斎
Chic Study Room
# オーシャンビューのバルコニー
Ocean View Balcony
# クラシックなホールウェイ
Classic Hallway
# 現代的なゲームルーム
Contemporary Game Room
# ボヘミアンなリビングスペース
Bohemian Living Space
# ファンタジーな子供部屋
Fantasy Kids' Room
# インダストリアルなロフト
Industrial Loft
# 和風ティールーム
Japanese-Style Tea Room
# アーバンスタイルのバー
Urban-Style Bar
# ヴィンテージなサロン
Vintage Salon
# プライベートシアタールーム
Private Theater Room
# パステルトーンのベビールーム
Pastel-Toned Nursery
# アクティブなフィットネススタジオ
Active Fitness Studio
# ロマンティックなキャンドルナイト
Romantic Candlelit Setting
# レトロなゲームアーケード
Retro Game Arcade
# グリーンインテリアのリビングルーム
Green Interior Living Room
# ミッドセンチュリーなダイニングスペース
Mid-Century Dining Space
# 親子で楽しむプレイルーム
Family Playroom
# モダンなホテルロビー
Modern Hotel Lobby
# ヨーロピアンスタイルのホール
European-Style Hall
# リラックスできるヨガスタジオ
Relaxing Yoga Studio
# ファンシーなバスルーム
Fancy Bathroom
# アーティスティックなギャラリースペース
Artistic Gallery Space
# ナチュラルなテラスガーデン
Natural Terrace Garden
# リフレッシュメントを楽しむバー
Refreshing Bar
# カジュアルなホームオフィス
Casual Home Office
# テクノロジー溢れるスマートルーム
Tech-Savvy Smart Room
# ボタニカルなベランダ
Botanical Balcony
# シンプルな寝室デザイン
Simple Bedroom Design
# クリエイティブなアートスタジオ
Creative Art Studio
# 落ち着いた図書室
Cozy Library
# アーバンスタイルのカフェ
Urban-Style Cafe
# エレガントなダイニングルーム
Elegant Dining Room
# テーマパーク風の子供部屋
Theme Park-Inspired Kids' Room
# ナチュラルな日光浴スペース
Natural Sunroom
# グラマラスなドレッシングルーム
Glamorous Dressing Room
# シンメトリーなホールウェイ
Symmetrical Hallway
# クラシックなスタディールーム
Classic Study Room
# フォトジェニックなバスルーム
Photogenic Bathroom
# アーティスティックなワークショップ
Artistic Workshop
# 現代的なリビングエリア
Contemporary Living Area
# プールサイドのリラックススペース
Poolside Relaxation Area
# ヴィンテージなカフェテラス
Vintage Cafe Terrace
# ホテルスタイルのスイートルーム
Hotel-Style Suite Room
# ボヘミアンな寝室
Bohemian Bedroom
# インダストリアルなカフェスペース
Industrial Cafe Space
# オープンプランのキッチンダイニング
Open-Plan Kitchen-Dining
# ナチュラルなワークアウトルーム
Natural Workout Room
# ロマンチックなベッドキャノピー
Romantic Bed Canopy
# ファンタジックなアートギャラリー
Fantastical Art Gallery
# フレンチスタイルのホール
French-Style Hall
# ミッドセンチュリーなリビングルーム
Mid-Century Living Room
# 家族で楽しむリビングスペース
Family-Friendly Living Space
# ヨーロピアンスタイルのリーディングルーム
European-Style Reading Room
# モダンなバーインテリア
Modern Bar Interior
# ラグジュアリーなリビングエリア
Luxurious Living Area
# 落ち着いたトロピカルスパ
Serene Tropical Spa
# アートファンのためのスタジオ
Studio for Art Enthusiasts
# ナチュラルなキッチンスペース
Natural Kitchen Space
# エコフレンドリーなリビングルーム
Eco-Friendly Living Room
# スポーツバーの雰囲気
Sports Bar Vibes
# ロマンティックなファイヤープレイス
Romantic Fireplace Setting
# モダンなワードローブルーム
Modern Wardrobe Room
# アーティスティックなショーケース
Artistic Showcase
# オーガニックなリビングスペース
Organic Living Space
# クラシックなホテルロビー
Classic Hotel Lobby
# ナチュラルなヨガスタジオ
Natural Yoga Studio
# インダストリアルなキッチンデザイン
Industrial Kitchen Design
# ファンタジーな子供のプレイエリア
Fantasy Children's Play Area
# モダンなパントリー
Modern Pantry
# ボヘミアンなリーディングスペース
Bohemian Reading Space
# アーティスティックなベッドルーム
Artistic Bedroom
# ナイトクラブスタイルのバー
Nightclub-Style Bar
# グリーンインテリアのオフィス
Green Interior Office
# ミッドセンチュリーなベッドルーム
Mid-Century Bedroom
# パリジェンヌスタイルの化粧室
Parisian-Style Vanity Area
# エレガントな屋外テラス
Elegant Outdoor Terrace
# リフレッシュメントを楽しむカフェ
Refreshing Cafe
# アーバンスタイルのジムスペース
Urban-Style Gym Area
# モダンなバスルームデザイン
Modern Bathroom Design
# ナチュラルなプライベートスパ
Natural Private Spa
# シンプルなワークフロースペース
Simple Work-Flow Area
# ヴィンテージなライブラリールーム
Vintage Library Room
# リラックスできるインドアガーデン
Relaxing Indoor Garden
# モダンなカフェスタイルのキッチン
Modern Cafe-Style Kitchen
# ラグジュアリーなオフィスインテリア
Luxurious Office Interior
# クラシックなリビングスペース
Classic Living Space
# ボヘミアンスタイルのパティオ
Bohemian-Style Patio
# インダストリアルなテレビルーム
Industrial TV Room
# ナチュラルなクラフトルーム
Natural Craft Room
# ロマンチックなカフェインテリア
Romantic Cafe Interior
# ファンタジックな寝室デザイン
Fantastical Bedroom Design
# アートファンのためのギャラリースペース
Gallery Space for Art Enthusiasts
# ミッドセンチュリーなオフィスデザイン
Mid-Century Office Design
# 家族で楽しむキッズスペース
Family-Friendly Kids' Space
# ヨーロピアンスタイルのダイニングルーム
European-Style Dining Room
# モダンなスパイスキッチン
Modern Spice Kitchen
# ラグジュアリーなリーディングノック
Luxurious Reading Nook
# 落ち着いたカフェテラス
Serene Cafe Terrace
# アーティスティックなホームスタジオ
Artistic Home Studio
# ナチュラルなフラワーショップ
Natural Flower Shop
# フレンチスタイルのリビングエリア
French-Style Living Area
# モダンなミーティングルーム
Modern Meeting Room
# ラグジュアリーなインテリアデザイン
Luxurious Interior Design
# アーバンスタイルのリラックススペース
Urban-Style Relaxation Space
# オーシャンビューのリビングルーム
Ocean View Living Room
# クラシックなキッチンスペース
Classic Kitchen Space
# ナチュラルなウェルネススタジオ
Natural Wellness Studio
# ロマンティックなダイニングエリア
Romantic Dining Area
# ファンタジックな書斎デザイン
Fantastical Study Room Design
# ヴィンテージなカフェインテリア
Vintage Cafe Interior
# グリーンインテリアのリビングスペース
Green Interior Living Space
# モダンなパントリールーム
Modern Pantry Room
# ラグジュアリーなバスルームデザイン
Luxurious Bathroom Design
# アーティスティックな屋内ガーデン
Artistic Indoor Garden
# オーガニックなキッチンデザイン
Organic Kitchen Design
# クラシックなリビングエリア
Classic Living Area
# ナチュラルなワークショップスペース
Natural Workshop Space
# ロマンティックなベッドルームデザイン
Romantic Bedroom Design
# ファンタジックなアートギャラリー
Fantastical Art Gallery
# ヴィンテージなバーカウンター
Vintage Bar Counter
# グリーンインテリアのオフィススペース
Green Interior Office Space
# ミッドセンチュリーなリビングルーム
Mid-Century Living Room
# パリジャンスタイルの子供部屋
Parisian-Style Kids' Room
# モダンなゲームルーム
Modern Game Room
# アーバンスタイルのリーディングスペース
Urban-Style Reading Space
# ナチュラルなホームスパ
Natural Home Spa
# シンプルなオフィスデザイン
Simple Office Design
# ヴィンテージなリビングスペース
Vintage Living Space
# ボヘミアンスタイルのワークスペース
Bohemian-Style Workspace