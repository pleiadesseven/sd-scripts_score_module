import os
import torch
import argparse
##############################################################################################################
# EMA 実装
# ckptでよく見たモデル名についてるema_onlyとかのやつ
# 雑に解説すると直近の更新の影響が大きくなりすぎるのでそれを抑制する仕組み
# 
def add_append_score_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("--diff_enable", action="store_true", help="差分学習を行う")
    parser.add_argument("--diff_type", type=int, default=0, help="下記の計算式参考にして")
    parser.add_argument("--diff_vec", type=str, choices=["+", "-"], default="+")
    parser.add_argument("--diff_guidance", type=float, default=1.0, help="強度")
    parser.add_argument("--diff_nautral_alpha", type=float, default=0.0, help="式のタイプが6の時のみ有効、0より大きな値の時のみ有効になる、neutralな出力を学習しないようにする強度")
    parser.add_argument("--diff_alpha", type=float, default=1.0, help="negative_promptに対する強度")
    parser.add_argument("--diff_target_prompt", type=str, default="", help="ここで指定したpromptを作成する my_promptを指定すると教師データのpromptを使用する")
    parser.add_argument("--diff_neutral_prompt", type=str, default="", help="モデルの素の出力を決めるためのprompt、基本的には空でいい")
    parser.add_argument("--diff_negative_to_noise", action="store_true", help="negative_promptを使用する場合に教師データとの間でもnetative_promptとの差分を作成する")
    parser.add_argument("--diff_negative_prompt", type=str, default=None, help="ここで指定したpromptを作成する 指定しない場合negativeは使用しない")
'''
diff_vec が +
[diff_type = 0]式： neutral + (noise - target)
[diff_type = 1]式： target + (noise - neutral)
[diff_type = 2]式： noise + (neutral - target)
[diff_type = 3]式： noise + (target - neutral)
[diff_type = 4]式： noise + (noise - neutral + (target - neutral))
[diff_type = 5]式： noise + (noise - target + (neutral - target))
diff_vec が -
[diff_type = 0]式： neutral - (noise - target)
[diff_type = 1]式： target - (noise - neutral)
[diff_type = 2]式： noise - (neutral - target)
[diff_type = 3]式： noise - (target - neutral)
[diff_type = 4]式： noise - (noise - neutral + (target - neutral))
[diff_type = 5]式： noise - (noise - target + (neutral - target))

negative_prompt有効時は下記の式が先に適用される
neutral = neutral + (neutral - negative)
target = neutral + (target - negative)

[diff_type = 6] 上記とは違って教師側ではなくunet側の出力に対しての計算
noise_pred = unet(inputs)
negative = negative - neutral
target = target - neutral
noise_pred + (alpha * negative) - (guidance * target)

'''
##############################################################################################################
# ここから下は特に説明ない
class diff():
    def __init__(self, network, args) -> None:
        print("差分学習管理クラスを作成しました")
        self.type = args.diff_type
        self.multipliers = {}
        self.diff_alpha = args.diff_alpha
        self.guidance = args.diff_guidance
        self.negative_to_noise = args.diff_negative_to_noise
        if args.diff_vec == "-":
            self.guidance *= -1
        self.target_prompt = args.diff_target_prompt
        self.neutral_prompt = args.diff_neutral_prompt
        self.negative_prompt = args.diff_negative_prompt
        for lora in network.text_encoder_loras + network.unet_loras:
            self.multipliers[lora.lora_name] = lora.multiplier
    def set_multi(self, network, value=0.):
        for lora in network.text_encoder_loras + network.unet_loras:
            lora.multiplier = value
    def reset_multi(self, network):
        for lora in network.text_encoder_loras + network.unet_loras:
            lora.multiplier = self.multipliers[lora.lora_name]
    def create_prompts(self, prompts, mode="neutral"):
        new_prompt = []
        for i, prompt in enumerate(prompts):
            if mode == "neutral":
                new_prompt.append("")
            elif mode == "target":
                if self.target_prompt == "my_prompt":
                    new_prompt.append(prompt)
                else:
                    new_prompt.append(self.target_prompt)
            elif mode == "negative":
                if self.negative_prompt == None:
                    new_prompt = None
                else:
                    new_prompt.append(self.negative_prompt)
        return new_prompt
    
    def create_target(self, noise, neutral, target, negative=None):
        if negative is not None and self.negative_to_noise:
            noise = noise + self.diff_alpha * (noise - negative)
        if self.type == 0:
            output = neutral
            guidance = (noise - target)
        if self.type == 1:
            output = target
            guidance = (noise - neutral)
        if self.type == 2:
            output = noise
            guidance = (neutral - target)
        if self.type == 3:
            output = noise
            guidance = (target - neutral)
        if self.type == 4:
            output = noise
            guidance = (noise - neutral + (target - neutral))
        if self.type == 5:
            output = noise
            guidance = (noise - target + (neutral - target))
        return output + self.guidance * guidance