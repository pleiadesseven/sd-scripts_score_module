# こちらを向く
looking at viewer
# 他を見る
looking at another
looking away
# こちらを見上げる
looking up
# こちらを見下ろす
looking down
# 顔を傾ける
head tilt