# ドラゴン
dragon
__zzz_color__ dragon
dragon, with scales and wings
# ゴブリン
goblin
__zzz_color__ goblin
goblin, small and mischievous creature
# オーク
orc
__zzz_color__ orc
orc, muscular humanoid with tusks
#ゾンビ
zombie
__zzz_color__ zombie
zombie, undead creature with decaying flesh
#ウィッチ
witch
__zzz_color__ witch
witch, with a pointed hat and broomstick
#スケルトン
skeleton
__zzz_color__ skeleton
skeleton, undead creature made of bones
#ゴーレム
golem
__zzz_color__ golem
golem, massive creature made of stone or clay
#ヴァンパイア
vampire
__zzz_color__ vampire
vampire, pale creature with fangs and a cape
#サイクロプス
cyclops
__zzz_color__ cyclops
cyclops, giant creature with a single eye
#ウェアウルフ
werewolf
__zzz_color__ werewolf
werewolf, human with the ability to transform into a wolf
#ハーピー
harpy
__zzz_color__ harpy
harpy, half-bird, half-human creature
#ケンタウロス
centaur
__zzz_color__ centaur
centaur, humanoid with the body of a horse
#メデューサ
medusa
__zzz_color__ medusa
medusa, snake-haired woman with a petrifying gaze
#ティラノサウルス
tyrannosaurus
__zzz_color__ tyrannosaurus
tyrannosaurus, giant carnivorous dinosaur
#ヒドラ
hydra
__zzz_color__ hydra
hydra, multi-headed serpent-like creature
#エルフ
elf
__zzz_color__ elf
elf, graceful and long-lived humanoid with pointed ears
#ミノタウロス
minotaur
__zzz_color__ minotaur
minotaur, humanoid with the head of a bull
#ミイラ
mummy
__zzz_color__ mummy
mummy, wrapped in bandages and cursed
#ナーガ
naga
__zzz_color__ naga
naga, half-human, half-serpent creature
#ドワーフ
dwarf
__zzz_color__ dwarf
dwarf, short and stout humanoid skilled in mining and crafting
#ハーピー
harpy
__zzz_color__ harpy
harpy, half-bird, half-human creature
#ゴースト
ghost
__zzz_color__ ghost
ghost, spirit of a deceased person
#リザードマン
lizardman
__zzz_color__ lizardman
lizardman, reptilian humanoid
#イーグルグリフォン
eagle griffin
__zzz_color__ eagle griffin
eagle griffin, majestic creature with the body of a lion and wings of an eagle
#ファイアエレメンタル
fire elemental
__zzz_color__ fire elemental
fire elemental, living embodiment of fire
#デーモン
demon, powerful and malevolent supernatural being
#ワイト
wight, undead creature with a pale complexion
#クリーチャー
creature, mysterious and otherworldly being
#ネクロマンサー
necromancer, dark sorcerer who commands the undead
#ハーピーウィッチ
harpy witch, a combination of a harpy and a witch
#ゴーレムガーディアン
golem guardian, a colossal stone creature protecting ancient ruins
#リッチ
lich, undead sorcerer with powerful dark magic
#ワイルドベア
wild bear, a massive and aggressive bear
#ストームジャイアント
storm giant, towering giant with control over thunder and lightning
#ダークエルフ
dark elf, sinister and stealthy elf associated with the shadows
#デーモンハンター
demon hunter, skilled warrior dedicated to hunting and slaying demons
#フェニックス
phoenix, mythical bird of fire and rebirth
#ドラゴンキング
dragon king, the ruler of dragons with immense power
#ウォーターエレメンタル
water elemental, living embodiment of water
#サンダーゴーレム
thunder golem, a golem charged with electricity and lightning
#フロストウォーリアー
frost warrior, warrior with icy armor and weapons
#ナイトメア
nightmare, horse-like creature with fiery mane and hooves
#ダークファイアウィザード
dark fire wizard, spellcaster with control over dark flames
#ベヒーモス
behemoth, massive and powerful creature of destruction
#スプライト
sprite, tiny and mischievous nature spirit
#サーペントドラゴン
serpent dragon, dragon with a long serpentine body
#シャドウアサシン
shadow assassin, assassin skilled in stealth and shadow manipulation
#アンデッドナイト
undead knight, skeletal knight armed with a sword and shield
#アイスウィッチ
ice witch, witch with control over ice and cold magic
#エンタングルヴァイン
entangle vine, animated and aggressive plant tendrils
#デスワーム
death worm, gigantic and vicious subterranean creature
#サイバーネクロマンサー
cyber necromancer, futuristic sorcerer who manipulates technology and the undead
#スライム
slime, gelatinous creature in a simple round shape
#ラット
rat, small rodent with sharp teeth and whiskers
#バット
bat, flying mammal with leathery wings
#スパイダー
spider, eight-legged arachnid with a venomous bite
#ネズミ
mouse, small rodent with a long tail and whiskers
#コボルト
kobold, small reptilian creature with a mischievous nature
#バンパイアバット
vampire bat, bat-like creature with fangs and a thirst for blood
#邪悪な蠕動
malevolent wriggling mass, composed of intertwining tendrils, eyes and mouths, emitting cacophony of grotesque sounds