import torch
from torch.nn import functional as F
from torch import Tensor
from torch.nn import Module
from torch.nn import _reduction as _Reduction

class _Loss(Module):
    reduction: str

    def __init__(self, size_average=None, reduce=None, reduction: str = 'mean') -> None:
        super().__init__()
        if size_average is not None or reduce is not None:
            self.reduction: str = _Reduction.legacy_get_string(size_average, reduce)
        else:
            self.reduction = reduction
class MSE_to_HuberLoss(_Loss):
    """
    warmupで指定されたステップ数の間はmse lossを使用しそれ以降はhuber lossに切り替えるだけ
    ステップ数のカウントは呼び出し毎に行われるので注意
    またreductionの設定はmse huber共通なのも注意
    warmup付きのLr schedulerと併せて使うのを推奨
    """
    __constants__ = ['reduction', 'delta']

    def __init__(self, reduction: str = 'mean', delta: float = 1.0, warmup: int = 500) -> None:
        super().__init__(reduction=reduction)
        self.delta = delta
        self.warmup = warmup
        self.count = -1

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        self.count += 1
        if self.count>= self.warmup:
            return F.huber_loss(input, target, reduction=self.reduction, delta=self.delta)
        else:
            return F.mse_loss(input, target, reduction=self.reduction)