import os
import glob
import argparse
import tqdm

import numpy as np
from PIL import Image, ImageFile

import clip
import torch
from torch.utils.data import TensorDataset, DataLoader

import common_modules
from score_module.rewardfunction_score import MLP, Score_Manager, get_image_preprocess
from append_module_score import Prompt_manager
def train():
    parser = argparse.ArgumentParser(description= "train rewardfunction")
    parser.add_argument("--output", type=str, default="rf_model")
    parser.add_argument("--dataset", type=str, default=None)
    parser.add_argument("--prompt_file", type=str, default=None)
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--eval_per", type=float, default=0.1)
    parser.add_argument("--num_workers", type=int, default=0)
    parser.add_argument("--temperature", type=float, default=2.)
    parser.add_argument("--lambda", type=float, default=0.5)
    parser.add_argument("--epoch", type=int, default=50)
    parser.add_argument("--shuffle_caption", action="store_true")
    parser.add_argument("--keep_tokens", type=int, default=0)
    parser.add_argument("--score_args", type=str, nargs="*", help="スコアAIの拡張用")
    parser.add_argument("--version", type=int, default=0, help="0 is maxtoken 75 1 is maxtoken 225")
    parser.add_argument("--max_create_prompt_count", type=int, default=20)
    parser.add_argument("--exception_list", type=str, default="exception_list.txt")
    parser.add_argument("--less_data", action="store_true", help="データ量が少ない場合指定した方が精度があがるかもしれない")
    args = parser.parse_args()

    ###############################################################
    img_dot = ["jpg", "png", "webp"]
    ###############################################################
    # 起動設定

    if args.prompt_file is None:
        print("学習に使用するprompt設定のファイルを選択してください")
        args.prompt_file = common_modules.filedialog_clicked(["txt"])
    if not os.path.isfile(args.prompt_file):
        print(f"prompt設定ファイルの指定が正しくありません: {args.prompt_file}")
        return
    print(f"prompt file is {args.prompt_file}")
    if args.dataset is None:
        print("学習に使用するデータセットフォルダを選択してください")
        args.dataset = common_modules.dirdialog_clicked()
    if not os.path.isdir(args.dataset):
        print(f"データセットフォルダの指定が正しくありません: {args.dataset}")
        return
    print(f"dataset dir is {args.dataset}")

    ###############################################################
    # model準備

    device = "cuda" if torch.cuda.is_available() else "cpu"
    if args.version == 0:
        image_size = 1
        token_max_size = 77
    elif args.version == 1:
        image_size = 1
        token_max_size = 231
        args.output = args.output + "_225"
    elif  args.version == 2:
        image_size = 5
        token_max_size = 77
        args.output = args.output + "_x4"
    elif  args.version == 3:
        image_size = 5
        token_max_size = 231
        args.output = args.output + "_x4_225"

    clip_model, clip_preprocess = clip.load("ViT-L/14", device=device)  #RN50x64
    tokenizer = clip.tokenize
    token_chunk_size= 77
    token_split_num = token_max_size // token_chunk_size
    mlp = MLP(768*(image_size+token_split_num))
    prompt_manager = Prompt_manager(args.prompt_file, False, args.keep_tokens)

    preprocess_size = clip_model.visual.input_resolution
    npy_dot = ".npy"
    if args.version >= 2:
        npy_dot = "_x4.npy"
        image_preprocess = get_image_preprocess(preprocess_size, 2)

    # 例外リスト作成
    exception_list = []
    if os.path.isfile(args.exception_list):
        with open(args.exception_list, "rt", encoding="utf-8") as f:
            for line in f.readlines():
                if line == "" or line[0] == "#":
                    continue
                exception_list.append(line)

    ###############################################################
    # dataset 作成

    _file_list = [glob.glob(args.dataset+"/true/*.txt")+glob.glob(args.dataset+"/true/*/*.txt"), glob.glob(args.dataset+"/false/*.txt")+glob.glob(args.dataset+"/false/*/*.txt")]

    target = ["true", "false"]
    data_len = 0
    images_list = []
    prompts_list = []
    ids_list = []
    text_emb_list = []
    scores_list = []
    scores_false = []

    for count, target_file_list in enumerate(_file_list):
        print(f"データセット構築中({count+1}/2)...")
        target_key = target[count]
        for _fn in tqdm.tqdm(target_file_list):
            base_name = os.path.splitext(_fn)[0]

            # まずは画像ファイルもしくはコンバート済みのファイルを読み込む
            npy_name = f"{base_name}{npy_dot}"
            img_name = []
            for dot in img_dot:
                img_name.append(f"{base_name}.{dot}")
            get_img_flag = False
            if os.path.isfile(npy_name):
                image = np.load(npy_name)
                if image.shape[1] == 768 * image_size:
                    get_img_flag = True
            if not get_img_flag:
                for i in range(len(img_name)):
                    if os.path.isfile(img_name[i]):
                        get_img_flag = True
                        img_name = img_name[i]
                        break
                if get_img_flag:
                    raw_image = Image.open(img_name)
                    pre_image = clip_preprocess(raw_image).unsqueeze(0).to(device)
                    with torch.no_grad():
                        image_features = clip_model.encode_image(pre_image)
                    if args.version >= 2:
                        _pre_image = image_preprocess(raw_image).unsqueeze(0).to(device)
                        for i in range(2):
                            for j in range(2):
                                with torch.no_grad():
                                    image_features = torch.concat([image_features, clip_model.encode_image(_pre_image[:,:,i*preprocess_size:(i+1)*preprocess_size, j*preprocess_size:(j+1)*preprocess_size])], dim=1)

                    image = image_features.cpu().detach().numpy()
                    np.save(npy_name, image)

            if not get_img_flag:
                print(f"{base_name}に対応する画像ファイルもしくはnpzファイルが見つかりませんでした")
                continue

            # txt ファイルを読み込む
            with open(_fn, "rt", encoding="utf-8") as f:
                prompt = f.read()
            prompt_ids = tokenizer(prompt, token_max_size, truncate=True)
            if token_split_num > 1:
                prompt_ids = torch.stack(prompt_ids.chunk(token_split_num, 1)).squeeze(1)
            try :
                with torch.no_grad():
                    text_emb = clip_model.encode_text(prompt_ids.to(device))
            except:
                print(f"{_fn} の読み込みに失敗しました")
                continue
            if token_split_num > 1:
                prompt_ids = prompt_ids.view([1,-1])
                text_emb = text_emb.view([1,-1])

            images_list.append(image.squeeze(0))
            prompts_list.append(prompt)
            ids_list.append(prompt_ids.squeeze(0))
            text_emb_list.append(text_emb.squeeze(0))
            scores_list.append(1.-count)
    datalen = len(images_list)
    train_datalen = int(datalen * (1-args.eval_per))
    eval_datalen = int(datalen- train_datalen)
    print(f"data count: {datalen} train len: {train_datalen} eval len: {eval_datalen}")
    # dataloaderに入れる
    images_list = np.stack(images_list)
    images_list = torch.Tensor(images_list)
    scores_list = torch.Tensor(scores_list)
    ids_list = torch.stack(ids_list)
    text_emb_list = torch.stack(text_emb_list)
    prompts_id_list = torch.Tensor([i for i in range(len(prompts_list))])
    print(text_emb_list.size())

    _dataset = TensorDataset(images_list, scores_list, ids_list, text_emb_list, prompts_id_list)
    train_dataset, val_dataset = torch.utils.data.random_split(_dataset, [train_datalen, eval_datalen])
    num_workers = min(args.num_workers, os.cpu_count() - 1)  # cpu_count-1 ただし最大で指定された数まで
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True,  num_workers=num_workers) # create your dataloader
    val_loader = DataLoader(val_dataset, batch_size=args.batch_size,  num_workers=num_workers) # create your dataloader
    ###############################################################
    # 学習準備
    mlp.to(device)
    optimizer = torch.optim.Adam(mlp.parameters()) 
    ###############################################################
    # 学習

    def RewardFunction(img_embedding, ids=None, txt_emb=None):
        # compute embeddings for tokens
        if txt_emb is not None:
            txt_embedding = txt_emb
        else:
            txt_embeddings = []
            with torch.no_grad():
                for i in range(token_split_num):
                    txt_embedding = clip_model.encode_text(ids[:, i*77:(i+1)*77])
                    txt_embeddings.append(txt_embedding)
                if token_split_num > 1:
                    txt_embedding = torch.concat(txt_embeddings, dim=1)
        input_embeds = torch.concat([img_embedding, txt_embedding], dim=1)
        # predict score
        return mlp(input_embeds)

    best_loss = 99999.
    best_epoch = 0
    _scores = None
    if args.less_data:
        print("---------------------\n少量データのための事前調整")
        _create_prompt_count = 0
        for i, batch in enumerate(tqdm.tqdm(train_loader)):
            images, scores, ids, text_emb, prompts_id = batch
            images = images.to(device).float()
            scores = scores.unsqueeze(1).to(device)
            text_emb = text_emb.to(device)
            ids = ids.to(device)
            _scores = torch.zeros([prompts_id.size()[0]], dtype=torch.int64, device=device)

            if args.shuffle_caption:
                for j in range(prompts_id.size()[0]):
                    _prompt_id = int(prompts_id[j].item())
                    _prompt = prompts_list[_prompt_id]
                    _prompt = prompt_manager._shuffle_caption(_prompt)
                    ids[j] = tokenizer(_prompt, token_max_size, truncate=True).to(device)
                r_preds = RewardFunction(images, ids)
            else:
                r_preds = RewardFunction(images, ids, text_emb)
            loss_mse = torch.nn.functional.mse_loss(r_preds, scores * 0.)

            fake_prompt = []
            max_create_prompt_count = args.max_create_prompt_count
            for j in range(prompts_id.size()[0]):
                create_prompt_count = 0
                while True:
                    create_prompt_count += 1
                    if _create_prompt_count < create_prompt_count: _create_prompt_count = create_prompt_count
                    _prompt = prompt_manager.get_prompt()
                    _prompt_id = int(prompts_id[j].item())
                    if _prompt == prompts_list[_prompt_id] : continue
                    #print(_prompt)
                    _tags = _prompt.split(",")
                    found_tag = False
                    for _tag in _tags:
                        if _tag[0] == " ": _tag = _tag[1:]
                        if _tag[-1] == " ": _tag = _tag[:-1]
                        if _tag in exception_list: continue
                        if _tag in prompts_list[_prompt_id]:
                            found_tag = True
                            break
                    if create_prompt_count >= max_create_prompt_count:
                        break
                    if found_tag:
                        continue
                    else:
                        break
                if args.shuffle_caption:
                    _prompt = prompt_manager._shuffle_caption(_prompt)
                fake_prompt.append(_prompt)
            try:
                fake_preds = RewardFunction(images, tokenizer(fake_prompt, token_max_size, truncate=True).to(device))
            except RuntimeError:
                fake_preds = RewardFunction(images, tokenizer(fake_prompt, 225, truncate=True).to(device))


            fake_preds = fake_preds / args.temperature
            loss_cre = torch.nn.functional.cross_entropy(fake_preds, _scores)
            loss = loss_mse.mean() + loss_cre.mean()

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

    for epoch in range(args.epoch):
        print("---------------------")
        print(f"epoch {epoch}")
        _create_prompt_count = 0
        for i, batch in enumerate(tqdm.tqdm(train_loader)):
            images, scores, ids, text_emb, prompts_id = batch
            images = images.to(device).float()
            scores = scores.unsqueeze(1).to(device)
            text_emb = text_emb.to(device)
            ids = ids.to(device)
            _scores = torch.zeros([prompts_id.size()[0]], dtype=torch.int64, device=device)

            if args.shuffle_caption:
                for j in range(prompts_id.size()[0]):
                    _prompt_id = int(prompts_id[j].item())
                    _prompt = prompts_list[_prompt_id]
                    _prompt = prompt_manager._shuffle_caption(_prompt)
                    ids[j] = tokenizer(_prompt, token_max_size, truncate=True).to(device)
                r_preds = RewardFunction(images, ids)
            else:
                r_preds = RewardFunction(images, ids, text_emb)
            loss_mse = torch.nn.functional.mse_loss(r_preds, scores)

            fake_prompt = []
            max_create_prompt_count = args.max_create_prompt_count
            for j in range(prompts_id.size()[0]):
                create_prompt_count = 0
                while True:
                    create_prompt_count += 1
                    if _create_prompt_count < create_prompt_count: _create_prompt_count = create_prompt_count
                    _prompt = prompt_manager.get_prompt()
                    _prompt_id = int(prompts_id[j].item())
                    if _prompt == prompts_list[_prompt_id] : continue
                    #print(_prompt)
                    _tags = _prompt.split(",")
                    found_tag = False
                    for _tag in _tags:
                        _tag = _tag.strip()
                        if _tag == "": continue
                        if _tag in exception_list: continue
                        if _tag in prompts_list[_prompt_id]:
                            found_tag = True
                            break
                    if create_prompt_count >= max_create_prompt_count:
                        break
                    if found_tag:
                        continue
                    else:
                        break
                if args.shuffle_caption:
                    _prompt = prompt_manager._shuffle_caption(_prompt)
                fake_prompt.append(_prompt)
            try:
                fake_preds = RewardFunction(images, tokenizer(fake_prompt, token_max_size, truncate=True).to(device))
            except RuntimeError:
                fake_preds = RewardFunction(images, tokenizer(fake_prompt, 225, truncate=True).to(device))


            fake_preds = fake_preds / args.temperature
            loss_cre = torch.nn.functional.cross_entropy(fake_preds, _scores)
            loss = loss_mse.mean() + loss_cre.mean()

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        losses = []
        for i, batch in enumerate(val_loader):
            images, scores, ids, text_emb, _ = batch
            images = images.to(device).float()
            scores = scores.unsqueeze(1).to(device)
            text_emb = text_emb.to(device)
            ids = ids.to(device)

            with torch.no_grad():
                r_preds = RewardFunction(images, ids, text_emb)
                loss = torch.nn.functional.mse_loss(r_preds, scores)

            losses.append(loss.item())

            if i % 1000 == 0:
                print('\tValidation - Epoch %d | Batch %d | MSE Loss %6.4f' % (epoch, i, loss.item()))

        print('Validation - Epoch %d | MSE Loss %6.4f' % (epoch, sum(losses) / len(losses)))
        print(f"create prompt max count: {_create_prompt_count}")
        if sum(losses) / len(losses) < best_loss:
            print("Best MAE Val loss so far. Saving model")
            best_loss = sum(losses) / len(losses)
            best_epoch = epoch
            print(best_loss)

            save_name = f"{args.output}.pth"
            torch.save(mlp.state_dict(), save_name)

    save_name = f"{args.output}_last.pth"
    torch.save(mlp.state_dict(), save_name)
    print("-------------------")
    print(f"[epoch:{best_epoch}]best loss: {best_loss}")


if __name__ == "__main__":
    train()