import os
import tqdm
import argparse
import time
import datetime

import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from safetensors.torch import save_file
from transformers.optimization import Adafactor, AdafactorSchedule, SchedulerType, TYPE_TO_SCHEDULER_FUNCTION
#from accelerate import Accelerator, InitProcessGroupKwargs, DistributedDataParallelKwargs, PartialState

import common_modules
from custom_CosinLR_Scheduler import CustomCosineAnnealingLR
from score_module.musiq_module import Create_MUSIQ_SCORE_Model, MUSIQ_Score_Dataset, get_property, print_dict, collate_fn

try:
    import torch.utils.tensorboard.writer as tensorboard
except:
    pass

class print_command():
    DEL = "\033[2K\033[G"
def create_time_log(start_time, data_len=None):
    epoch_time_log = ""
    epoch_time = time.time() - start_time
    now_epoch_time = epoch_time
    for t in range(2):
        et = now_epoch_time // (60**(2-t))
        epoch_time_log = f"{epoch_time_log}:{int(et):02d}"
        now_epoch_time = now_epoch_time % (60**(2-t))
    if data_len is not None:
        return f"{epoch_time_log}:{now_epoch_time:02.02f} ({data_len/epoch_time:02.03f} step/time)"
    else:
        return f"{epoch_time_log}:{now_epoch_time:02.02f}"
#def get_accelerator(mixed_precision):
#    accelerator = Accelerator(
#        mixed_precision=mixed_precision,
#    )
#    return accelerator

def train():
    parser = argparse.ArgumentParser(description= "train predictor")
    parser.add_argument("--version", type=int, default=0)
    parser.add_argument("--output", type=str, default=None)
    parser.add_argument("--dataset", type=str, default=None)
    parser.add_argument("--batch_size", type=int, default=4)
    parser.add_argument("--eval_per", type=float, default=0.1)
    parser.add_argument("--epoch", type=int, default=50)
    parser.add_argument("--num_workers", type=int, default=0)
    parser.add_argument("--reso_size", type=int, default=None, help="最大解像度、指定しない場合は832")
    parser.add_argument("--hdf5", action="store_true", help="データセットをhdf5形式で構築する、データセット規模が大きい時はメモリ上に載せきれないので使用推奨")
    parser.add_argument("--hdf5_resume", action="store_true", help="すでにhdf5形式になっているデータセットがある場合それをロードする")
    parser.add_argument("--hdf5_add_load", action="store_true", help="hdf5_resumeが有効な時、指定されたデータセットフォルダの内容を追加する")
    parser.add_argument("--hdf5_path", type=str, default=None, help="任意のhdf5ファイルをロードする場合のパス指定")
    #parser.add_argument("--hdf5_paths", type=str, default=None)
    parser.add_argument("--persistent_data_loader_workers", action="store_true")
    parser.add_argument("--optimizer", type=str, default="sgd", help="sgd or adamw or adafactor / 小規模データセットの場合adam系は更新幅が大きすぎて極所解に陥りやすいためsgd推奨")
    parser.add_argument("--load_model", type=str, default=None)
    parser.add_argument("--tensorboard", action="store_true")
    args = parser.parse_args()

    ###############################################################
    img_dot = ["jpg", "jpeg", "png", "webp"]
    ###############################################################
    # 起動設定

    if args.dataset is None:
        print("学習に使用するデータセットフォルダを選択してください")
        args.dataset = common_modules.dirdialog_clicked()
    if not os.path.isdir(args.dataset):
        print(f"データセットフォルダの指定が正しくありません: {args.dataset}")
        return
    print(f"dataset dir is {args.dataset}")

    device = "cuda" if torch.cuda.is_available() else "cpu"
    # get params
    model_params, dataset_params, train_params = get_property(args.dataset,
                                                              args.batch_size, eval_per=args.eval_per, mode="musiq_score",
                                                              reso_size=args.reso_size,
                                                              hdf5=args.hdf5, hdf5_resume=args.hdf5_resume, hdf5_add_load=args.hdf5_add_load,
                                                              hdf5_path=args.hdf5_path,
                                                              version=0, model_name=args.output)

    # print params
    print("学習情報")
    print_dict(dataset_params, "dataset params")
    print_dict(model_params, "model params")
    print_dict(train_params, "train params")
    # tensorboard 起動
    tensorboard_flag = args.tensorboard
    if tensorboard_flag:
        try:
            _t = datetime.datetime.today().strftime('%Y%m%d_%H%M')
            tensorboard_output = f"musiq_log/{_t}"
            tb_log = tensorboard.SummaryWriter(tensorboard_output)
        except:
            print("tensorboardの作成に失敗しました")
            tensorboard_flag = False

    pre_encoder, model = Create_MUSIQ_SCORE_Model(**model_params)
    if args.load_model is not None:
        if os.path.isfile(args.load_model):
            state = torch.load(args.load_model)
            model.load_state_dict(state)
    dataset = MUSIQ_Score_Dataset(**dataset_params)
    dataset.create_datalist(pre_encoder)
    del pre_encoder
    dataset.print_datacount()
    dataset.print_datacount_score()
    train_loader = DataLoader(
        dataset,
        batch_size=1,
        shuffle=False,
        collate_fn=collate_fn,
        num_workers=args.num_workers,
        persistent_workers=args.persistent_data_loader_workers,
        pin_memory=True,
    )

    model.to(device)
    t_max_len = min(int((args.epoch//5) * len(train_loader)), 50000)
    u_max_len = min(len(train_loader), 5000)
    if args.optimizer=="adamw":
        optimizer = torch.optim.AdamW(model.parameters(), 1e-5) 
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=t_max_len, eta_min=0)
    elif args.optimizer=="sgd":
        optimizer = torch.optim.SGD(model.parameters(), 1e-4, 0.9, weight_decay=0)
        lr_scheduler = CustomCosineAnnealingLR(optimizer, T_max=t_max_len, U_max=u_max_len, eta_min=0, warmup=u_max_len//2)
        #lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=t_max_len, eta_min=0)
    else:
        optimizer = Adafactor(model.parameters(), warmup_init=True)
        lr_scheduler = AdafactorSchedule(optimizer)

    # choose the loss you want to optimze for
    criterion = nn.MSELoss()
    criterion2 = nn.L1Loss()

    epochs = args.epoch
    global_step = 0

    best_loss =999
    best_l1_loss = 999
    best_epoch = 0
    save_name = train_params["model_name"]
    save_last_name = train_params["save_last_name"]
    scaler = torch.cuda.amp.GradScaler(enabled=True)
    for epoch in range(epochs):
        losses = []
        losses2 = []
        print_outputs = None
        print_scores = None
        max_loss = 0.0
        print_max_outputs = ""
        model.train()
        model.requires_grad_(True)
        train_loader.dataset.eval = False
        pbar_log = {}
        s_time = time.time()
        print(f"--------- Epoch: {epoch:03d} ---------")
        with tqdm.tqdm(range(len(train_loader)), leave=False) as pbar:
            for batch_num, input_data in enumerate(train_loader):
                scores = input_data["score"].to(device)
                latents = input_data["latent"].to(device)
                scale_1_latents = input_data["s1_latent"].to(device)
                scale_2_latents = input_data["s2_latent"].to(device)
                optimizer.zero_grad()

                with torch.cuda.amp.autocast(enabled=True):
                    output = model(latents, scale_1_latents, scale_2_latents)
                    loss = criterion(output, scores)

                losses.append(loss.item())
                scaler.scale(loss).backward()
                scaler.step(optimizer)
                scale = scaler.get_scale()
                scaler.update()
                skip_lr_sched = (scale != scaler.get_scale())
                if not skip_lr_sched:
                    lr_scheduler.step()
                
                # info
                if tensorboard_flag:
                    lrs = lr_scheduler.get_last_lr()
                    for i, lr in enumerate(lrs):
                        tb_log.add_scalar(f"Train/lr{i}", float(lr), global_step)
                    tb_log.add_scalar("Train/step loss", loss.item(), global_step)
                
                if loss.item() > max_loss:
                    max_loss = loss.item()
                    output_len = min(scores.size(0), 5)
                    print_max_outputs = f"max loss: {max_loss}\nscores: {scores[:output_len,0]}\noutput: {output[:output_len,0]}"

                if batch_num % 1000 == 0:
                    print(f'{print_command.DEL}\tEpoch {epoch} | Batch {batch_num} | Loss {loss.item():3.6f}')
                    #print(y)
                pbar_log["iter"] = f"{loss.item():3.3f}"
                pbar_log["loss"] = f"{sum(losses)/len(losses):3.3f}"
                pbar.set_postfix(pbar_log)
                pbar.update(1)
                global_step += 1
                if batch_num + 5 >= len(train_loader):
                    if print_outputs is None:
                        output_len = min(scores.size(0), 5)
                        print_outputs = output[:output_len,0]
                        print_scores = scores[:output_len,0]
                    else:
                        if print_outputs.size(0) < 5:
                            output_len = min(scores.size(0), 5-print_outputs.size(0))
                            print_outputs = torch.concat([print_outputs, output[:output_len,0]], dim=0)
                            print_scores = torch.concat([print_scores, scores[:output_len,0]], dim=0)
        # 情報表示
        print(f"scores: {print_scores.data}")
        print(f"output: {print_outputs.data}")
        print(print_max_outputs)
        epoch_time_log = create_time_log(s_time, len(train_loader))
        print(f'Epoch {epoch} | Loss {sum(losses)/len(losses):3.6f} | time: {epoch_time_log}')
        print("-" * 24)
        # Tensorboard
        if tensorboard_flag:
            tb_log.add_scalar("Train/loss", (sum(losses)/len(losses)), epoch)

        # eval
        losses = []
        losses2 = []
        print_outputs = None
        print_scores = None
        max_loss = 0.0
        print_max_outputs = ""
        model.eval()
        model.requires_grad_(False)
        train_loader.dataset.eval = True
        s_eval_time = time.time()
        with tqdm.tqdm(range(len(train_loader)), leave=False) as pbar:
            for batch_num, input_data in enumerate(train_loader):
                scores = input_data["score"].to(device)
                latents = input_data["latent"].to(device)
                scale_1_latents = input_data["s1_latent"].to(device)
                scale_2_latents = input_data["s2_latent"].to(device)

                with torch.cuda.amp.autocast(enabled=True):
                    output = model(latents, scale_1_latents, scale_2_latents)
                    loss = criterion(output, scores)
                    lossMAE = criterion2(output, scores)
                #loss.backward()
                losses.append(loss.item())
                losses2.append(lossMAE.item())
                #optimizer.step()

                # info
                if loss.item() > max_loss:
                    max_loss = loss.item()
                    output_len = min(scores.size(0), 5)
                    print_max_outputs = f"max loss: {max_loss}\nscores: {scores[:output_len,0]}\noutput: {output[:output_len,0]}"

                if batch_num % 1000 == 0:
                    print(f'{print_command.DEL}\tValidation - Epoch {epoch} | Batch {batch_num} | MSE Loss {loss.item():3.6f} | MAE Loss {lossMAE.item():3.6f}')
                pbar_log["iter"] = f"{loss.item():3.3f}"
                pbar_log["loss"] = f"{sum(losses)/len(losses):3.3f}"
                pbar.set_postfix(pbar_log)
                pbar.update(1)
                if print_outputs is None:
                    output_len = min(scores.size(0), 5)
                    print_outputs = output[:output_len,0]
                    print_scores = scores[:output_len,0]
                else:
                    if print_outputs.size(0) < 5:
                        output_len = min(scores.size(0), 5-print_outputs.size(0))
                        print_outputs = torch.concat([print_outputs, output[:output_len,0]], dim=0)
                        print_scores = torch.concat([print_scores, scores[:output_len,0]], dim=0)
        # 情報表示
        epoch_time_log = create_time_log(s_eval_time, len(train_loader))
        print(f'Validation - Epoch {epoch} | MSE Loss {(sum(losses)/len(losses)):3.6f} | MAE Loss {(sum(losses2)/len(losses2)):3.6f} | time {epoch_time_log}')
        # Tensorboard
        if tensorboard_flag:
            tb_log.add_scalar("Eval/MSE_loss", (sum(losses)/len(losses)), epoch)
            tb_log.add_scalar("Eval/MAE_loss", (sum(losses2)/len(losses2)), epoch)
            tb_log.add_histogram("Eval/MSE_hist", torch.tensor(losses), epoch)
            tb_log.add_histogram("Eval/MAE_hist", torch.tensor(losses2), epoch)
        print("-" * 24)
        print(f"scores: {print_scores.data}")
        print(f"output: {print_outputs.data}")
        print(print_max_outputs)
        if sum(losses)/len(losses) < best_loss:
            print("-" * 24)
            print(f"Best MSE Val loss so far. Saving model: {save_name}")
            best_loss = sum(losses)/len(losses)
            best_l1_loss = sum(losses2)/len(losses2)
            print( f"{best_loss} / {best_l1_loss}" ) 
            best_epoch = epoch

            torch.save(model.state_dict(), save_name)
            print("-" * 24)
        epoch_time_log = create_time_log(s_time)
        print(f"time taken for epoch: {epoch_time_log}")
        print("=" * 28)

    torch.save(model.state_dict(), save_last_name)

    print( f"best loss: {best_loss} \t savename: {save_name}" ) 
    print(f"[epoch:{best_epoch}]best loss: {best_loss} l1 loss: {best_l1_loss}")

    print("training done!")

if __name__ == "__main__":
    train()