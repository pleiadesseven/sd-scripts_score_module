
import os
import re
# from PIL import Image
# import cv2
import random
import argparse
# import time
import numpy as np
import importlib
import gc

import torch
import pytorch_lightning
# import safetensors.torch
from torchvision import transforms

# import diffusers
from diffusers import (
    StableDiffusionPipeline,
    DDPMScheduler,
    EulerAncestralDiscreteScheduler,
    DPMSolverMultistepScheduler,
    DPMSolverSinglestepScheduler,
    LMSDiscreteScheduler,
    PNDMScheduler,
    DDIMScheduler,
    EulerDiscreteScheduler,
    HeunDiscreteScheduler,
    KDPM2DiscreteScheduler,
    KDPM2AncestralDiscreteScheduler,
)
from library.lpw_stable_diffusion import StableDiffusionLongPromptWeightingPipeline

##############################################################################################################
# オプション関連　ヘルプ書きたくないので　ここ読んで
# 参考論文：https://arxiv.org/abs/2302.12192
#
# 現状だとスコアリングAIにCLIP Aethetic Scoreしか実装してないので
# pip install -e git+https://github.com/openai/CLIP.git@main#egg=clip
# で clip　をインストールしておく必要がある
#
# CLIP Aethetic Scoreのモデルデータは全入りマージGUIのセットに入ってる学習セット使って自分用を作るか
# https://github.com/grexzen/SD-Chad から
# chadscorer.pth (AI画像を使ってAI出力に適応させたモデル)
# sac+logos+ava1-l14-linearMSE.pth (基本的なモデル)
# をダウンロードして置く必要がある
#
# 簡単な導入説明
# コマンドプロンプト起動してから下の３行実行していけば環境構築はできるはず
# cd sd-scriptsのパス
# venv\script\activate
# pip install -e git+https://github.com/openai/CLIP.git@main#egg=clip
#


def add_append_score_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("--score_attenuation", type=float, default=None, help="nllにかける重みをepoch毎に減衰させる場合の値")
    parser.add_argument("--score_multipliers", type=float, default=None, help="スコアリング用の画像を生成する際に学習中のLoRAの適用度を指定する場合にその適用度を設定する")
    parser.add_argument("--score_stack_threshold", type=float, default=None, help="スコアリング用に生成した画像のうちここで指定した値以上のスコアの画像は次の生成以降もそのまま利用する")
    parser.add_argument("--score_make_img_step", type=int, default=1, help="スコアリング用の画像生成をnステップ毎にする 0指定で作成するのは最初の1度だけ")
    parser.add_argument("--score_start_epoch", type=int, default=0, help="スコアリングを用いた学習を開始するステップ数")
    parser.add_argument("--score_sampler", type=str, default="euler", help="スコアリング用の画像生成に使うサンプラー")
    parser.add_argument("--score_sample_step", type=int, default=20, help="スコアリング用の画像生成に使うステップ数")
    parser.add_argument("--score_sample_scale", type=float, default=7.5, help="スコアリング用の画像生成に使うCfgScale")
    parser.add_argument("--score_algo", type=str, default="clip_aesthetic", help="スコアリングに使うAIのアルゴリズム")
    parser.add_argument("--score_model_path", type=str, default="test.pth", help="スコアリングAIのモデルファイルのパス")
    parser.add_argument("--score_prompt", type=str, default="sample.txt", help="スコアリング用の画像生成に使うpromptの設定ファイル")
    parser.add_argument("--score_neg_prompt", type=str, default="", help="スコアリング用の画像生成時に使うネガティブprompt")
    parser.add_argument("--score_beta", type=float, default=1.0, help="negative log likelihoo lossの正則に掛ける重み")
    parser.add_argument("--score_weight", type=float, default=1.0, help="negative log likelihoo lossにかける重み")
    parser.add_argument("--score_shift", type=float, default=0., help="スコアをシフトする値")
    parser.add_argument("--score_clip_min", type=float, default=None, help="スコアをシフトした時にここで指定した値でクリップする")
    parser.add_argument("--score_max", type=float, default=None, help="スコアリングAIの最大評価値・指定しない場合はスコアリングAIのデフォの最大値が使用される")
    parser.add_argument("--score_enable_token_args", action="store_true", help="shuffle tokenなどの設定をスコア用の生成にも適用する")
    parser.add_argument("--score_args", type=str, nargs="*", help="スコアAIの拡張用")
    parser.add_argument("--score_output_img", action="store_true", help="スコアリングように生成した画像を保存するかどうか")
    parser.add_argument("--score_not_add_comment", action="store_true", help="これを指定した場合スコアリング用の設定をメタデータに書き込まない")
    parser.add_argument("--score_loss_algo", type=str, choices=["mse", "nll", "softplus", "kl_div", "mse_v2"], default="nll", help="nllがデフォルト、mseで通常の学習と同じloss計算")
    parser.add_argument("--score_sample_num", type=int, default=1, help="スコアリング用に生成する画像のスコアがより高い値のもの使うために、繰り返し生成する回数")
    parser.add_argument("--score_sample_threshold", type=float, default=0.3, help="繰り返し生成する回数前に次の生成に移るスコアの閾値")
    parser.add_argument("--score_samenoise", action="store_true", help="スコアリング用に生成した画像と教師データに追加するノイズを同じにする")
##############################################################################################################
# ここから下は特に説明ない
#
class print_command():
    DEL = "\033[2K\033[G"
    ROW_DEL = "\033[K"
# {print_command.DEL}

def get_parser_to_comment(args):
    output = f"score's setting: start_epoch={args.score_start_epoch}, sampler={args.score_sampler}, sample_step={args.score_sample_step},"
    output = f"{output} sample_scale={args.score_sample_scale}, algorithm={args.score_algo}, beta={args.score_beta},"
    output = f"{output} weight={args.score_weight}, shift={args.score_shift}, max={args.score_max}"
    if args.score_args is not None:
        output = f"{output}, args={args.score_args}"
    return output

def check_score_flag(args):
    if args.score_sampler is None or args.score_sampler == "":
        return False
    if args.score_algo is None or args.score_algo == "":
        return False
    if args.score_model_path is None or args.score_model_path == "":
        return False
    if os.path.splitext(args.score_model_path)[-1] == ".pth":
        if not os.path.isfile(args.score_model_path):
            return False
    else:
        if not os.path.isfile(f"{args.score_model_path}.pth"):
            return False
    if args.score_prompt is None or args.score_prompt == "":
        return False
    if not os.path.isfile(args.score_prompt):
        return False
    return True

########################################################################################
# オリジナルのスコアリングAIを１から設計したい人はこの辺りの実装参考に
# 評価AI自体はモジュール化前提で呼び出し作ってあるから
# def create_clip_aethetic でimportlib使ってるからこの辺り参考にすれば多分いける
#


class MLP(pytorch_lightning.LightningModule):
    def __init__(self, input_size, xcol='emb', ycol='avg_rating'):
        super().__init__()
        self.input_size = input_size
        self.xcol = xcol
        self.ycol = ycol
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(self.input_size, 1024),
            # nn.ReLU(),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(1024, 128),
            # nn.ReLU(),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(128, 64),
            # nn.ReLU(),
            torch.nn.Dropout(0.1),

            torch.nn.Linear(64, 16),
            # nn.ReLU(),

            torch.nn.Linear(16, 1)
        )

    def forward(self, x):
        return self.layers(x)


class Score_Manager():
    def __init__(self, model_path, device, score_max=None) -> None:
        import clip
        self.clip_model, self.clip_preprocess = clip.load("ViT-L/14", device="cpu")  # RN50x64
        self.mlp = MLP(768)
        self.device = device
        if score_max is not None:
            self.score_max = score_max
        else:
            self.score_max = 10.
        if os.path.splitext(model_path)[-1] != ".pth":
            model_path = f"{model_path}.pth"
        s = torch.load(model_path)

        self.mlp.load_state_dict(s)

    def normalized(self, a, axis=-1, order=2):
        l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
        l2[l2 == 0] = 1
        return a / np.expand_dims(l2, axis)

    def to_gpu(self):
        self.mlp.to(self.device)
        self.mlp.requires_grad_(False)
        self.mlp.eval()
        self.clip_model.to(self.device)
        self.clip_model.eval()

    def to_cpu(self):
        self.mlp.to("cpu")
        self.clip_model.to("cpu")

    def get_score(self, image, prompt=None):
        with torch.no_grad():
            image = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            image_features = self.clip_model.encode_image(image)
            im_emb_arr = self.normalized(image_features.cpu().detach().numpy())

        return self.mlp(torch.from_numpy(im_emb_arr).to(self.device).type(torch.cuda.FloatTensor)).data.cpu()[0][0] / self.score_max

########################################################################################

class Prompt_manager():
    def __init__(self, prompt_file, shuffle_caption=False, keep_tokens=0) -> None:
        self.prompt_path = prompt_file
        self.prompt_dir = os.path.dirname(prompt_file)
        self.enable_flag = os.path.isfile(self.prompt_path)
        self.shuffle_caption = shuffle_caption
        self.keep_tokens = keep_tokens
        self.base_prompt = []
        if self.enable_flag:
            with open(self.prompt_path, "rt", encoding="utf-8") as f:
                for line in f.readlines():
                    if line == "" or line[0] == "#":
                        continue
                    self.base_prompt.append(line)
        self.base_prompt_num = len(self.base_prompt)

    def replace_text_with_file_content(self, text):
        p = re.compile('__(.*?)__')

        while True:
            matches = p.findall(text)
            replace_count = 0
            for match in matches:
                filename = match + '.txt'
                filepath = os.path.join(self.prompt_dir, filename)
                try:
                    with open(filepath, "rt", encoding="utf-8") as f:
                        lines = f.readlines()
                        # check
                        check_count = 0
                        for line in lines:
                            if line == "" or line[0] == "#":
                                continue
                            check_count += 1
                        # get replace word
                        if check_count > 0:
                            while True:
                                line = random.choice(lines).strip()
                                if line == "":
                                    continue
                                if line[0] == "#":
                                    continue
                                break
                            text = text.replace('__' + match + '__', line, 1)
                            replace_count += 1
                except FileNotFoundError:
                    print(f"cant't load {filepath}")
                    pass
                except UnicodeDecodeError:
                    print(f"Unicode Error(文字コードがutf-8以外のファイルは読み込めません): {filepath}")

            if replace_count == 0:
                break
        if self.shuffle_caption:
            text = self._shuffle_caption(text)
            text = text.replace("\n", "")
        return text

    def get_prompt(self):
        if not self.enable_flag:
            return ""
        return self.replace_text_with_file_content(self.base_prompt[random.randint(0, self.base_prompt_num - 1)])

    def _shuffle_caption(self, prompt: str):
        tag_lists = prompt.split(",")
        tag_keep = []
        if self.keep_tokens > 0:
            tag_keep = tag_lists[:self.keep_tokens]
            tag_lists = tag_lists[self.keep_tokens:]
        for _id in range(len(tag_lists)):
            if tag_lists[_id][0] == " ":
                tag_lists[_id] = tag_lists[_id][1:]
        random.shuffle(tag_lists)
        return ", ".join(tag_keep + tag_lists)


class Image_score_manager():
    def __init__(self, tokenizer, device, bucket_list, args) -> None:
        self.score_model_manager = None
        self.prompt_manager = Prompt_manager(args.score_prompt, (args.shuffle_caption and args.score_enable_token_args), args.keep_tokens)
        self.enable = self.prompt_manager.enable_flag
        if self.enable:
            random.seed()
            self.tokenizer = tokenizer
            self.tokenizer_max_length = self.tokenizer.model_max_length if args.max_token_length is None else args.max_token_length + 2
            self.score_neg_prompt = args.score_neg_prompt
            self.score_algo = args.score_algo
            self.model_path = args.score_model_path
            self.device = device
            self.sampler = args.score_sampler
            self.score_sample_step = args.score_sample_step
            self.score_sample_scale = args.score_sample_scale
            self.v_parameterization = args.v_parameterization
            self.clip_skip = args.clip_skip
            self.image_dec = {}
            self.stack_threshold = args.score_stack_threshold
            self.image_dec_stack = None
            self.dec_id_list = {}
            self.score_max = args.score_max
            self.score_args = args.score_args
            self.outpu_flag = args.score_output_img
            self.save_dir_base = args.output_dir + "/score_sample"
            self.sample_num = args.score_sample_num
            self.sample_threshold = args.score_sample_threshold
            self.attenuation = args.score_attenuation
            self.save_dir = None
            self.total_score = 0
            self.score_count = 0
            self.multipliers = {}
            if self.outpu_flag:
                os.makedirs(self.save_dir_base, exist_ok=True)

            # 画像サイズ一覧取得
            # self = dataset enumerate(zip(self.bucket_manager.resos, self.bucket_manager.buckets)) count=len(buckets)
            self.bucket_list = bucket_list
            # for key in bucket_list.keys():
            #    self.bucket_images[key] = []
            # self.every_n_step = args.
            # self.output_dir = args.
            self.create_clip_aethetic()
    def culc_attenuation(self, max_epoch, now_epoch):
        if self.attenuation is None:
            return 1.
        return 1. - (np.arctan(now_epoch/(max_epoch-1)) * self.attenuation)
    def init_multi(self, network):
        for lora in network.text_encoder_loras + network.unet_loras:
            self.multipliers[lora.lora_name] = lora.multiplier
    def set_multi(self, network, value=0.):
        for lora in network.text_encoder_loras + network.unet_loras:
            lora.multiplier = value
    def reset_multi(self, network):
        for lora in network.text_encoder_loras + network.unet_loras:
            lora.multiplier = self.multipliers[lora.lora_name]

    def copy_to_stack(self):
        self.image_dec_stack = {}
        total_count = 0
        for key, items in self.image_dec.items():
            _new_items = []
            count = 0
            for i, _item in enumerate(items):
                if _item["scores"] < self.stack_threshold:
                    continue
                _new_items.append(_item)
                count += 1
            self.image_dec_stack[key] = _new_items
            total_count += count
        print(f"高評価画像の総枚数: {total_count}{print_command.ROW_DEL}")

    def clear_mem(self):
        self.image_dec_stack = None
        if self.stack_threshold is not None:
            self.copy_to_stack()
        del self.image_dec, self.dec_id_list
        gc.collect()
        self.image_dec = {}
        self.dec_id_list = {}

    def make_score_data(self, accelerator, batch_size, epoch, steps, device, vae, text_encoder, unet):
        self.total_score = 0
        self.score_count = 0
        self.image_dec = self.get_image_score(self.make_images(accelerator, batch_size, epoch, steps, device, vae, text_encoder, unet))
        self.create_key_to_idlist()
        print(f"score mean: {self.total_score/self.score_count:.04f}")

    def create_key_to_idlist(self):
        for key in self.image_dec.keys():
            self._create_key_to_dilist(key)

    def _create_key_to_dilist(self, key):
        self.dec_id_list[key] = [i for i in range(len(self.image_dec[key]))]
        random.shuffle(self.dec_id_list[key])

    def create_clip_aethetic(self):
        if self.score_algo == "clip_aethetic" or self.score_algo == "clip_aesthetic":
            self.score_model_manager = Score_Manager(self.model_path, self.device, self.score_max)
        else:
            # importlib
            #  Score_Manager
            #   to_gpu, to_cpu　でgpuとcpuに送る
            #   get_score(image, prompt) でスコアを取得する　imageとpromptはそれぞれ一つずつ送るので注意(バッチ化した処理かくのが面倒だった)
            _score_manager = importlib.import_module(self.score_algo)
            self.score_model_manager = _score_manager.Score_Manager(self.model_path, self.device, self.score_max, self.score_args)

    def get_item(self, batchsize, widht, heigh):
        key = f"({widht}, {heigh})"
        latents_list = []
        id_list = []
        score_list = []
        prompts_list = []
        for i in range(batchsize):
            if len(self.dec_id_list[key]) <= 0:
                self._create_key_to_dilist(key)
            target_id = self.dec_id_list[key].pop(0)
            latents_list.append(self.image_dec[key][target_id]["latents"])
            id_list.append(self.image_dec[key][target_id]["input_ids"])
            score_list.append(self.image_dec[key][target_id]["scores"])
            prompts_list.append(self.image_dec[key][target_id]["prompts"])

        output = {}
        output["latents"] = torch.stack(latents_list)
        output["input_ids"] = torch.stack(id_list)
        output["scores"] = torch.FloatTensor(score_list)
        output["prompts"] = prompts_list

        return output
    
    def _get_image_score(self, img, prompt):
        score = self.score_model_manager.get_score(img, prompt)
        return score

    def get_image_score(self, image_dic):
        self.score_model_manager.to_gpu()

        for key, items in image_dic.items():
            for i, _item in enumerate(items):
                print(f"\rto score {i+1}/{len(items)}{print_command.ROW_DEL}", end="")
                if _item["images"] is None:
                    self.total_score += _item["scores"]
                    self.score_count += 1
                    continue
                _item["scores"] = self.score_model_manager.get_score(_item["images"], _item["prompts"])
                now_score = _item["scores"]
                now_score = f"{now_score:.4f}".replace(".", "_")
                self.total_score += _item["scores"]
                self.score_count += 1
                if self.outpu_flag:
                    filename = f"{self.score_count:05d}_{now_score}"
                    img_filename = f"{filename}.png"
                    txt_filename = f"{filename}.txt"
                    _item["images"].save(os.path.join(self.save_dir, img_filename))
                    with open(os.path.join(self.save_dir, txt_filename), "w", encoding="utf-8") as f:
                        f.write(_item["prompts"])
                _item["images"] = None

        gc.collect()
        self.score_model_manager.to_cpu()
        return image_dic

    def get_input_ids(self, prompt):
        input_ids = self.tokenizer(
            prompt, padding="max_length", truncation=True, max_length=self.tokenizer_max_length, return_tensors="pt"
        ).input_ids
        if self.tokenizer_max_length > self.tokenizer.model_max_length:
            input_ids = input_ids.squeeze(0)
            iids_list = []
            if self.tokenizer.pad_token_id == self.tokenizer.eos_token_id:
                # v1
                # 77以上の時は "<BOS> .... <EOS> <EOS> <EOS>" でトータル227とかになっているので、"<BOS>...<EOS>"の三連に変換する
                # 1111氏のやつは , で区切る、とかしているようだが　とりあえず単純に
                for i in range(
                    1, self.tokenizer_max_length - self.tokenizer.model_max_length + 2, self.tokenizer.model_max_length - 2
                ):  # (1, 152, 75)
                    ids_chunk = (
                        input_ids[0].unsqueeze(0),
                        input_ids[i: i + self.tokenizer.model_max_length - 2],
                        input_ids[-1].unsqueeze(0),
                    )
                    ids_chunk = torch.cat(ids_chunk)
                    iids_list.append(ids_chunk)
            else:
                # v2
                # 77以上の時は "<BOS> .... <EOS> <PAD> <PAD>..." でトータル227とかになっているので、"<BOS>...<EOS> <PAD> <PAD> ..."の三連に変換する
                for i in range(
                    1, self.tokenizer_max_length - self.tokenizer.model_max_length + 2, self.tokenizer.model_max_length - 2
                ):
                    ids_chunk = (
                        input_ids[0].unsqueeze(0),  # BOS
                        input_ids[i: i + self.tokenizer.model_max_length - 2],
                        input_ids[-1].unsqueeze(0),
                    )  # PAD or EOS
                    ids_chunk = torch.cat(ids_chunk)

                    # 末尾が <EOS> <PAD> または <PAD> <PAD> の場合は、何もしなくてよい
                    # 末尾が x <PAD/EOS> の場合は末尾を <EOS> に変える（x <EOS> なら結果的に変化なし）
                    if ids_chunk[-2] != self.tokenizer.eos_token_id and ids_chunk[-2] != self.tokenizer.pad_token_id:
                        ids_chunk[-1] = self.tokenizer.eos_token_id
                    # 先頭が <BOS> <PAD> ... の場合は <BOS> <EOS> <PAD> ... に変える
                    if ids_chunk[1] == self.tokenizer.pad_token_id:
                        ids_chunk[1] = self.tokenizer.eos_token_id

                    iids_list.append(ids_chunk)

            input_ids = torch.stack(iids_list)  # 3,77
        return input_ids

    def _make_images(self, accelerator, pipeline, prompts, width, height, add_print=""):
        if not self.enable:
            return
        #
        images = []
        latents = []
        import time
        with torch.no_grad():
            with accelerator.autocast():
                for i, prompt in enumerate(prompts):
                    if not accelerator.is_main_process:
                        continue
                    print(f"\r{add_print} w:{width} h:{height} {i+1}/{len(prompts)}{print_command.ROW_DEL}", end="")

                    # subset of gen_img_diffusers
                    negative_prompt = self.score_neg_prompt
                    sample_steps = self.score_sample_step
                    scale = self.score_sample_scale
                    # seed = None

                    # image is PIL image
                    sample_threshold = -10000
                    image = None
                    
                    if self.sample_num > 1:
                        self.score_model_manager.to_gpu()

                    for j in range(self.sample_num):
                        _latents = pipeline(
                            prompt=prompt,
                            height=height,
                            width=width,
                            num_inference_steps=sample_steps,
                            guidance_scale=scale,
                            negative_prompt=negative_prompt,
                        )
                        _image = pipeline.latents_to_image(_latents)[0]
                        if self.sample_num > 1:
                            score = self._get_image_score(_image, prompt)
                            if score > self.sample_threshold: break
                            if score > sample_threshold:
                                sample_threshold = score
                                image = _image.copy()
                            print(f"\r{add_print} w:{width} h:{height} {i+1}/{len(prompts)}({j+1}/{self.sample_num}) score:{sample_threshold}{print_command.ROW_DEL}", end="")
                    
                    if image == None:
                        image = _image.copy()
                    
                    if self.sample_num > 1:
                        self.score_model_manager.to_cpu()

                    images.append(image)
                    latents.append(_latents.squeeze(0))
                    #images.append(pipeline(
                    #    prompt=prompt,
                    #    height=height,
                    #    width=width,
                    #    num_inference_steps=sample_steps,
                    #    guidance_scale=scale,
                    #    negative_prompt=negative_prompt,
                    #).images[0])

        return images, latents

    def make_images(self, accelerator, batch_size, epoch, steps, device, vae, text_encoder, unet):
        # 画像生成のコードはkohyaさんのものをほぼそのまま流用
        SCHEDULER_LINEAR_START = 0.00085
        SCHEDULER_LINEAR_END = 0.0120
        SCHEDULER_TIMESTEPS = 1000
        SCHEDLER_SCHEDULE = "scaled_linear"
        width = 512
        height = 512

        # 保存先を作成
        if self.outpu_flag:
            self.save_dir = self.save_dir_base + f"/e-{epoch}"
            os.makedirs(self.save_dir, exist_ok=True)

        # VAEの準備
        org_vae_device = vae.device  # CPUにいるはず
        vae.to(device)
        # schedulerを用意する
        sched_init_args = {}
        if self.sampler == "ddim":
            scheduler_cls = DDIMScheduler
        elif self.sampler == "ddpm":  # ddpmはおかしくなるのでoptionから外してある
            scheduler_cls = DDPMScheduler
        elif self.sampler == "pndm":
            scheduler_cls = PNDMScheduler
        elif self.sampler == "lms" or self.sampler == "k_lms":
            scheduler_cls = LMSDiscreteScheduler
        elif self.sampler == "euler" or self.sampler == "k_euler":
            scheduler_cls = EulerDiscreteScheduler
        elif self.sampler == "euler_a" or self.sampler == "k_euler_a":
            scheduler_cls = EulerAncestralDiscreteScheduler
        elif self.sampler == "dpmsolver" or self.sampler == "dpmsolver++":
            scheduler_cls = DPMSolverMultistepScheduler
            sched_init_args["algorithm_type"] = self.sampler
        elif self.sampler == "dpmsingle":
            scheduler_cls = DPMSolverSinglestepScheduler
        elif self.sampler == "heun":
            scheduler_cls = HeunDiscreteScheduler
        elif self.sampler == "dpm_2" or self.sampler == "k_dpm_2":
            scheduler_cls = KDPM2DiscreteScheduler
        elif self.sampler == "dpm_2_a" or self.sampler == "k_dpm_2_a":
            scheduler_cls = KDPM2AncestralDiscreteScheduler
        else:
            scheduler_cls = DDIMScheduler

        if self.v_parameterization:
            sched_init_args["prediction_type"] = "v_prediction"

        scheduler = scheduler_cls(
            num_train_timesteps=SCHEDULER_TIMESTEPS,
            beta_start=SCHEDULER_LINEAR_START,
            beta_end=SCHEDULER_LINEAR_END,
            beta_schedule=SCHEDLER_SCHEDULE,
            **sched_init_args,
        )
        # clip_sample=Trueにする
        if hasattr(scheduler.config, "clip_sample") and scheduler.config.clip_sample is False:
            # print("set clip_sample to True")
            scheduler.config.clip_sample = True

        pipeline = StableDiffusionLongPromptWeightingPipeline(
            text_encoder=text_encoder,
            vae=vae,
            unet=unet,
            tokenizer=self.tokenizer,
            scheduler=scheduler,
            clip_skip=self.clip_skip,
            safety_checker=None,
            feature_extractor=None,
            requires_safety_checker=False,
        )
        pipeline.progress_bar = lambda x: x
        pipeline.to(device)
        rng_state = torch.get_rng_state()
        cuda_rng_state = torch.cuda.get_rng_state()

        # 画像生成タスク
        image_transforms = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize([0.5], [0.5]),
            ]
        )

        image_dic = {}
        total_length = len(self.bucket_list)
        now_count = 0
        for key, value in self.bucket_list.items():
            width, height = key.split(",")
            width = int(width[1:])
            height = int(height[:-1])
            count = value + (value % batch_size)
            _count = 0

            if self.image_dec_stack is not None:
                if key in self.image_dec_stack:
                    _count = len(self.image_dec_stack[key])

            prompts = []
            images = []
            input_ids = []
            latents_list = []
            # promptを生成する
            for i in range(count - _count):
                prompt = self.prompt_manager.get_prompt()
                _input_ids = self.get_input_ids(prompt)
                prompts.append(prompt)
                input_ids.append(_input_ids)

            # seed生成
            seed = random.randint(0, 0x7FFFFFFF)
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)

            # 画像生成
            images, latents_list = self._make_images(accelerator, pipeline, prompts, width, height, f"[{now_count+1}/{total_length}]")

            # create latents
            #with torch.no_grad():
            #    for i, image in enumerate(images):
            #        # latentに変換
            #        print(f"\rto latents...w: {width} h:{height} {i+1}/{count - _count}{print_command.ROW_DEL}", end="")
            #        if image.mode != "RGB":
            #            image = image.convert("RGB")
            #        latents = vae.encode(image_transforms(np.array(image, np.uint8)).unsqueeze(0).to(device=vae.device, dtype=vae.dtype)).latent_dist.sample().to("cpu")
            #        latents_list.append(latents.squeeze(0) * 0.18215)

            image_dic[key] = []
            for i in range(len(images)):
                print(latents_list[i])
                image_dic[key].append({"images": images[i], "prompts": prompts[i], "latents": latents_list[i], "input_ids": input_ids[i], "scores": None})
            for i in range(_count):
                image_dic[key].append(self.image_dec_stack[key][i])
            now_count += 1

        # clear pipeline and cache to reduce vram usage
        del pipeline
        del image_transforms
        torch.cuda.empty_cache()
        gc.collect()

        torch.set_rng_state(rng_state)
        torch.cuda.set_rng_state(cuda_rng_state)
        vae.to(org_vae_device)

        return image_dic

def apply_snr_weight(loss, timesteps, noise_scheduler, gamma):
    alphas_cumprod = noise_scheduler.alphas_cumprod
    sqrt_alphas_cumprod = torch.sqrt(alphas_cumprod)
    sqrt_one_minus_alphas_cumprod = torch.sqrt(1.0 - alphas_cumprod)
    alpha = sqrt_alphas_cumprod
    sigma = sqrt_one_minus_alphas_cumprod
    all_snr = (alpha / sigma) ** 2
    snr = torch.stack([all_snr[t] for t in timesteps[:loss.size()[0]]])
    gamma_over_snr = torch.div(torch.ones_like(snr) * gamma, snr)
    snr_weight = torch.minimum(gamma_over_snr, torch.ones_like(gamma_over_snr)).float()  # from paper
    loss = loss * snr_weight
    return loss


'''
# 動作確認
if __name__ == "__main__":
    pm = Prompt_manager("a_score_base.txt", True, 1)
    print(pm.get_prompt())

'''
