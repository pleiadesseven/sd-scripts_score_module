import os
import torch
import argparse
##############################################################################################################
# EMA 実装
# ckptでよく見たモデル名についてるema_onlyとかのやつ
# 雑に解説すると直近の更新の影響が大きくなりすぎるのでそれを抑制する仕組み
# 
def add_append_score_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("--ema_enable", action="store_true", help="EMAを使ったモデルを作成する")
    parser.add_argument("--ema_beta", type=float, default=0.995, help="最後の更新の影響具合（小さいほうが影響が大きくなる）")
    parser.add_argument("--ema_start_step", type=int, default=1, help="EMA更新を始めるstep数")
    parser.add_argument("--ema_device", type=str, default="cpu", help="EMAモデルを置いておくデバイス")

##############################################################################################################
# ここから下は特に説明ない
class EMA:
    def __init__(self, args):
        super().__init__()
        print("EMAモデルを作成します")
        self.beta = args.ema_beta
        self.step = 0
        self.start_ema = args.ema_start_step
        self.ema_device = args.ema_device
        self.dtype = None
        self.ema_model = {}

    def create_ema_model(self, current_model: torch.nn.Module):
        self._copy_state_dict(current_model)
    
    def _copy_state_dict(self, current_model: torch.nn.Module):
        self.ema_model = current_model.state_dict()
        for key, item in self.ema_model.items():
            self.ema_model[key] = item.clone().detach().to(self.ema_device)
            if self.dtype is None:
                self.dtype = self.ema_model[key].dtype

    def update_ema(self, current_model: torch.nn.Module, disable_key: list=[]):
        for current_key, current_value in current_model.state_dict().items():
            if current_key in self.ema_model:
                if current_key in disable_key: continue
                self.ema_model[current_key] = self.ema_model[current_key] * self.beta + (1-self.beta) * current_value.to(self.ema_device)

    def step_ema(self, current_model: torch.nn.Module, disable_key: list=[]):
        if self.step == self.start_ema:
            self._copy_state_dict(current_model)
        if self.step >= self.start_ema:
            self.update_ema(current_model, disable_key)

        self.step += 1

    def change_dtype(self, target_device, target_dtype):
        if target_device is None:
            target_device = self.ema_device
        if target_dtype is None:
            target_dtype = torch.float16
            if self.dtype is not None:
                target_dtype = self.dtype
        for key in list(self.ema_model.keys()):
            v = self.ema_model[key]
            v = v.detach().clone().to(target_device).to(target_dtype)
            self.ema_model[key] = v

    def ema_save_to_lora(self, file, dtype, metadata):
        if self.step > self.start_ema:
            if metadata is not None and len(metadata) == 0:
                metadata = None
            
            new_file = f"{os.path.splitext(file)[0]}_ema{os.path.splitext(file)[1]}"

            if dtype is not None:
                self.change_dtype("cpu", dtype)

            if os.path.splitext(file)[1] == ".safetensors":
                from safetensors.torch import save_file
                from library import train_util
                if metadata is None:
                    metadata = {}
                model_hash, legacy_hash = train_util.precalculate_safetensors_hashes(self.ema_model, metadata)
                metadata["sshs_model_hash"] = model_hash
                metadata["sshs_legacy_hash"] = legacy_hash
                save_file(self.ema_model, new_file, metadata)
            else:
                torch.save(self.ema_model, new_file)

            self.change_dtype(self.ema_device, self.dtype)
