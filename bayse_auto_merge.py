import argparse
import numpy
import gc
import os
import sys
import random
from PIL import Image
import requests
import datetime
import re
import matplotlib.pyplot as plt

from tqdm import tqdm
import torch
import toml
import safetensors
from transformers import CLIPTextModel, CLIPTokenizer, CLIPTextConfig, pipeline
from torchvision import transforms

import library.train_util as train_util
from library.model_util import create_unet_diffusers_config, convert_ldm_unet_checkpoint, create_vae_diffusers_config, convert_ldm_vae_checkpoint, convert_ldm_clip_checkpoint_v1, load_vae
from library.original_unet import UNet2DConditionModel
from library.lpw_stable_diffusion import StableDiffusionLongPromptWeightingPipeline

from diffusers import (
    AutoencoderKL,
    StableDiffusionPipeline,
    DDPMScheduler,
    EulerAncestralDiscreteScheduler,
    DPMSolverMultistepScheduler,
    DPMSolverSinglestepScheduler,
    LMSDiscreteScheduler,
    PNDMScheduler,
    DDIMScheduler,
    EulerDiscreteScheduler,
    HeunDiscreteScheduler,
    KDPM2DiscreteScheduler,
    KDPM2AncestralDiscreteScheduler,
    AutoencoderTiny,
)

try:
    from streamdiffusion import StreamDiffusion
    from streamdiffusion.image_utils import postprocess_image
except:
    print("現在の環境ではstream diffusionは使えません") 

from bayes_opt import BayesianOptimization, Events
from bayes_opt.util import load_logs
from bayes_opt.domain_reduction import SequentialDomainReductionTransformer
from bayes_opt.logger import JSONLogger
from scipy.stats import qmc
from torchvision.transforms import Compose, Resize, CenterCrop, ToTensor, Normalize
try:
    from torchvision.transforms import InterpolationMode
    BICUBIC = InterpolationMode.BICUBIC
except ImportError:
    BICUBIC = Image.BICUBIC
import clip

import score_module.clip_aethetic_score as cas_module
from append_merge_lora import check_lora, check_keys
from common_modules import filedialog_clicked
'''
pip install -e git+https://github.com/openai/CLIP.git@main#egg=clip
pip install bayesian-optimization
pip install matplotlib
'''
class print_command():
    DEL = "\033[2K\033[G"
    ROW_DEL = "\033[K"

def create_dirs(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
        #print(f"create: {dir_path}")

def maxwhere(li: list[float]) -> list[int, float]:
    m = 0
    mi = -1
    for i, v in enumerate(li):
        if v > m:
            m = v
            mi = i
    return mi, m


def minwhere(li: list[float]) -> list[int, float]:
    m = 10
    mi = -1
    for i, v in enumerate(li):
        if v < m:
            m = v
            mi = i
    return mi, m
def convergence_plot(
    scores: list[float],
    figname: str = None,
    minimise=False,
) -> None:
    fig = plt.figure()
    ax = fig.add_subplot(111)

    plt.plot(scores)

    star_i, star_score = minwhere(scores) if minimise else maxwhere(scores)
    plt.plot(star_i, star_score, "or")

    plt.xlabel("iterations")

    if minimise:
        plt.ylabel("loss")
    else:
        plt.ylabel("score")

    if figname:
        plt.title(os.path.split(os.path.splitext(figname)[0])[-1])
        print("Saving fig to:", figname)
        plt.savefig(figname)

def _make_image(pipeline, prompt, height, width, step, guidance_scale, neg_prompt, seed, eta=0, strem_flag=False, lcm_flag=False, count=1):
    with torch.no_grad():
        # seed生成
        if seed == -1:
            seed = random.randint(0, 0x7FFFFFFF-eta)
        seed += eta

        if strem_flag:
            stream = StreamDiffusion(
                pipeline,
                t_index_list=[0, 16, 32, 45],
                torch_dtype=torch.float16,
                cfg_type="none",
                width=width,
                height=height
            )
            # 読み込んだモデルがLCMでなければマージする
            if lcm_flag:
                stream.load_lcm_lora()
                stream.fuse_lora()
            
            stream.prepare(prompt, neg_prompt, guidance_scale=guidance_scale, seed=seed)
            for _ in range(4):
                stream()
            
            _images = []
            for j in range(count):
                x_output = stream.txt2img()
                image = postprocess_image(x_output, output_type="pil")
                for i in range(x_output.size(0)):
                    _images.append(image[i])
            
        else:
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)
            generator = torch.Generator(pipeline.device).manual_seed(seed)
            _images = []
            for j in range(count):
                _latents = pipeline(
                    prompt=prompt,
                    height=height,
                    width=width,
                    num_inference_steps=step,
                    guidance_scale=guidance_scale,
                    negative_prompt=neg_prompt,
                    generator=generator
                )
                for i in range(_latents.size(0)):
                    _images.append(pipeline.latents_to_image(_latents[i:i+1])[0])

    return _images

class AestheticClassifier(torch.nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.fc1 = torch.nn.Linear(input_size, hidden_size)
        self.fc2 = torch.nn.Linear(hidden_size, hidden_size // 2)
        self.fc3 = torch.nn.Linear(hidden_size // 2, output_size)
        self.relu = torch.nn.ReLU()
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.relu(x)
        x = self.fc3(x)
        x = self.sigmoid(x)
        return x


class AestheticPredictor(torch.nn.Module):
    def __init__(self, input_size):
        super().__init__()
        self.input_size = input_size
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(self.input_size, 1024),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(1024, 128),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(128, 64),
            torch.nn.Dropout(0.1),
            torch.nn.Linear(64, 16),
            torch.nn.Linear(16, 1),
        )

    def forward(self, x):
        return self.layers(x)

class RewardFunction(torch.nn.Module):
    def __init__(self, input_size, xcol='emb', ycol='avg_rating'):
        super().__init__()
        self.input_size = input_size
        self.xcol = xcol
        self.ycol = ycol
        self.input_size = input_size
        self.hidden_size = max(self.input_size//2, 1024)
        self.layers = torch.nn.Sequential(
            torch.nn.Linear(self.input_size, self.hidden_size),
            torch.nn.ReLU(),
            torch.nn.Dropout(0.2),
            torch.nn.Linear(self.hidden_size, 1)
        )
    def forward(self, x):
        return self.layers(x)

class Score_Manager:
    def __init__(self, setting: dict) -> None:
        self.algo = setting["algo"]
        self.model_path = setting["model_file"]
        self.device = setting.get("device", "cuda")
        self.save_img_flag = setting.get("save_image", True)
        if self.save_img_flag =="false":
            self.save_img_flag = False
        else:
            self.save_img_flag = True
        self.save_img_score = setting.get("save_img_score", -999.)
        self.log_dir = setting["log_dir"]

        self.get_model()
        if not "cafe" in self.algo:
            self.load_model()
        self.threshold = setting.get("threshold", None)
        self.attenuation = setting.get("attenuation", 0.5)

    def _convert_image_to_rgb(self, image):
        return image.convert("RGB")
    def _transform(self, n_px):
        return Compose([
            Resize(n_px, interpolation=BICUBIC),
            CenterCrop(n_px),
            self._convert_image_to_rgb,
            ToTensor(),
            Normalize((0.48145466, 0.4578275, 0.40821073), (0.26862954, 0.26130258, 0.27577711)),
        ])
    def get_image_preprocess(self, preprocess_size, scale=1):
        return self._transform(preprocess_size*scale)
    def get_model(self) -> None:
        if self.algo in ["rf", "rf1", "rf2", "rf3", "cas", "cas1", "cas2"]: return

        LAION_URL = (
            "https://github.com/Xerxemi/sdweb-auto-MBW/blob/master/scripts/classifiers/laion/"
        )
        CHAD_URL = (
            "https://github.com/christophschuhmann/improved-aesthetic-predictor/blob/main/"
        )
        AES_URL = "https://raw.githubusercontent.com/Xerxemi/sdweb-auto-MBW/master/scripts/classifiers/aesthetic/"
        if "cafe" in self.algo:
            print("Creating scoring pipeline")
            self.judge = pipeline(
                "image-classification",
                model=f"cafeai/{self.algo}",
            )
            return

        if os.path.isfile(self.model_path):
            return

        print("You do not have an aesthetic model ckpt, let me download that for you")
        if self.algo == "chad":
            url = CHAD_URL
        elif self.algo == "laion":
            url = LAION_URL
        elif self.algo == "aes":
            url = AES_URL

        url += f"{self.algo}?raw=true"

        r = requests.get(url)
        r.raise_for_status()

        with open(self.model_path, "wb") as f:
            print(f"saved into {self.model_path}")
            f.write(r.content)
    def load_model(self) -> None:
        print(f"Loading {self.algo}")
        self.score_max = 10.0

        if self.algo in ["chad", "laion"]:
            self.model = AestheticPredictor(768).to(self.device).eval()
        elif self.algo in ["rf3"]:
            self.model = RewardFunction(768*8).to(self.device).eval()
            self.score_max = 1.0
        elif self.algo in ["rf2"]:
            self.model = RewardFunction(768*6).to(self.device).eval()
            self.score_max = 1.0
        elif self.algo in ["rf1"]:
            self.model = RewardFunction(768*4).to(self.device).eval()
            self.score_max = 1.0
        elif self.algo in ["rf"]:
            self.model = RewardFunction(768*2).to(self.device).eval()
            self.score_max = 1.0
        elif self.algo in ["aes"]:
            self.model = AestheticClassifier(512, 256, 1).to(self.device).eval()
        elif self.algo in ["cas", "cas1", "cas2"]:
            if self.algo == "cas":
                version = 0
            else:
                version = int(self.algo[-1])
            self.scale_size, self.image_size, _, _, _ = cas_module.get_property(version, self.model_path)
            self.model = cas_module.MLP(768*self.image_size)

        if os.path.splitext(self.model_path)[-1] == ".safetensors":
            self.model.load_state_dict(
                safetensors.torch.load_file(
                    self.model_path,
                )
            )
            self.model.to(self.device)
        else:
            self.model.load_state_dict(
                torch.load(
                    self.model_path,
                    map_location=self.device,
                )
            )
            self.model.to(self.device)
        self.model.eval()
        self.load_clip()
    def load_clip(self) -> None:
        if self.algo in ["chad", "laion", "rf", "rf1", "rf2", "rf3", "cas", "cas1", "cas2"]:
            self.clip_model_name = "ViT-L/14"
        elif self.algo in ["aes"]:
            self.clip_model_name = "ViT-B/32"

        print(f"Loading {self.clip_model_name}")

        self.clip_model, self.clip_preprocess = clip.load(
            self.clip_model_name,
            device=self.device,
        )
        if self.algo in ["rf", "rf1", "rf2", "rf3"]:
            self.tokenizer = clip.tokenize
        self.preprocess_size = self.clip_model.visual.input_resolution
        if self.algo in ["rf2", "rf3"]:
            self.image_preprocess = self.get_image_preprocess(self.preprocess_size, 2)
        elif self.algo in ["cas1", "cas2"]:
            self.image_preprocess = self.get_image_preprocess(self.preprocess_size, self.scale_size)
        self.clip_model.eval()

    def get_prompts_features(self, prompt: str, image_features) -> torch.Tensor:
        if self.algo not in ["rf", "rf1", "rf2", "rf3"]: return image_features
        if self.algo in ["rf"]:
            image_size = 1
            max_token = 77
        elif self.algo in ["rf1"]:
            image_size = 1
            max_token = 231
        elif self.algo in ["rf2"]:
            image_size = 5
            max_token = 77
        elif self.algo in ["rf3"]:
            image_size = 5
            max_token = 231
        token_chunk = 77
        token_split_num = max_token // token_chunk
        
        txt_id = self.tokenizer(prompt, context_length=max_token, truncate=True).to(self.device)
        
        txt_features_list = []
        for i in range(token_split_num):
            txt_features = self.clip_model.encode_text(txt_id[:,i*token_chunk:(i+1)*token_chunk])
            txt_features_list.append(txt_features)
        if token_split_num>1:
            txt_features = torch.concat(txt_features_list, dim=1)

        input_emb = torch.concat([image_features, txt_features], dim=1)
        input_emb = input_emb.cpu().detach().numpy()
        return input_emb
    def get_image_features(self, image: Image.Image) -> torch.Tensor:
        if self.algo in ["chad", "laion"]:
            image = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            with torch.no_grad():
                image_features = self.clip_model.encode_image(image)
                image_features /= image_features.norm(dim=-1, keepdim=True)
            image_features = image_features.cpu().detach().numpy()
            return image_features
        elif self.algo in ["rf", "rf1", "rf2", "rf3"]:
            _image = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            with torch.no_grad():
                image_features = self.clip_model.encode_image(_image)
            if self.algo in ["rf2", "rf3"]:
                _image = self.image_preprocess(image).unsqueeze(0).to(self.device)
                for i in range(2):
                    for j in range(2):
                        with torch.no_grad():
                            image_features = torch.concat([image_features, self.clip_model.encode_image(_image[:,:,i*self.preprocess_size:(i+1)*self.preprocess_size,j*self.preprocess_size:(j+1)*self.preprocess_size])], dim=1)
            return image_features
        elif self.algo in ["cas", "cas1", "cas2"]:
            _image = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            with torch.no_grad():
                image_features = self.clip_model.encode_image(_image)
            if self.algo in ["cas1", "cas2"]:
                _image = self.image_preprocess(image).unsqueeze(0).to(self.device)
                for i in range(self.scale_size):
                    for j in range(self.scale_size):
                        with torch.no_grad():
                            image_features = torch.concat([image_features, self.clip_model.encode_image(_image[:,:,i*self.preprocess_size:(i+1)*self.preprocess_size,j*self.preprocess_size:(j+1)*self.preprocess_size])], dim=1)
                image_features = cas_module.normalized(image_features.cpu().detach().numpy())
                return image_features
        elif self.algo in ["aes"]:
            inputs = self.clip_preprocess(image).unsqueeze(0).to(self.device)
            image_features = self.clip_model.encode_image(inputs).cpu().detach().numpy()
            return (image_features / numpy.linalg.norm(image_features))
    def score(self, image: Image.Image, prompt: str) -> float:
        if self.algo.startswith("cafe"):
            # TODO: this returns also a 'label', what can we do with it?
            # TODO: does it make sense to use top_k != 1?
            data = self.judge(image, top_k=1)
            return data[0]["score"]

        image_features = self.get_image_features(image)
        image_features = self.get_prompts_features(prompt, image_features)

        score = self.model(
            torch.from_numpy(image_features).to(self.device).float(),
        )
        score /= self.score_max

        if self.threshold is not None:
            if score > self.threshold:
                score = torch.atan((score-self.threshold)*self.attenuation) + self.threshold

        return score.item()
    def _to_device(self, _device=None):
        if _device is None: _device = self.device
        if _device == self.device: return

        self.model.to(_device)
        self.clip_model.to(_device)
    def batch_score(
        self,
        images: dict,
        it: int,
    ) -> list[float]:
        scores = []
        self._to_device()
        keys = list(images.keys())
        i = 0
        for key in keys:
            prompt = images[key]["prompt"]
            for img in images[key]["images"]:
                with torch.no_grad():
                    torch.manual_seed(0)
                    torch.cuda.manual_seed(0)
                    score = self.score(img, prompt)
                print(f"{key}-{i} {score:4.5f}")
                if self.save_img_flag:
                    if score > self.save_img_score:
                        self.save_img(img, key, score, it, i)
                i += 1
                scores.append(score)
        self._to_device(self.device)

        return scores
        
    def average_score(self, scores: list[float]) -> float:
        return sum(scores) / len(scores)
    def save_img(
        self,
        image: Image.Image,
        name: str,
        score: float,
        it: int,
        batch_n: int,
    ) -> None:
        img_path = f"{self.log_dir}\{name}-{batch_n}-e{it}-{score:4.3f}.png"
        image.save(img_path)
        return

class Auto_Merger:
    def __init__(self) -> None:
        t_delta = datetime.timedelta(hours=9)
        JST = datetime.timezone(t_delta, 'JST')
        self.now = datetime.datetime.now(JST)

    def make_images(self, prompt_args: dict):
        # 画像生成のコードはkohyaさんのものをほぼそのまま流用
        SCHEDULER_LINEAR_START = 0.00085
        SCHEDULER_LINEAR_END = 0.0120
        SCHEDULER_TIMESTEPS = 1000
        SCHEDLER_SCHEDULE = "scaled_linear"
        width = prompt_args.get("w", 512)
        height = prompt_args.get("h", 512)
        sampler = prompt_args.get("sampler", "euler_a")
        clip_skip = prompt_args.get("clip_skip", None)
        seed = prompt_args.get("seed", -1)
        if prompt_args.get("batch_size", 1) > 1:
            prompt = [prompt_args["prompt"]] * prompt_args.get("batch_size", 1)
            neg_prompt = [prompt_args["neg_prompt"]] * prompt_args.get("batch_size", 1)
        else:
            prompt = [prompt_args["prompt"]]
            neg_prompt = [prompt_args["neg_prompt"]]
        count = prompt_args.get("count", 1)

        # schedulerを用意する
        sched_init_args = {}
        if sampler == "ddim":
            scheduler_cls = DDIMScheduler
        elif sampler == "ddpm":  # ddpmはおかしくなるのでoptionから外してある
            scheduler_cls = DDPMScheduler
        elif sampler == "pndm":
            scheduler_cls = PNDMScheduler
        elif sampler == "lms" or sampler == "k_lms":
            scheduler_cls = LMSDiscreteScheduler
        elif sampler == "euler" or sampler == "k_euler":
            scheduler_cls = EulerDiscreteScheduler
        elif sampler == "euler_a" or sampler == "k_euler_a":
            scheduler_cls = EulerAncestralDiscreteScheduler
            sched_init_args["steps_offset"] = 1
        elif sampler == "dpmsolver" or sampler == "dpmsolver++":
            scheduler_cls = DPMSolverMultistepScheduler
            sched_init_args["algorithm_type"] = sampler
        elif sampler == "dpmsingle":
            scheduler_cls = DPMSolverSinglestepScheduler
        elif sampler == "heun":
            scheduler_cls = HeunDiscreteScheduler
        elif sampler == "dpm_2" or sampler == "k_dpm_2":
            scheduler_cls = KDPM2DiscreteScheduler
        elif sampler == "dpm_2_a" or sampler == "k_dpm_2_a":
            scheduler_cls = KDPM2AncestralDiscreteScheduler
        else:
            scheduler_cls = DDIMScheduler

        scheduler = scheduler_cls(
            num_train_timesteps=SCHEDULER_TIMESTEPS,
            beta_start=SCHEDULER_LINEAR_START,
            beta_end=SCHEDULER_LINEAR_END,
            beta_schedule=SCHEDLER_SCHEDULE,
            **sched_init_args,
        )
        # clip_sample=Trueにする
        if hasattr(scheduler.config, "clip_sample") and scheduler.config.clip_sample is False:
            # print("set clip_sample to True")
            scheduler.config.clip_sample = True

        pipeline = StableDiffusionLongPromptWeightingPipeline(
            text_encoder=self.text_model,
            vae=self.vae,
            unet=self.unet,
            tokenizer=self.tokenizer,
            scheduler=scheduler,
            clip_skip=clip_skip,
            safety_checker=None,
            feature_extractor=None,
            requires_safety_checker=False,
        )
        pipeline.to(self.device)
        rng_state = torch.get_rng_state()
        cuda_rng_state = torch.cuda.get_rng_state()

        # 画像生成タスク
        image_transforms = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize([0.5], [0.5]),
            ]
        )
        
        _images = _make_image(pipeline, prompt, height, width, prompt_args["step"], prompt_args["guidance_scale"], neg_prompt, seed, self.setting["eta"], self.stream_flag, self.lcm_flag, count)
        '''
        with torch.no_grad():
            # seed生成
            if seed == -1:
                seed = random.randint(0, 0x7FFFFFFF)
            torch.manual_seed(seed)
            torch.cuda.manual_seed(seed)
            _latents = pipeline(
                prompt=prompt,
                height=height,
                width=width,
                num_inference_steps=prompt_args["step"],
                guidance_scale=prompt_args["guidance_scale"],
                negative_prompt=neg_prompt,
            )
            _images = []
            for i in range(_latents.size(0)):
                _images.append(pipeline.latents_to_image(_latents[i:i+1])[0])
        '''
        # clear pipeline and cache to reduce vram usage
        del pipeline
        del image_transforms
        torch.cuda.empty_cache()
        gc.collect()

        torch.set_rng_state(rng_state)
        torch.cuda.set_rng_state(cuda_rng_state)

        return _images


    def transform_checkpoint_dict_key(self, k):
        chckpoint_dict_replacements = {
            "cond_stage_model.transformer.embeddings.": "cond_stage_model.transformer.text_model.embeddings.",
            "cond_stage_model.transformer.encoder.": "cond_stage_model.transformer.text_model.encoder.",
            "cond_stage_model.transformer.final_layer_norm.": "cond_stage_model.transformer.text_model.final_layer_norm.",
        }
        for text, replacement in chckpoint_dict_replacements.items():
            if k.startswith(text):
                k = replacement + k[len(text) :]
        return k
    def get_state_dict_from_checkpoint(self, pl_sd):
        pl_sd = pl_sd.pop("state_dict", pl_sd)
        pl_sd.pop("state_dict", None)
        sd = {}
        for k, v in pl_sd.items():
            if new_key := self.transform_checkpoint_dict_key(k):
                sd[new_key] = v

        pl_sd.clear()
        pl_sd.update(sd)
        return pl_sd
    def load_model(self, model_path, device="cpu"):
        print(f"loading: {model_path}")
        if os.path.splitext(model_path)[-1] == ".safetensors":
            ckpt = safetensors.torch.load_file(
                model_path,
                device=device,
            )
        else:
            ckpt = torch.load(model_path, map_location=device)
        return check_lora(self.get_state_dict_from_checkpoint(ckpt))
    
    def merge(self, args):
        if args.toml is None:
            print("tomlファイルを選択してください")
            args.toml = filedialog_clicked([".toml"])
        print(f"load {args.toml}")

        enc_list = ["utf-8"]
        toml_flag = False
        for enc in enc_list:
            try:
                with open(args.toml, mode="r", encoding=enc) as f:
                    self.setting = toml.load(f)
                    toml_flag = True
                    break
            except Exception as e:
                print(f"Error: {e}")
                toml_flag = False
                continue
        if not toml_flag:
            sys.exit("tomlファイルの読み込みに失敗しました")
        # 設定がない場合のデフォルト指定追加
        self.init_points = self.setting.get("init_points", 10)
        self.n_iters = self.setting.get("n_iters", 10)
        if not "precision" in self.setting:
            self.setting["precision"] = "fp16"
        if not "only_best" in self.setting:
            self.setting["only_best"] = True
        if not "output_dir" in self.setting:
            self.setting["output_dir"] = None
        if self.setting["precision"] == "fp16":
            self.dtype = torch.float16
        elif self.setting["precision"] == "fp32":
            self.dtype = torch.float32
        if not "eta" in self.setting:
            self.setting["eta"] = 0
        self.device = self.setting.get("device", "cuda")
        self.mem_eff_attn = False
        self.xformers = False
        self.sdpa = False
        if self.setting.get("attn", "") == "mem_eff_attn":
            self.mem_eff_attn = True
        elif self.setting.get("attn", "") == "xformers":
            self.xformers = True
        elif self.setting.get("attn", "") == "sdpa":
            self.sdpa = True
        if self.setting["output_dir"] is None:
            self.setting["output_dir"] = os.path.split(self.setting["base"])[0]
        if self.setting["output_dir"] == "":
            self.setting["output_dir"] = os.path.split(self.setting["base"])[0]
        self.setting["output_dir"] = os.path.abspath(self.setting["output_dir"])
        if not "name" in self.setting:
            self.setting["name"] = os.path.split(os.path.splitext(self.setting["base"])[0])[-1]
        if not "output_log" in self.setting:
            self.setting["output_log"] = None
        if self.setting["output_log"] is None:
            self.setting["output_log"] = "./log"
        elif self.setting["output_log"] == "":
            self.setting["output_log"] = "./log"
        else:
            self.setting["output_log"] = self.setting["output_log"] + "/log"
        self.setting["output_log"] = os.path.abspath(self.setting["output_log"])
        self.tiny_vae = self.setting.get("tiny_vae", False)
        self.stream_flag = self.setting.get("stream", False)
        self.lcm_flag = self.stream_flag
        if self.stream_flag:
            self.tiny_vae = True

        # 出力先確認
        self.model_output_dir = self.setting["output_dir"]
        self.model_output_base_name = "bam_" + self.setting["name"]
        if self.setting["precision"] == "fp16":
            self.model_output_base_name = f"{self.model_output_base_name}_fp16"
        self.model_output_name = f"{self.model_output_dir}/{self.model_output_base_name}.safetensors"
        
        self.log_dir_base = self.now.strftime("%Y_%m_%d_%H_%M") + "_" + self.setting.get("algo", "bayse") + "_" + self.setting["score"]["algo"]
        self.log_dir_dir = self.setting["output_log"] + "/" + self.log_dir_base
        self.log_dir_img = self.log_dir_dir + "/img"
        self.log_name = self.model_output_base_name + ".json"
        self.log_plt = self.log_dir_dir + "/" + self.model_output_base_name + ".png"
        self.setting["score"]["log_dir"] = self.log_dir_img
        print(f"モデル出力先: {self.model_output_name}")
        print(f"log出力先: {self.log_dir_dir}")

        create_dirs(self.setting["output_dir"])
        create_dirs(self.log_dir_img)

        
        # マージするモデルの設定確認
        default_model_key = None
        if "model_a" in self.setting:
            default_model_key = "model_a"
        self.models_keys = []
        for k in self.setting.keys():
            if "model_" in k:
                if not self.setting[k].get("model_a", False):
                    print(f"fail {k}")
                    continue
                if not os.path.isfile(self.setting[k]["model_a"]):
                    print(f"[fail] not found file {k}")
                    continue
                if default_model_key is None:
                    default_model_key = k
                    print(f"default model setting key: {k}")
                self.models_keys.append(k)
                print(f"found model setting[{len(self.models_keys)}]: {k}")
        # デフォルトの設定確認
        if not "mode" in self.setting[default_model_key]:
            self.setting[default_model_key]["mode"] = "sum"
        if not "culc" in self.setting[default_model_key]:
            self.setting[default_model_key]["culc"] = "default"
        if not "type" in self.setting[default_model_key]:
            self.setting[default_model_key]["type"] = "default"
        if not "global_min" in self.setting[default_model_key]:
            self.setting[default_model_key]["global_min"] = 0.0
        if not "global_max" in self.setting[default_model_key]:
            self.setting[default_model_key]["global_max"] = 1.0
        
        # 出力設定ファイルの確認
        self.prompts_paths = []
        for k in self.setting["prompts"].keys():
            prompt_path = self.setting["prompts"][k]
            if os.path.isfile(prompt_path):
                self.prompts_paths.append(self.setting["prompts"][k])
                print(f"画像生成の設定ファイル[{len(self.prompts_paths)}]: {k}={prompt_path}")

        # log読み込みを行うかチェック
        load_log_flag = False
        log_files = []
        if "logs" in self.setting:
            for v in self.setting["logs"]:
                if os.path.isfile(v):
                    log_files.append(v)
        if len(log_files) >= 1:
            load_log_flag = True
        if load_log_flag:
            print(f"使用するlogファイル: {log_files}")

        # モデルの確認
        print("ベースモデルの事前確認を行います")
        self.global_keys = [kk for kk in self.load_model(self.setting["base"])[0].keys() if "model" in kk and "first_stage_model" not in kk]
        print("モデルの事前確認を行います")
        self.total_param_count = 0
        for k in self.models_keys:
            my_param_count = 0

            # デフォルト設定のコピー
            if not "mode" in self.setting[k]:
                self.setting[k]["mode"] = self.setting[default_model_key]["mode"]
            if not "culc" in self.setting[k]:
                self.setting[k]["culc"] = self.setting[default_model_key]["culc"]
            if not "type" in self.setting[k]:
                self.setting[k]["type"] = self.setting[default_model_key]["type"]
            if self.setting[k]["culc"] == "tensor":
                self.setting[k]["mode"] = "sum"
            
            self.setting[k]["start_id"] = self.total_param_count
            # モデル情報の取得
            theta, self.setting[k]["lora_flag"] = self.load_model(self.setting[k]["model_a"])
            lora_flag = self.setting[k]["lora_flag"]
            if lora_flag is None:
                print(f"{k} is SD file")
            else:
                if self.setting[k]["mode"] != "sum":
                    print("LoRA/Lycoris使用時はmode=sumのみ有効となります")
                    self.setting[k]["mode"] = "sum"
                print(f"{k} is {lora_flag}")
            self.setting[k]["te_flags"], self.setting[k]["unet_flags"], self.setting[k]["key_flags"] = check_keys(theta, self.global_keys)
            # 
            if self.setting[k]["type"] == "elemental":
                my_param_count += list(self.setting[k]["key_flags"].values()).count(True)
            else:
                if self.setting[k]["type"] == "default":
                    if self.setting[k]["te_flags"][0]:
                        my_param_count += 1
                elif self.setting[k]["type"] == "full_split":
                    my_param_count += self.setting[k]["te_flags"][1:].count(True)
                my_param_count += self.setting[k]["unet_flags"].count(True)
            if self.setting[k]["culc"] == "tensor":
                my_param_count *= 2
            self.setting[k]["param_count"] = my_param_count
            print(f"探索パラメータ数: {my_param_count}")
            self.total_param_count += my_param_count
            del theta
        print(f"総探索パラメータ数: {self.total_param_count}")

        # スコアリングAI作成
        self.score_manager = Score_Manager(self.setting["score"])

        # 探索用パラメーター作成
        weights = {}
        if self.setting.get("algo", "bayse") == "bayse":
            for k in self.models_keys:
                if self.setting[k]["culc"] == "tensor" or self.setting[k]["mode"] in ["sum_twice", "triple_sum"]:
                    target_alphas = ["alpha", "beta"]
                    if self.setting[k]["culc"] == "tensor":
                        self.setting[k]["model_b_flag"] = False
                    else:
                        self.setting[k]["model_b_flag"] = True
                else:
                    target_alphas = ["alpha"]
                    if self.setting[k]["mode"] == "add_diff":
                        self.setting[k]["model_b_flag"] = True
                    else:
                        self.setting[k]["model_b_flag"] = False
                if self.setting[k]["type"] == "elemental":
                    for kk, v in self.setting[k]["key_flags"].items():
                        if v:
                            my_min = self.setting[k].get("global_min", self.setting[default_model_key]["global_min"])
                            my_max = self.setting[k].get("global_max", self.setting[default_model_key]["global_max"])
                            _kk = kk.replace(".", "_")
                            for tk in target_alphas:
                                my_min = self.setting[k].get(f"{tk}_min", self.setting[default_model_key].get(f"{tk}_min", my_min))
                                my_max = self.setting[k].get(f"{tk}_max", self.setting[default_model_key].get(f"{tk}_max", my_max))
                                target_name = f"{k}_{tk}_{_kk}"
                                weights[target_name] = (my_min, my_max)
                else:
                    # TextEnc
                    if self.setting[k]["type"] == "default":
                        if self.setting[k]["te_flags"][0]:
                            my_min = self.setting[k].get("global_min", self.setting[default_model_key]["global_min"])
                            my_max = self.setting[k].get("global_max", self.setting[default_model_key]["global_max"])
                            for tk in target_alphas:
                                my_min = self.setting[k].get(f"{tk}_min", self.setting[default_model_key].get(f"{tk}_min", my_min))
                                my_max = self.setting[k].get(f"{tk}_max", self.setting[default_model_key].get(f"{tk}_max", my_max))
                                target_name = f"{k}_{tk}_te"
                                weights[target_name] = (my_min, my_max)
                    elif self.setting[k]["type"] == "full_split":
                        for i, v in enumerate(self.setting[k]["te_flags"]):
                            if i == 0:
                                if (not v): break
                                continue
                            if v:
                                my_min = self.setting[k].get("global_min", self.setting[default_model_key]["global_min"])
                                my_max = self.setting[k].get("global_max", self.setting[default_model_key]["global_max"])
                                for tk in target_alphas:
                                    my_min = self.setting[k].get(f"{tk}_min", self.setting[default_model_key].get(f"{tk}_min", my_min))
                                    my_max = self.setting[k].get(f"{tk}_max", self.setting[default_model_key].get(f"{tk}_max", my_max))
                                    target_name = f"{k}_{tk}_te_{i-1}"
                                    weights[target_name] = (my_min, my_max)
                    # Unet
                    for i, v in enumerate(self.setting[k]["unet_flags"]):
                        if v:
                            my_min = self.setting[k].get("global_min", self.setting[default_model_key]["global_min"])
                            my_max = self.setting[k].get("global_max", self.setting[default_model_key]["global_max"])
                            for tk in target_alphas:
                                my_min = self.setting[k].get(f"{tk}_min", self.setting[default_model_key].get(f"{tk}_min", my_min))
                                my_max = self.setting[k].get(f"{tk}_max", self.setting[default_model_key].get(f"{tk}_max", my_max))
                                target_name = f"{k}_{tk}_unet_{i}"
                                weights[target_name] = (my_min, my_max)
        
        # Optimizer作成
        opt_seed = self.setting.get("seed", -1)
        if opt_seed == -1:
            import random
            opt_seed = random.randint(0, 2**31)
            print(f"set seed: {opt_seed}")
        self.bounds_transformer = SequentialDomainReductionTransformer()
        bounds_transformer_flag = self.setting.get("bounds_transformer", False)
        if bounds_transformer_flag:
            print("bounds_transformer_flagが有効です")
        self.optimizer = BayesianOptimization(
            f=self.sd_target_function,
            pbounds=weights,
            random_state=opt_seed,
            bounds_transformer=self.bounds_transformer if bounds_transformer_flag else None,
            allow_duplicate_points=self.setting.get("allow_duplicate_points", False),
        )

        # logロード
        self.iteration = 0
        self.best_rolling_score = -99999.
        if load_log_flag:
            print(f"load log: {log_files}")
            load_logs(self.optimizer, logs=log_files)
            self.best_rolling_score = self.optimizer.max["target"]
            print(f"load best score: {self.best_rolling_score}")

        self.start_logging()
        self.optimizer.subscribe(Events.OPTIMIZATION_STEP, self.logger)

        add_zero_serch = self.setting.get("add_zero_serch", False)
        if add_zero_serch:
            print(f"add_zero_serch: {add_zero_serch}")
            zero_params = {p: 0.0 for p in weights}
            self.optimizer.probe(params=zero_params, lazy=True)
        
        latin_hypercube_sampling_flag = self.setting.get("latin_hypercube_sampling", False)
        if latin_hypercube_sampling_flag:
            sampler = qmc.LatinHypercube(d=len(weights))
            samples = sampler.random(self.init_points-(add_zero_serch))
            l_bounds = [b[0] for b in weights.values()]
            u_bounds = [b[1] for b in weights.values()]
            scaled_samples = qmc.scale(samples, l_bounds, u_bounds)

            for sample in scaled_samples.tolist():
                params = {p: s for p, s in zip(weights, sample)}
                self.optimizer.probe(params=params, lazy=True)

            init_points = 0
        else:
            init_points = self.init_points - (add_zero_serch)

        # モデル作成
        self.unet_config = create_unet_diffusers_config(False)
        self.unet = UNet2DConditionModel(**self.unet_config).to("cpu")
        vae_file = self.setting.get("vae", None)
        self.vae_flag = False

        if self.tiny_vae:
            self.vae = AutoencoderTiny.from_pretrained("madebyollin/taesd").to(device="cpu", dtype=self.dtype)
            self.vae_flag = True
        else:
            if vae_file is not None:
                if os.path.isfile(vae_file):
                    self.vae = load_vae(vae_file, self.dtype)
                    self.vae_flag = True
        
        if not self.vae_flag:
            self.vae_config = create_vae_diffusers_config()
            self.vae = AutoencoderKL(**self.vae_config).to("cpu")
        cfg = CLIPTextConfig(
            vocab_size=49408,
            hidden_size=768,
            intermediate_size=3072,
            num_hidden_layers=12,
            num_attention_heads=12,
            max_position_embeddings=77,
            hidden_act="quick_gelu",
            layer_norm_eps=1e-05,
            dropout=0.0,
            attention_dropout=0.0,
            initializer_range=0.02,
            initializer_factor=1.0,
            pad_token_id=1,
            bos_token_id=0,
            eos_token_id=2,
            model_type="clip_text_model",
            projection_dim=768,
            torch_dtype="float32",
        )
        self.text_model = CLIPTextModel._from_config(cfg)
        self.tokenizer = CLIPTokenizer.from_pretrained("openai/clip-vit-large-patch14")

        self.optimizer.maximize(
            init_points=init_points,
            n_iter=self.n_iters,
        )

        scores = self.parse_scores(self.optimizer.res)
        convergence_plot(scores, self.log_plt)
        best_param = self.optimizer.max["params"]
        print("\n" + "-"*10 + "best params" + "-"*10 + "\n")
        best_target = self.optimizer.max["target"]
        print(f"best score: {best_target}")
        print(self.create_param_logstr(best_param))

        if self.setting.get("output_best", True):
            print(f"\noutput model to {self.model_output_name}")
            best_theta = self.merge_model(best_param)
            if self.setting["precision"] == "fp16":
                for k in best_theta.keys():
                    best_theta[k] = best_theta[k].half()
                metadata = {"format": "pt", "precision": "fp16"}
            else:
                metadata = {"format": "pt"}

            safetensors.torch.save_file(
                best_theta,
                self.model_output_name,
                metadata=metadata,
            )
        
        return

    def start_logging(self) -> None:
        self.logger = JSONLogger(path=f"{self.log_dir_dir}/{self.log_name}")
    
    def sd_target_function(self, **params):
        self.iteration += 1
        if self.iteration == 1:
            print("\n" + "-" * 10 + " warmup " + "-" * 10 + ">")
        elif self.iteration == self.init_points + 1:
            print("\n" + "-" * 10 + " optimisation " + "-" * 10 + ">")

        it_type = "warmup" if self.iteration <= self.init_points else "optimisation"
        print(f"\n{it_type} - Iteration: {self.iteration}")

        state_dict = self.merge_model(params)

        # Unetを読み込む
        converted_unet_checkpoint = convert_ldm_unet_checkpoint(False, state_dict, self.unet_config)
        info = self.unet.load_state_dict(converted_unet_checkpoint)
        train_util.replace_unet_modules(self.unet, self.mem_eff_attn, self.xformers, self.sdpa)
        print("loading unet:", info)

        # VAEを読み込む
        if not self.vae_flag:
            converted_vae_checkpoint = convert_ldm_vae_checkpoint(state_dict, self.vae_config)
            info = self.vae.load_state_dict(converted_vae_checkpoint)
            if torch.__version__ >= "2.0.0":  # PyTorch 2.0.0 以上対応のxformersなら以下が使える
                self.vae.set_use_memory_efficient_attention_xformers(True)
            self.vae_flag = True
            print("loading vae:", info)

        # TextEncoderを読み込む
        converted_text_encoder_checkpoint = convert_ldm_clip_checkpoint_v1(state_dict)
        info = self.text_model.load_state_dict(converted_text_encoder_checkpoint)
        print("loading text enc:", info)

        self.unet.to(self.device, dtype=self.dtype)
        self.vae.to(self.device, dtype=self.dtype)
        self.text_model.to(self.device, dtype=self.dtype)
        self.unet.eval()
        self.vae.eval()
        self.text_model.eval()
        self.lcm_flag = self.stream_flag

        images = self.create_img()

        self.unet.to("cpu")
        self.text_model.to("cpu")
        scores = None
        scores = self.score_manager.batch_score(images, self.iteration)
        avg_score = self.score_manager.average_score(scores)
        print(f"{'-'*10}\nRun score: {avg_score}")

        print("\n---merge info---\n")
        merge_log_str = self.create_param_logstr(params)
        print(merge_log_str)

        if self.best_rolling_score < avg_score:
            self.best_rolling_score = avg_score
            best_fn = f"{self.log_dir_dir}/{os.path.splitext(self.log_name)[0]}.txt"
            with open(best_fn, mode="w", encoding="utf-8") as f:
                f.write(merge_log_str)
                        
        return avg_score
    
    def create_param_logstr(self, params):
        logstr = ""
        for target_key in self.models_keys:
            beta_flag = False
            if self.setting[target_key]["culc"] == "tensor" or self.setting[target_key]["mode"] in ["sum_twice", "triple_sum"]:
                beta_flag = True
            logstr += f"[{target_key}]\n"
            if self.setting[target_key]["type"] == "elemental":
                pass
            elif self.setting[target_key]["type"] == "default":
                if self.setting[target_key]["te_flags"][0]:
                    target_name_a = f"{target_key}_alpha_te"
                    target_name_b = f"{target_key}_beta_te"
                    logstr += f"Text Encoder: {params[target_name_a]}"
                    if target_name_b in params:
                        logstr += f"\t(beta): {params[target_name_b]}"
                    logstr += "\n"
            elif self.setting[target_key]["type"] == "full_split":
                logstr += f"Text Encoder: ["
                logstr_b = f"beta: ["
                for i, v in enumerate(self.setting[target_key]["te_flags"]):
                    if i==0: continue
                    if v:
                        target_name_a = f"{target_key}_alpha_te_{i-1}"
                        target_name_b = f"{target_key}_beta_te_{i-1}"
                        logstr += f"{params[target_name_a]:.5f}"
                        if target_name_b in params:
                            logstr_b += f"{params[target_name_b]:.5f}"
                    else:
                        logstr += "0.0"
                        if beta_flag:
                            logstr_b += "0.0"
                    if i==12:
                        logstr += "]\n"
                        logstr_b += "]\n"
                    else:
                        logstr += ", "
                        logstr_b += ", "
                if beta_flag:
                    logstr += logstr_b
            if self.setting[target_key]["type"] in ["default", "full_split"]:
                logstr += f"Unet: ["
                logstr_b = "beta: ["
                for i, v in enumerate(self.setting[target_key]["unet_flags"]):
                    if v:
                        target_name_a = f"{target_key}_alpha_unet_{i}"
                        target_name_b = f"{target_key}_beta_unet_{i}"
                        logstr += f"{params[target_name_a]:.05f}"
                        if target_name_b in params:
                            logstr_b += f"{params[target_name_b]:.05f}"
                    else:
                        logstr += "0.0"
                        if beta_flag:
                            logstr_b += "0.0"
                    if i + 1 < len(self.setting[target_key]["unet_flags"]):
                        logstr += ","
                        logstr_b += ","
                logstr += "]\n"
                logstr_b += "]\n"
                if beta_flag:
                    logstr += logstr_b
            logstr += "\n"
        return logstr
    
    def parse_scores(self, iterations: list[dict]) -> list[float]:
        return [r["target"] for r in iterations]

    def create_img(self):
        prompts_args = {}
        for txt_path in self.prompts_paths:
            with open(txt_path, mode="r", encoding="utf-8") as f:
                now_prompts = toml.load(f)
            now_keys = list(now_prompts.keys())
            if "global" in now_keys:
                now_keys.remove("global")
            for now_key in now_keys:
                target_key = f"{os.path.split(os.path.splitext(txt_path)[0])[-1]}_{now_key}"
                prompts_args[target_key] = {}
                prompts_args[target_key]["prompt"] = now_prompts[now_key].get("prompt", now_prompts["global"].get("prompt", ",") if "global" in now_prompts else ",")
                prompts_args[target_key]["neg_prompt"] = now_prompts[now_key].get("neg_prompt", now_prompts["global"].get("neg_prompt", ",") if "global" in now_prompts else ",")
                prompts_args[target_key]["w"] = now_prompts[now_key].get("w", now_prompts["global"].get("w", 512) if "global" in now_prompts else 512)
                prompts_args[target_key]["h"] = now_prompts[now_key].get("h", now_prompts["global"].get("h", 512) if "global" in now_prompts else 512)
                prompts_args[target_key]["seed"] = now_prompts[now_key].get("seed", now_prompts["global"].get("seed", -1) if "global" in now_prompts else -1)
                prompts_args[target_key]["guidance_scale"] = now_prompts[now_key].get("guidance_scale", now_prompts["global"].get("guidance_scale", 7.5) if "global" in now_prompts else 7.5)
                prompts_args[target_key]["step"] = now_prompts[now_key].get("step", now_prompts["global"].get("step", 20) if "global" in now_prompts else 20)
                prompts_args[target_key]["clip_skip"] = now_prompts[now_key].get("clip_skip", now_prompts["global"].get("clip_skip", None) if "global" in now_prompts else None)
                prompts_args[target_key]["sampler"] = now_prompts[now_key].get("sampler", now_prompts["global"].get("sampler", "euler_a") if "global" in now_prompts else "euler_a")
                prompts_args[target_key]["batch_size"] = now_prompts[now_key].get("batch_size", now_prompts["global"].get("batch_size", 1) if "global" in now_prompts else 1)
                prompts_args[target_key]["count"] = now_prompts[now_key].get("count", now_prompts["global"].get("count", 1) if "global" in now_prompts else 1)
                if "add_prompt" in now_prompts[now_key]:
                    prompts_args[target_key]["prompt"] = prompts_args[target_key]["prompt"] + ", " + now_prompts[now_key]["add_prompt"]
        
        images = {}
        for now_key, value in prompts_args.items():
            images[now_key] = {"images": self.make_images(value), "prompt": value["prompt"]}
            self.stream_flag = False

        return images

    def merge_model(self, params):
        MAX_TOKENS = 77
        NUM_INPUT_BLOCKS = 12
        NUM_MID_BLOCK = 1
        NUM_OUTPUT_BLOCKS = 12
        NUM_TOTAL_BLOCKS = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + NUM_OUTPUT_BLOCKS
        NUM_TE_BLOCKS = 12
        # ベースモデル読み込み
        base_theta = self.load_model(self.setting["base"])[0]
        for target_model_key in self.models_keys:
            # モデル読み込み
            theta_a, lora_a_flag = self.load_model(self.setting[target_model_key]["model_a"])
            if self.setting[target_model_key]["model_b_flag"]:
                theta_b, lora_b_flag = self.load_model(self.setting[target_model_key]["model_b"])
            else:
                theta_b = None
                lora_b_flag = None
            lora_scale = None
            if lora_a_flag is not None:
                lora_scale = self.setting[target_model_key].get("tensor_lora_scale", 1.0)
            
            for key, value in tqdm(base_theta.items()):
                if "first_stage_model" in key: continue
                if "cond_stage_model" in key and "embeddings" in key: continue
                if self.setting[target_model_key]["type"] == "elemental":
                    _kk = key.replace(".", "_")
                    target_name_a = f"{target_model_key}_alpha_{_kk}"
                    target_name_b = f"{target_model_key}_beta_{_kk}"
                else:
                    if "cond_stage_model" in key:
                        if self.setting[target_model_key]["type"] == "full_split":
                            if "final_layer_norm" in key:
                                weight_index = 11
                            else:
                                re_te = re.compile(r"\.encoder.layers\.(\d+)\.") #12
                                m = re_te.search(key)
                                weight_index = int(m.groups()[0])
                            target_name_a = f"{target_model_key}_alpha_te_{weight_index}"
                            target_name_b = f"{target_model_key}_beta_te_{weight_index}"
                        else:
                            target_name_a = f"{target_model_key}_alpha_te"
                            target_name_b = f"{target_model_key}_beta_te"
                    elif "model.diffusion_model." in key:
                        weight_index = -1

                        re_inp = re.compile(r"\.input_blocks\.(\d+)\.")  # 12
                        re_mid = re.compile(r"\.middle_block\.(\d+)\.")  # 1
                        re_out = re.compile(r"\.output_blocks\.(\d+)\.")  # 12

                        if "time_embed" in key:
                            weight_index = 0  # before input blocks
                        elif ".out." in key:
                            weight_index = NUM_TOTAL_BLOCKS - 1  # after output blocks
                        elif m := re_inp.search(key):
                            weight_index = int(m.groups()[0])
                        elif re_mid.search(key):
                            weight_index = NUM_INPUT_BLOCKS
                        elif m := re_out.search(key):
                            weight_index = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + int(m.groups()[0])

                        if weight_index >= NUM_TOTAL_BLOCKS:
                            raise ValueError(f"illegal block index {key}")
                        target_name_a = f"{target_model_key}_alpha_unet_{weight_index}"
                        target_name_b = f"{target_model_key}_beta_unet_{weight_index}"
                    else:
                        continue
                if target_name_a in params:
                    if key in theta_a:
                        base_theta[key] = self.merge_block(
                                            value,
                                            theta_a[key],
                                            theta_b[key] if theta_b is not None else None,
                                            params[target_name_a],
                                            params[target_name_b] if target_name_b in params else None,
                                            lora_a_flag,
                                            lora_b_flag,
                                            target_model_key,
                                            lora_scale,
                                            )
        return base_theta


    def merge_block(self, theta_0, theta_1, theta_2, alpha, beta, lora_a_flag, lora_b_flag, target_model_key, lora_scale=None):
        if lora_a_flag is not None:
            if lora_a_flag != "ia3":
                if theta_0.shape != theta_1.shape:
                    theta_1 = theta_1.reshape(theta_0.shape)
        if self.setting[target_model_key]["culc"] == "tensor":
            dim = theta_0.dim()
            if lora_a_flag is not None:
                if lora_a_flag == "ia3":
                    _theta_1 = theta_0 + lora_scale * (theta_0*theta_1)
                else:
                    _theta_1 = theta_0 + lora_scale * theta_1
            else:
                _theta_1 = theta_1
            if alpha + beta <= 1:
                alpha_s = int(theta_0.shape[0] * (beta))
                alpha_e = int(theta_0.shape[0] * (alpha + beta))
                new_theta = theta_0.clone()
                if dim == 1:
                    new_theta[alpha_s:alpha_e] = _theta_1[alpha_s:alpha_e].clone()
                elif dim == 2:
                    new_theta[alpha_s:alpha_e, :] = _theta_1[alpha_s:alpha_e, :].clone()
                elif dim == 3:
                    new_theta[alpha_s:alpha_e, :, :] = _theta_1[alpha_s:alpha_e, :, :].clone()
                elif dim == 4:
                    new_theta[alpha_s:alpha_e, :, :, :] = _theta_1[alpha_s:alpha_e, :, :, :].clone()
            else:
                alpha_s = int(theta_0.shape[0] * (alpha + beta-1))
                alpha_e = int(theta_0.shape[0] * (beta))
                new_theta = _theta_1.clone()
                if dim == 1:
                    new_theta[alpha_s:alpha_e] = theta_0[alpha_s:alpha_e].clone()
                elif dim == 2:
                    new_theta[alpha_s:alpha_e, :] = theta_0[alpha_s:alpha_e, :].clone()
                elif dim == 3:
                    new_theta[alpha_s:alpha_e, :, :] = theta_0[alpha_s:alpha_e, :, :].clone()
                elif dim == 4:
                    new_theta[alpha_s:alpha_e, :, :, :] = theta_0[alpha_s:alpha_e, :, :, :].clone()
            return new_theta
        
        elif self.setting[target_model_key]["mode"] == "sum":
            if lora_a_flag is None:
                return (1.-alpha) * theta_0 + alpha * theta_1
            elif lora_a_flag == "ia3":
                return theta_0 + alpha * (theta_0*theta_1)
            else:
                return theta_0 + alpha * (theta_1)
        elif self.setting[target_model_key]["mode"] == "add_diff":
            return (1.-alpha) * theta_0 + alpha * (theta_1 - theta_2)
        elif self.setting[target_model_key]["mode"] == "sum_twice":
            return (1.-alpha) * theta_0 + alpha * ((1.-beta) * theta_1 + beta * theta_2)
        elif self.setting[target_model_key]["mode"] == "triple_sum":
            return (1.-alpha-beta) * theta_0 + alpha * theta_1 + beta * theta_2
                    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--toml", type=str, default=None)
    args = parser.parse_args()

    trainer = Auto_Merger()
    trainer.merge(args)

    print("\ndone")

'''
# toml書式
# 行頭に#をつけることによってコメントアウトが可能です

name = "名称、output_dir/name_best.safetensorsやoutput_dir/name_last.safetensorsのような名称で保存されます"

algo = "bayse" # bayseのみ
init_points = 10 # 探索準備の回数
n_iters = 10 # 探索回数
seed = -1 # optimizerのシード値
allow_duplicate_points = False # 同じ設定の値の探索を許可するか？（seed=-1にした場合など同じ設定でも評価が変わるのでこれをTrueにしたほうがいい、その分最適解までの探索回数は増える）
bounds_transformer = False
latin_hypercube_sampling = False

base = "ベースモデル"
vae = "VAEを別個に指定する場合"
device = "cuda"
attn = "xformers" # xformers or sdpa or mem_eff_attn

precision = "fp16" # ファイル保存時のデータ型
output_dir = "" # モデルファイルの保存先、指定がない場合はベースモデルと同じディレクトリが指定されます
output_log = "" # ログファイルの出力先
output_best = True # ベストスコアになったモデルを出力する（通常時は指定不要）

logs = [
    "続きから自動マージの探索をする場合に使う、logのjsonファイル"
]

[score]
    algo = "rf" # laion, aes, cafe_aesthetic, cafe_style, cafe_waifu, rf, rf1, rf2, rf3
    model_file = "スコアモデルのファイル"
    threshold = 0.5 # スコア評価に減衰式を使う場合の減衰基準
    attenuation = 0.5 # 減衰のかかり方
    device = "cuda"
    save_image = true
    save_img_score = 0.5 # ここで指定したスコア以上の画像のみを保存する

[model_a]
    mode = "sum" # sum, add_diff, sum_twice, triple_sum (LoRA/Lycorisではsumのみサポート)
    culc = "default" # default, cosin, tensor(cosineはまだ対応してないです)
    type = "default" # default, full_split, elemental (full_splitはテキストエンコーダーも層別に探索する)
    model_a = "マージするモデル"
    model_b = "add_diff/sumtwice/triple_sum時に使うもう一つのモデル
    tensor_lora_scale = 1.0 # culcにtensor指定時かつマージするモデルがLoRA/Lycorisの場合にベースモデルにここで指定した割合でマージしてからマージ処理を行います。式は下記参照

    add_zero_serch = false
    global_min = 0.0 # 探索する最小値
    global_max = 1.0 # 探索する最大値
    alpha_min = 0.0 # 探索するalpha値の最小値
    alpha_max = 1.0 # 探索するalpha値の最大値
    beta_min = 0.0 # 探索するbeta値の最小値(betaはculc=tensor時やmode=sum_twice, triple_sum時に使われます。式は下記参照)
    beta_max = 1.0 # 探索するbeta値の最大値

[model_b]
    [model_a]と同じ
    設定省略した個所は[model_a]の設定を引き継ぐ
    [model_a]がない場合は最初に取得されたmodel_xxxの設定を引き継ぐ

# [model_から始まれば自由]
#    [model_a]と同じ
    
[prompts]
    file0 = "書式のファイルについてはprompt設定ファイルのpromptサンプル.tomlファイルを参照"
    file1 = "複数ファイルに分けたい場合用"
    # ここの名称も自由: "ファイルパス"


# prompt設定ファイルの書式
# globalの設定は同ファイル内でのみ有効、また[global]を省略しても構わない、ただし個別のprompt設定で指定されなかった内容はデフォ値が適用される
[global]
    prompt = "illust"
    neg_prompt = "lowres"
    step = 15
    seed = -1
    w = 512
    h = 512
    sampler = "euler_a"
    batch_size = 1
    clip_skip = 1
    guidance_scale = 7.5
[prompt_name]
    # globalから変更したい内容だけ記述すればいい
    prompt = "photo"
    add_prompt = "1girl" # promptの末尾に追加するワード[global]にはない個別設定用の指定です
    w = 576
    seed = 100
# [ここの名称は自由に]
#    add_prompt = "1girl"

# promptとadd_promptの違い
#   [prompt_name]の場合
#   promptで設定した内容にadd_promptの内容が追加される
#   prompt = "photo, 1girl"で生成される
#   [ここの名称は自由に]の場合
#   [global]で設定したpromptにadd_promptの内容が追加される
#   prompt = "illust, 1girl"で生成される
    
# 計算式について
# mode="sum" 通常のマージ式: (1-alpha) * base + alpha * model_a
# mode="add_diff" 差分加算マージ: base + alpha * (model_a-model_b)
# mode="sum_twice" 2回加算マージ: (1-alpha) * base + (alpha * ((1-beta) * model_a + beta * model_b))
# mode="triple_sum" 3つ同時マージ: (1-alpha-beta) * base + alpha * model_a + beta * model_b

# culc = "default" # default, cosin, tensor supermergerにあるのと同じ
# LoRA/Lycorisでtensor指定時は以下の式になります
# tensor_merge(base, (base + tensor_lora_scale*lora)) # lora/lycorisの重みと直接tensor入れ替えは行えないので一度ベースに指定されたモデルにマージしてからtensor入れ替え処理を行います

# type = "default" # default, full_split, elemental (full_splitはテキストエンコーダーも層別に探索する)
# default = 通常の層別マージ(textEncoder, Unet*25層)の計26層
# full_split = 通常の層別に加えてTextEncoderも層別にマージ(TextEncoder*12層, Unet*25層)の計37層
# elemental = supermergerにあるモジュール毎に比率を変えるやつ（層の数が多すぎて自動マージでも試行回数がそれなりに必要となるため大変）
'''