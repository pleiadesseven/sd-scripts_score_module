# os.environ["CUDA_VISIBLE_DEVICES"] = "0"    # choose GPU if you are on a multi GPU server
import numpy as np
import torch
import pytorch_lightning as pl
import torch.nn as nn
from torchvision import datasets, transforms
# import tqdm

from os.path import join
# from datasets import load_dataset
import glob
import os
import shutil
import tqdm
import argparse
import clip
from PIL import Image, ImageFile
from score_module.musiq_module import Create_MUSIQ_SCORE_Model, get_resize_latents, get_property, print_dict, collate_fn, load_img
from common_modules import dirdialog_clicked, filedialog_clicked

parser = argparse.ArgumentParser(description="run")
parser.add_argument("--html", action='store_false', help="output html")
parser.add_argument("--auto_dir_step", type=float, nargs="*", default=[2.0, 3.0, 3.5, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0])
parser.add_argument("--auto_dir_path", type=str, default="output_score_musiq")
parser.add_argument("--pth_path", type=str, default="score_module/musiq_model")
parser.add_argument("--img_dir", type=str, default="testset")
parser.add_argument("--w_num", type=int, default=6)
parser.add_argument("--version", type=int, default=0)
parser.add_argument("--log_only", action="store_true")
parser.add_argument("--log", action="store_true")
parser.add_argument("--log_name", type=str, default=None)
parser.add_argument("--not_rename", action="store_true")

args = parser.parse_args()
print(f"モデル ver：({args.version})")
if args.img_dir is None:
    print("選別対象となるディレクトリを指定してください")
    args.img_dir = dirdialog_clicked()
if args.pth_path is None:
    print("読み込むモデルファイルを選択してください")
    args.pth_path = filedialog_clicked([".pth"])
    args.pth_path = os.path.splitext(args.pth_path)[0]
if args.log_only:
    args.log = True
    args.html = False
if args.log:
    if os.path.isdir("./score_logs"):
        log_number = len(glob.glob("./score_logs/*"))
    else:
        log_number = 0
    if args.log_name == None:
        args.log_name = os.path.split(args.img_dir)[-1]
    import torch.utils.tensorboard.writer as tensorboard_writer
    log_board = tensorboard_writer.SummaryWriter(f"score_logs")
    score_logs = []
    print(f"logname:{args.log_name}")

# This script will predict the aesthetic score for this image file:

img_dir = args.img_dir  # 自動分類対象のフォルダ
img_path_list = glob.glob(os.path.join(img_dir, "*.png")) + glob.glob(os.path.join(img_dir, "*.jpg")) + glob.glob(os.path.join(img_dir, "*.webp"))
auto_dir_base = args.auto_dir_path  # 自動分類する時の出力先
auto_dir_flag = args.html  # Trueの時はhtmlは生成しない
auto_dir_steps = args.auto_dir_step  # フォルダに分類する時のスコアステップ、0.5なら[1.0,1.5,2.0,.....9.5]と0.5刻みでスコアごとに分類する、0.4なら[1.0,5.0,9.0]の三段階、複数値を指定した時は指定したスコアで分類する
auto_dir_step = auto_dir_steps[0]
auto_dir_list = []
html_w_num = args.w_num  # html出力時に横列の数
print(f"target filecount: {len(img_path_list)}")

if auto_dir_flag:
    if not os.path.exists(auto_dir_base):
        # os.mkdir(auto_dir_base)
        os.makedirs(auto_dir_base, exist_ok=True)
    if len(auto_dir_steps) == 1:
        for i in range(int(10 / auto_dir_step)):
            key = 1.0 + (i * auto_dir_step)
            auto_dir_list.append(f"{auto_dir_base}/{key:.1f}")
    else:
        auto_dir_list.append(f"{auto_dir_base}/1.0")
        for i in range(len(auto_dir_steps)):
            key = auto_dir_steps[i]
            auto_dir_list.append(f"{auto_dir_base}/{key:.1f}")
        print(auto_dir_list)

# if you changed the MLP architecture during training, change it also here:
model_params, dataset_params, train_params = get_property(version=args.version, mode="musiq_score", model_name=args.pth_path)
pth_path = train_params["model_name"]
if not os.path.isfile(pth_path):
    pth_path = args.pth_path
if os.path.splitext(pth_path)[-1] == "":
    pth_path = f"{pth_path}.pth"
pre_encoder, model = Create_MUSIQ_SCORE_Model(**model_params)
model.load_state_dict(torch.load(f"{pth_path}"))

device = "cuda" if torch.cuda.is_available() else "cpu"

pre_encoder.to(device)
pre_encoder.eval()
pre_encoder.requires_grad_(False)
model.to(device)
model.eval()
model.requires_grad_(False)

output_str_tr = ""
output_str_img = ""
output_str_score = ""
total_score = 0.0
score_count = 0
i = 0
for img_path in tqdm.tqdm(img_path_list):
    pil_image = load_img(img_path)
    latents_org, w_org, h_org = get_resize_latents(pre_encoder, device, pil_image, dataset_params["reso_size"])
    latents_sc1, w_sc1, h_sc1 = get_resize_latents(pre_encoder, device, pil_image, dataset_params["scale_list"][0])
    latents_sc2, w_sc2, h_sc2 = get_resize_latents(pre_encoder, device, pil_image, dataset_params["scale_list"][1])

    latents_org = latents_org.to(device)
    latents_sc1 = latents_sc1.to(device)
    latents_sc2 = latents_sc2.to(device)
    
    prediction = model(latents_org, latents_sc1, latents_sc2)
    score = prediction.data.cpu()[0][0]
    if args.log:
        score_logs.append(score)
    total_score += score
    score_count += 1

    if auto_dir_flag:
        for j, f in enumerate(auto_dir_list):
            if len(auto_dir_steps) == 1:
                min = 1.0 + ((j + 1) * auto_dir_step)
                max = (j + 1) * auto_dir_step
            else:
                if j >= len(auto_dir_steps):
                    min = 10.0
                    max = 11.0
                else:
                    min = auto_dir_steps[j]
                    max = auto_dir_steps[j]
            if prediction < min or (max > 10.0):
                if not os.path.exists(f):
                    os.mkdir(f)
                score_s = f"{score:02.3f}".replace(".", "_")
                if args.not_rename:
                    copy_path = f"{f}/{os.path.basename(img_path)}"
                else:
                    copy_path = f"{f}/{score_s}_{os.path.basename(img_path)}"
                excount = 0
                while True:
                    if os.path.exists(copy_path):
                        if args.not_rename:
                            copy_path = f"{f}/{excount}_{os.path.basename(img_path)}"
                        else:
                            copy_path = f"{f}/{score_s}_{excount}_{os.path.basename(img_path)}"
                    else:
                        break
                    excount += 1
                shutil.move(img_path, copy_path)
                txt_path = os.path.splitext(img_path)[0] + ".txt"
                copy_path = os.path.splitext(copy_path)[0] + ".txt"
                if os.path.isfile(txt_path):
                    shutil.move(txt_path, copy_path)
                break
    elif args.log_only:
        pass
    else:
        if i % html_w_num == 0:
            if i > 0:
                output_str_tr += f"<tr>{output_str_img}</tr><tr>{output_str_score}</tr>"
            output_str_img = f"<td><img src=\"{img_path}\"></td>"
            output_str_score = f"<td>{score}</td>"
        else:
            output_str_img += f"<td><img src=\"{img_path}\"></td>"
            output_str_score += f"<td>{score}</td>"
    i += 1

if args.log:
    print("output tensorboard log")
    log_board.add_histogram(f"{os.path.split(pth_path)[-1]}/[{log_number:03}]{args.log_name}_{total_score/score_count:.04f}", torch.tensor(score_logs).unsqueeze(0))
    for i in range(score_count//1000):
        log_board.add_histogram(f"{os.path.split(pth_path)[-1]}/total[{(i+1)*1000}]", torch.tensor(score_logs[:(i+1)*1000]).unsqueeze(0), log_number)
    log_board.close()
elif not auto_dir_flag:
    output_str_tr += f"<tr>{output_str_img}</tr><tr>{output_str_score}</tr>"

    output_str = "<html><head><style>table{text-align:center;}\nimg{width:50%;height:50%;max-widht:240px;max-hight:240px;}</style></head><body><table>"
    output_str += output_str_tr
    output_str += "</table></body></html>"

    output_file = open('prev.html', 'w')
    output_file.write(output_str)

print("done!!")
print(f"score mean: {total_score/score_count:.04f}")
