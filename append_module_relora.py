import os
import torch
import argparse
import math
import tqdm
from torch.optim.lr_scheduler import LambdaLR

from functools import partial

##############################################################################################################
# ReLoRA
# PreTrainにLoRAを活用できるとかなんとかっていう仕組み
# 雑にマージとリセットするだけでいいらしい
# 
def add_append_relora_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("--relora_enable", action="store_true", help="ReLoRAを有効にします")
    parser.add_argument("--relora_step", type=int, default=None, help="ReLoRAをstep単位で行いたい場合にstepを指定、指定がない場合はepoch毎に行う")
    parser.add_argument("--relora_multiplier", type=float, default=None)
    parser.add_argument("--relora_dim", type=int, default=None)
    parser.add_argument("--relora_conv_dim", type=int, default=None)
    parser.add_argument("--relora_lr_scheduler", action="store_true", help="ReLoRA用のLrSchedulerを使用する")
    parser.add_argument("--relora_restart_warmup_steps", type=int, default=None, help="ReLoRA更新後に再度warm_upをするstep数")

##############################################################################################################
# ここから下は特に説明ない

def get_relora_args(args, net_kwargs):
    relora_multiplier = args.relora_multiplier
    if relora_multiplier is None:
        relora_multiplier = 1.0
    relora_dim = args.relora_dim
    if relora_dim is None:
        relora_dim = args.network_dim
    relora_conv_dim = args.relora_conv_dim
    if relora_conv_dim is None:
        relora_conv_dim = relora_dim
        if relora_conv_dim is None:
            relora_conv_dim = int(net_kwargs.get("conv_dim", args.network_dim))
    return relora_multiplier, relora_dim, relora_conv_dim

@torch.no_grad()
def random_pruning_(tensor, prune_ratio):
    """
    Performs random pruning dimensionality reduction **inplace**.
    Only reduces the inner dimensionality, does not affect the shape of the tensor
    """
    random_pruning_mask = torch.rand_like(tensor) > prune_ratio
    tensor.mul_(random_pruning_mask)

def get_optimizer_param(optimizer_type):
    if optimizer_type == "":
        return ["exp_avg", "exp_avg_sq"]
    if optimizer_type.lower()=="AdamW".lower():
        return ["exp_avg", "exp_avg_sq"]
    elif optimizer_type.lower()=="Lion".lower():
        return ["exp_avg"]
    elif optimizer_type.lower()=="Adafactor".lower():
        return ["exp_avg", "exp_avg_sq_row", "exp_avg_sq_col", "exp_avg_sq"]
    return None

    #lora_params = [p for n, p in model.named_parameters() if p.requires_grad and "lora_" in n]
    
def reset_optimizer(optimizer, reset_params, opt_keys):
    pruning_fn = partial(random_pruning_, prune_ratio=0.999)
    n_zeros = 0
    n_total = 0

    optimizer_state = optimizer.state
    
    for p in reset_params:
        param_state = optimizer_state[p]
        if len(param_state) == 0: # no state for this param, happens for ZeRo optimizer
            continue
        for key in opt_keys:
            pruning_fn(param_state[key])  # pruning fn has to be inplace to keep the same keys in the dict
            n_total += param_state[key].numel()
            n_zeros += torch.sum(param_state[key] == 0).item()

def reset_lora(loras):
    for key, value in loras.items():
        if "bias" in key:
            torch.nn.init.zeros_(value)
        elif "norm" in key:
            if "w_norm" in key:
                torch.nn.init.zeros_(value)
            elif "b_norm" in key:
                torch.nn.init.zeros_(value)
        elif "hada" in key:
            if "hada_w1_b" in key:
                torch.nn.init.normal_(value, std=1)
            elif "hada_w1_a" in key:
                torch.nn.init.normal_(value, std=0.1)
            elif "hada_w2_b" in key:
                torch.nn.init.normal_(value, std=1)
            elif "hada_w2_a" in key:
                torch.nn.init.constant_(value, 0)
        elif "lora" in key:
            if "lora_down" in key:
                torch.nn.init.kaiming_uniform_(value, a=math.sqrt(5))
            elif "lora_up" in key:
                torch.nn.init.zeros_(value)

def create_lora(lora_network_o, lora_network_t, dim, conv_dim, device):
    diffs = {}
    bias_diff = {}
    norm_b_diff = {}
    norm_flags = {}
    bias_flags = {}
    text_encoder_different = False
    min_diff=0.01
    clamp_quantile=0.99
    for i, (lora_o, lora_t) in enumerate(zip(lora_network_o.text_encoder_loras, lora_network_t.text_encoder_loras)):
        lora_name = lora_o.lora_name
        if (lora_o.org_module.__class__.__name__ == "list"):
            module_t = lora_t.org_module[0]
            module_o = lora_o.org_module[0]
        else:
            module_t = lora_t.org_module
            module_o = lora_o.org_module
        is_norm = (module_o.__class__.__name__ == "LayerNorm") or (module_o.__class__.__name__ == "GroupNorm")
        target_dtype = module_t.weight.dtype
        diff = module_t.weight - module_o.weight.to(device)
        diff = diff.float()
        diffs[lora_name] = diff
        # Text Encoder might be same
        if not text_encoder_different and torch.max(torch.abs(diff)) > min_diff:
            text_encoder_different = True
            print(f"Text encoder is different. {torch.max(torch.abs(diff))} > {min_diff}")

        if is_norm:
            norm_flags[lora_name] = True
            norm_b_diff[lora_name] = (module_t.bias - module_o.bias.to(device)).float()
        else:
            norm_flags[lora_name] = False
            if module_o.bias is None:
                bias_flags[lora_name] = False
            else:
                bias_flags[lora_name] = True
                bias_diff[lora_name] = (module_t.bias - module_o.bias.to(device)).float()
    if not text_encoder_different:
        print("Text encoder is same. Extract U-Net only.")
        lora_network_o.text_encoder_loras = []
        diffs = {}

    for i, (lora_o, lora_t) in enumerate(zip(lora_network_o.unet_loras, lora_network_t.unet_loras)):
        lora_name = lora_o.lora_name
        if (lora_o.org_module.__class__.__name__ == "list"):
            module_t = lora_t.org_module[0]
            module_o = lora_o.org_module[0]
        else:
            module_t = lora_t.org_module
            module_o = lora_o.org_module
        is_norm = (module_o.__class__.__name__ == "LayerNorm") or (module_o.__class__.__name__ == "GroupNorm")
        diff = module_t.weight - module_o.weight.to(device)
        diff = diff.float()
        diffs[lora_name] = diff
        if is_norm:
            norm_flags[lora_name] = True
            norm_b_diff[lora_name] = (module_t.bias - module_o.bias.to(device)).float()
        else:
            norm_flags[lora_name] = False
            if module_o.bias is None:
                bias_flags[lora_name] = False
            else:
                if torch.max(torch.abs((module_t.bias - module_o.bias.to(device)).float())) < 0.00000001:
                    bias_flags[lora_name] = False
                else:
                    bias_flags[lora_name] = True
                    bias_diff[lora_name] = (module_t.bias - module_o.bias.to(device)).float()

    # make LoRA with svd
    print("calculating by svd")
    lora_weights = {}
    _count = 0
    _total_count = len(diffs)
    with torch.no_grad():
        for lora_name, mat in diffs.items():
            _count += 1
            print(f"\r{_count}/{_total_count}", end="")
            if norm_flags[lora_name]: continue
            # if conv_dim is None, diffs do not include LoRAs for conv2d-3x3
            conv2d = len(mat.size()) == 4
            kernel_size = None if not conv2d else mat.size()[2:4]
            conv2d_3x3 = conv2d and kernel_size != (1, 1)

            rank = dim if not conv2d_3x3 or conv_dim is None else conv_dim
            out_dim, in_dim = mat.size()[0:2]

            # print(lora_name, mat.size(), mat.device, rank, in_dim, out_dim)
            rank = min(rank, in_dim, out_dim)  # LoRA rank cannot exceed the original dim

            if conv2d:
                if conv2d_3x3:
                    mat = mat.flatten(start_dim=1)
                else:
                    mat = mat.squeeze()

            U, S, Vh = torch.linalg.svd(mat)

            U = U[:, :rank]
            S = S[:rank]
            U = U @ torch.diag(S)

            Vh = Vh[:rank, :]

            dist = torch.cat([U.flatten(), Vh.flatten()])
            hi_val = torch.quantile(dist, clamp_quantile)
            low_val = -hi_val

            U = U.clamp(low_val, hi_val)
            Vh = Vh.clamp(low_val, hi_val)

            if conv2d:
                U = U.reshape(out_dim, rank, 1, 1)
                Vh = Vh.reshape(rank, in_dim, kernel_size[0], kernel_size[1])

            U = U.to("cpu").contiguous()
            Vh = Vh.to("cpu").contiguous()

            lora_weights[lora_name] = (U, Vh)
    print("\rdone culc svd")

    # make state dict for LoRA
    lora_sd = {}
    for lora_name, (up_weight, down_weight) in lora_weights.items():
        lora_sd[lora_name + ".lora_up.weight"] = up_weight
        lora_sd[lora_name + ".lora_down.weight"] = down_weight
        lora_sd[lora_name + ".alpha"] = torch.tensor(down_weight.size()[0])
    for lora_name, diff in diffs.items():
        if norm_flags[lora_name]:
            lora_sd[lora_name + ".w_norm"] = diff.to("cpu")
            lora_sd[lora_name + ".b_norm"] = norm_b_diff[lora_name].to("cpu")
        elif bias_flags[lora_name]:
            lora_sd[lora_name + ".lora_up.bias"] = bias_diff[lora_name].to("cpu")
    return lora_sd


'''
from https://github.com/Guitaricet/relora
'''
def check_relora_scheduler(args):
    if (not args.relora_enable) or (not args.relora_lr_scheduler):
        return False
    return True
def get_relora_scheduler(optimizer, args, num_processes: int, datalen: int,
    min_lr_ratio=0.1,
    adjust_step=0,
    last_epoch=-1,):
    if args.relora_step is None:
        restart_every = datalen
    else:
        restart_every = args.relora_step
    if args.relora_restart_warmup_steps is None:
        args.relora_restart_warmup_steps = args.lr_warmup_steps
    return get_cosine_schedule_with_multiple_warmups(
        optimizer,
        num_training_steps=args.max_train_steps * num_processes,
        first_warmup_steps=args.lr_warmup_steps,
        restart_warmup_steps=args.relora_restart_warmup_steps,
        restart_every=restart_every,
        min_lr_ratio=min_lr_ratio,
        last_epoch=last_epoch,
        adjust_step=adjust_step,
    )
def get_cosine_schedule_with_multiple_warmups(
    optimizer,
    *,
    num_training_steps,
    first_warmup_steps,
    restart_warmup_steps,
    restart_every,
    min_lr_ratio=0.1,
    adjust_step=0,
    last_epoch=-1,
):
    if restart_every is None:
        raise ValueError("restart_every must be specified for cosine_restarts scheduler")

    #if num_training_steps % restart_every != 0:
    #    raise ValueError(f"num_training_steps ({num_training_steps}) must be divisible by restart_every ({restart_every})")

    lr_lambda = partial(
        _get_cosine_schedule_with_multiple_warmups_lambda,
        num_training_steps=num_training_steps,
        first_warmup_steps=first_warmup_steps,
        restart_warmup_steps=restart_warmup_steps,
        restart_every=restart_every,
        min_lr_ratio=min_lr_ratio,
        adjust_step=adjust_step,
    )
    return LambdaLR(optimizer, lr_lambda, last_epoch)
def _get_cosine_schedule_with_multiple_warmups_lambda(
    current_step,
    *,
    num_training_steps,
    first_warmup_steps,
    restart_warmup_steps,
    restart_every,
    min_lr_ratio,
    adjust_step,
):
    """
    Args:
        adjust_step: useful when continuing training from a warmed up checkpoint,
            it allows to sync the resets by reducing the number of steps
            after the first warmup and before the first reset.
            Thus, your ReLoRA resets can be synced with the optimizer resets.
    """
    assert 0 < min_lr_ratio <= 1.0, "min_lr_ratio must be in (0,1]"
    assert restart_every > 0, "restart_every must be positive"
    assert adjust_step + first_warmup_steps <= num_training_steps, "warmup + adjust_step is more than full training steps"
    assert adjust_step + first_warmup_steps <= restart_every, "the first reset will happen before the warmup is done"

    if current_step < first_warmup_steps:
        return float(current_step) / float(max(1, first_warmup_steps))

    _current_step = current_step + adjust_step

    restart_step = _current_step % restart_every
    restart_number = _current_step // restart_every

    if restart_step < restart_warmup_steps and current_step >= restart_every:
        # get expected lr multipler at the end of the warmup
        end_of_warmup_progress = (
            float(restart_number * restart_every + restart_warmup_steps - first_warmup_steps) /
            float(max(1, num_training_steps - first_warmup_steps))
        )

        _cosine_decay = 0.5 * (1.0 + math.cos(math.pi * end_of_warmup_progress))
        warmup_lr_multiplier = min_lr_ratio + (1.0 - min_lr_ratio) * _cosine_decay
    
        return float(restart_step) / float(max(1, restart_warmup_steps)) * warmup_lr_multiplier

    progress = float(_current_step - first_warmup_steps) / float(max(1, num_training_steps - first_warmup_steps))
    cosine_decay = 0.5 * (1.0 + math.cos(math.pi * progress))

    return min_lr_ratio + (1.0 - min_lr_ratio) * cosine_decay
