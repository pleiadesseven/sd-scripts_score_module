# 自動分類を使用してみる（自動分類サンプルを使用）

## 独自学習を使用せずCLIPのみを使用して分類する場合

1. 自動分類サンプルの`test`フォルダに分類したい画像を入れる（タグ付けしたtxtファイルがある場合はtxtファイルごと入れてもいい）。
2. 以下のコマンドを実行する。

    ```bash
    python score_run_sel.py --prompt_path "自動分類サンプル/expression.txt" --auto_dir_path "自動分類サンプル/output" --img_dir "自動分類サンプル/test" --algo "clip"
    ```

## 独自の分類用モデルを学習する場合

### 1. 最初に分類用のフォルダを作成する

- 手動でもいいが数が多い場合は面倒なので以下のスクリプトを使ってまとめて作成する。

    ```bash
    python score_run_sel_make_dataset_dir.py --output_dir "自動分類サンプル/pre" --prompt_path "自動分類サンプル/expression.txt"
    ```

### 2. 01で作成した分類用のフォルダ内に画像を入れていく

- 画像のみでいい。

### 3. 以下のスクリプトを使って分類用フォルダからデータセットを作成する

    ```bash
    python score_run_sel_make_dataset.py --img_dir "自動分類サンプル/pre" --output_dir "自動分類サンプル/true"
    ```

### 4. 学習コードを実行する

    ```bash
    python train_rf_score.py --dataset "自動分類サンプル" --prompt_file "自動分類サンプル/expression.txt" --output "自動分類サンプル/rf_model_expression" --epoch 200
    ```

### 5. 自動分類してみる

- 自動分類サンプルの`test`フォルダに自動分類したい画像を入れる。

    ```bash
    python score_run_sel.py --prompt_path "自動分類サンプル/expression.txt" --auto_dir_path "自動分類サンプル/output" --pth_path "自動分類サンプル/rf_model_expression" --img_dir "自動分類サンプル/test"
    ```

## まとめ

- 分類のためのフォルダを作成し、画像を配置。
- データセットを作成し、学習する。
- 学習したモデルを用いて自動分類を実行する。

    ```markdown
    python score_run_sel_make_dataset_dir.py --output_dir "分類のためのフォルダを作成するフォルダ" --prompt_path "分類したいワードを記述したテキストファイルのパス"
    python score_run_sel_make_dataset.py --img_dir "分類のためのフォルダ" --output_dir "データセットフォルダ内のtrueフォルダ"
    python train_rf_score.py --dataset "データセットフォルダ" --prompt_file "分類したいワードを記述したテキストファイルのパス" --output "モデルファイルの出力先" --epoch 200
    python score_run_sel.py --prompt_path "分類したいワードを記述したテキストファイルのパス" --auto_dir_path "自動分類の出力先" --pth_path "モデルファイルのパス" --img_dir "データセットフォルダ"
    ```
