from tkinter import filedialog
import glob
import os
import shutil
import argparse

parser = argparse.ArgumentParser(description= "Merge two stable diffusion models with git re-basin")
parser.add_argument("--backup", action="store_true", help="タグを書き換える時に書き換え前のファイルを残す")
parser.add_argument("--image_dir_depth", type=int, default=0, help="画像フォルダの探査深度/0で直下のみ")
#parser.add_argument("--model_b", type=str, help="Path to model b")
args = parser.parse_args()

#元のファイルを残す場合はTrue
backup_flag = args.backup
#画像フォルダ探索深度
image_dir_depth = args.image_dir_depth
#特殊テンプレートファイル名
char_temp_name = "_chartemplate"

#############################################
def dirdialog_clicked():
    iDir = os.path.abspath(os.path.dirname(__file__))
    iDirPath = filedialog.askdirectory(initialdir = iDir)
    return iDirPath

config_dir = "./tag_config"
config_paths = glob.glob(f"{config_dir}/*.txt")
configs = {}
char_configs = {}

wild_dir = "./wild_config"
wild_paths = glob.glob(f"{wild_dir}/*.txt")
wildcard_configs = {}
for filepath in wild_paths:
    key = os.path.splitext(os.path.basename(filepath))[0]
    txt = open(filepath, "r", encoding="utf8")
    datas = []
    for v in txt:
        datas.append(v.replace("\n", ""))
    wildcard_configs[key] = datas

for filepath in config_paths:
    key = os.path.splitext(os.path.basename(filepath))[0]
    if key == char_temp_name:
        question = input("chartemplateを特定のキャラ名に置き換えて使用しますか？[y/n] :")
        if question=="y":
            new_key = input("chartemplateを適用したいキャラ名を入力してください: ")
            if not new_key == "":
                key = f"_{new_key}"
    txt = open(filepath, "r", encoding="utf8")
    datas = []
    for v in txt:
        vlist = [v.replace("\n", "")]
        while True:
            _v = vlist.pop(0)
            if "__" in _v:
                for w_key, w_list in wildcard_configs.items():
                    if f"__{w_key}__" in _v:
                        for w_v in w_list:
                            vlist.append(_v.replace(f"__{w_key}__", w_v))
                        break
            else:
                datas.append(_v)
            
            if len(vlist)==0:
                break
        #datas.append(v)
    if os.path.basename(filepath)[0]=="_":
        char_configs[key[1:]] = datas
    else:
        configs[key] = datas
    txt.close()

config_keys = configs.keys()
chars_keys = char_configs.keys()
target_dir = dirdialog_clicked()
filelist = glob.glob(f"{target_dir}/*.txt")
if image_dir_depth>0:
    dir_depth = "/*"
    for i in range(image_dir_depth):
        dir_path = f"{target_dir}{dir_depth}/*.txt"
        filelist += glob.glob(dir_path)
        dir_depth += "/*"

for filepath in filelist:
    dellist = []
    logs = f"処理中のファイル: {filepath}" 
    print(logs)
    with open(filepath, "r", encoding="utf8") as txt:
        txtdata = txt.read()
        txtdata = txtdata.replace(", ", ",")
        txt.close()
    tags = txtdata.split(",")
    for tag in tags:
        if tag in config_keys:
            for v in configs[tag]:
                if not v in tags:
                    continue
                dellist.append(tag)
                logs += f"\n詳細タグ:{v} / 不要タグ:{tag}"
                break
        if tag in chars_keys:
            for v in char_configs[tag]:
                if not v in tags:
                    continue
                dellist.append(v)
                logs += f"\nキャラ名タグ:{tag} / 不要タグ:{v}"
    newtxt = ""
    for tag in tags:
        if tag in dellist:
            continue
        if newtxt == "":
            newtxt = f"{tag}"
        else:
            newtxt = f"{newtxt}, {tag}"
    if len(dellist) == 0:
        continue
    print(logs)
    print(f"dellist: {dellist}")
    if backup_flag:
        i = 0
        while True:
            backup_path = f"{filepath}.bak{i}"
            if os.path.isfile(backup_path):
                i += 1
                continue
            break
        shutil.copyfile(filepath, backup_path)

    with open(filepath, "w", encoding="utf8") as txt:
        txt.write(newtxt)
        txt.close()