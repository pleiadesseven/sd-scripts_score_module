画像サイズが256x256を最大にすればとりあえずVRAM 4Gでも学習可能

pip install lpips==0.1.4
が必要

・sd-scriptsでLoRA学習ができていれば苦労なく使えるはず

・導入
cd sd-scriptsのディレクトリパス
venv\scripts\activate
pip install lpips==0.1.4

このフォルダの中のファイルを train_network.pyと同じディレクトリに入れる

・使い方
cd sd-scriptsのディレクトリパス
venv\scripts\activate
python train_vae.py

を上だけでとりあえず学習は可能です
データセットに使う画像フォルダや学習ベースにするvaeファイルの場所はダイアログで選択できます

どんな設定があるのかについては

python train_vae.py -h

で確認して


設定がyamlファイルを使っての読み書きもできるので

python train_vae.py --config yamlファイルのパス

で yamlファイルの設定を読み込むようになります
設定ファイルの保存は学習終了時に自動でその時に使った設定を出力するようになってる


上の内容について解説

cd ディレクトリパス　でディレクトリパスで指定したディレクトリに移動してそこを作業の基本ディレクトリにする

venv\scripts\activate　で仮想環境を起動する venvの部分は仮想環境を作った時の名前によるから違う時もあるたいていそのディレクトリにvenvとか名前つけたディレクトリある

ついでに
python -m venv venvの名前　で venvの名前 っていう仮想環境が作れる
venvの名前\scripts\activate　で上で得作った venvの名前 という仮想環境が起動する

pip install 名前　は　名前　のライブラリをインストールするよって命令
仮想環境を起動してからやらないと仮想環境にインストールされないので注意　なのでとにかく仮想環境起動は癖付けていいレベルで大事

この辺のことは覚えておくととりあえず導入関連で何をやらされてるのかわかる
