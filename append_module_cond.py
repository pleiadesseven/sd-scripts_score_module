import os
import torch
import argparse
import importlib
##############################################################################################################
# EMA 実装
# ckptでよく見たモデル名についてるema_onlyとかのやつ
# 雑に解説すると直近の更新の影響が大きくなりすぎるのでそれを抑制する仕組み
# 
def add_append_score_arguments(parser: argparse.ArgumentParser):
    # parser.add_argument("--cond_use", action="store_ture", help="条件付けを使用するかどうか")
    parser.add_argument("--cond_module", type=str, nargs="*", default=None, help="条件付けに使うモジュール")
##############################################################################################################
# ここから下は特に説明ない
### 動作実験のための簡単なDiscriminator
### cahiner時代にシンプルさを追及して作ったDiscriminator 精度は知らん
### conv2を積層にしたら広い範囲を見れる構造 ただその場合padding計算してskip connect必要
class Cond_Module(torch.nn.Module):
    def __init__(self, cond_names={"test": 1280}, hid_dims=320) -> None:
        super(Cond_Module, self).__init__()
        # 条件判定のため
        self.use = True
        self.gan_enable= True
        self.pre_train = False
        self.use_forward = False
        self.use_dataloader = False
        # モデルサイズ決定のための情報取り出し
        cond_keys = list(cond_names.keys())
        print(cond_names, cond_keys)
        # モデル定義
        self.conv1 = torch.nn.Conv2d(cond_names[cond_keys[0]], hid_dims, 3, bias=False)
        self.activation1 = lambda x: x * torch.sigmoid(x)
        self.conv2 = torch.nn.Conv2d(hid_dims, hid_dims*2, 3, 1, 0, 2, bias=False)
        self.activation2 = lambda x: x * torch.sigmoid(x)
        self.conv3 = torch.nn.Conv2d(hid_dims*2, hid_dims, 3, bias=False)
    def forward(self, inputs):
        h = self.conv1(inputs)
        h = self.activation1(h)
        h = self.conv2(h)
        h = self.activation2(h)
        h = self.conv3(h)
        return h
    def loss_gan_func(self, latents, inputs=None, teacher=None):
        b_size = latents.size(0)//2
        fake = self(latents[:b_size])
        real = self(latents[b_size:])
        loss_fake = torch.nn.functional.softplus(fake).mean()
        loss_real = torch.nn.functional.softplus(-real).mean()
        return loss_fake + loss_real
    def loss_func(self, latents, inputs=None, teacher=None):
        b_size = latents.size(0)//2
        fake = self(latents[b_size:])
        loss_real = torch.nn.functional.softplus(-fake).mean()
        return loss_real

# これから作る
class Forward_Module(torch.nn.Module):
    def __init__(self, input_dims=[1280], hid_dims=320) -> None:
        super(Cond_Module, self).__init__()
        # 条件判定のため
        self.use = True
        self.gan_enable= True
        self.pre_train = False
        self.use_forward = False
        # モデル定義
        self.conv1 = torch.nn.Conv2d(input_dims[0], hid_dims, 3, bias=False)
        self.activation1 = lambda x: x * torch.sigmoid(x)
        self.conv2 = torch.nn.Conv2d(hid_dims, hid_dims*2, 3, 1, 0, 2, bias=False)
        self.activation2 = lambda x: x * torch.sigmoid(x)
        self.conv3 = torch.nn.Conv2d(hid_dims*2, hid_dims, 3, bias=False)
    def forward(self, inputs):
        h = self.conv1(inputs)
        h = self.activation1(h)
        h = self.conv2(h)
        h = self.activation2(h)
        h = self.conv3(h)
        return h

##############################################################################################################
# 条件付け学習のための管理クラス
# 暫定的な実装
class Cond_Train(torch.nn.Module):
    def __init__(self, module_names, cond_names, *args) -> None:
        super().__init__()
        print("条件付け管理モジュール作成中....")
        self.cond_modules = []
        self.optimizer = None
        self.use = False
        self.gan_enable = False
        self.pre_train = False
        self.use_forward = False
        self.optimizer_class = torch.optim.AdamW

        for module_name in module_names:
            if module_name == "append_module_cond":
                self.cond_modules.append(Cond_Module(cond_names, 320))
            else:
                try: 
                    _module = importlib.import_module(module_name)
                    self.cond_modules.append(_module.Cond_Module(cond_names, 320))
                except:
                    print(f"{module_name}の作成に失敗しました")
                    continue
            self.use = (self.use or self.cond_modules[-1].use)
            self.gan_enable = (self.gan_enable or self.cond_modules[-1].gan_enable)
            self.pre_train = (self.pre_train or self.cond_modules[-1].pre_train)
            self.use_forward = (self.use_forward or self.cond_modules[-1].use_forward)
        
        self.forward_model = None
        print(f"use: {self.use}, gan: {self.gan_enable}, pretrain: {self.pre_train}, use_forward: {self.use_forward}")
        
    def get_parameters(self):
        all_params = []
        if self.use_forward:
            self.forward_model = Forward_Module()
            all_params.append(self.forward_model.parameters())
        for cond_module in self.cond_modules:
            all_params.append({"params": cond_module.parameters()})
        #self.optimizer = optimizer_class(all_params)
        return all_params
        
    def gan_forward(self, inputs, dataname):
        loss = None
        z = None
        teacher = None
        if self.use_forward:
            z = self.forward_model(inputs)
        for module in self.cond_modules:
            if module.gan_enable:
                if loss is None:
                    loss = module.loss_gan_func(inputs, z, teacher)
                else:
                    loss += module.loss_gan_func(inputs, z, teacher)
        return loss

    def forward(self, inputs, dataname):
        loss = None
        z = None
        teacher = None
        if self.use_forward:
            z = self.forward_model(inputs)
        for module in self.cond_modules:
            if loss is None:
                loss = module.loss_func(inputs, z, teacher)
            else:
                loss += module.loss_func(inputs, z, teacher)
        return loss