import argparse
import torch
from tkinter import filedialog
import safetensors.torch
import os
#import glob
import tqdm

def flatten_params(model):
  return model["state_dict"]
def dirdialog_clicked():
    iDir = os.path.abspath(os.path.dirname(__file__))
    iDirPath = filedialog.askdirectory(initialdir = iDir)
    return iDirPath
def filedialog_clicked():
    fTyp = [("", "ckpt"),("", "safetensors"),("", "pt")]
    iFile = os.path.abspath(os.path.dirname(__file__))
    iFilePath = filedialog.askopenfilename(filetype = fTyp, initialdir = iFile)
    return iFilePath

def load_model(model_path, device):
    if os.path.splitext(model_path)[-1]==".ckpt" or os.path.splitext(model_path)[-1]==".pt":
        model = torch.load(model_path, map_location=device)
        model = model["state_dict"]
    else:
        model = safetensors.torch.load_file(model_path, device=device)
    return model
parser = argparse.ArgumentParser(description= "Merge two stable diffusion models with git re-basin")
parser.add_argument("--model_a", type=str, default=None, help="Path to model a")
parser.add_argument("--save_type", type=str, default="safetensors")
args = parser.parse_args()   

if args.model_a is None:
    print("読み込むファイルを選択してください")
    args.model_a = filedialog_clicked()
    output_file = os.path.splitext(args.model_a)[0]

device = "cpu"
theta_a = load_model(args.model_a, device)

logs = ""
repair_flag = False

keys = list(theta_a.keys())
for k in tqdm.tqdm(keys):
    if ".mid.attn" in k:
        if "to_q.weight" in k:
            new_k = k.replace("to_q.weight", "q.weight")
            theta_a[new_k] = theta_a.pop(k)
            theta_a[new_k] = theta_a[new_k].unsqueeze(dim=2).unsqueeze(dim=2)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_q.bias" in k:
            new_k = k.replace("to_q.bias", "q.bias")
            theta_a[new_k] = theta_a.pop(k)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_k.weight" in k:
            new_k = k.replace("to_k.weight", "k.weight")
            theta_a[new_k] = theta_a.pop(k)
            theta_a[new_k] = theta_a[new_k].unsqueeze(dim=2).unsqueeze(dim=2)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_k.bias" in k:
            new_k = k.replace("to_k.bias", "k.bias")
            theta_a[new_k] = theta_a.pop(k)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_v.weight" in k:
            new_k = k.replace("to_v.weight", "v.weight")
            theta_a[new_k] = theta_a.pop(k)
            theta_a[new_k] = theta_a[new_k].unsqueeze(dim=2).unsqueeze(dim=2)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_v.bias" in k:
            new_k = k.replace("to_v.bias", "v.bias")
            theta_a[new_k] = theta_a.pop(k)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_out.0.weight" in k:
            new_k = k.replace("to_out.0.weight", "proj_out.weight")
            theta_a[new_k] = theta_a.pop(k)
            theta_a[new_k] = theta_a[new_k].unsqueeze(dim=2).unsqueeze(dim=2)
            logs += f"{k} to {new_k}\n"
            repair_flag = True
        elif "to_out.0.bias" in k:
            new_k = k.replace("to_out.0.bias", "proj_out.bias")
            theta_a[new_k] = theta_a.pop(k)
            logs += f"{k} to {new_k}\n"
            repair_flag = True

print(logs)
if repair_flag:
    print(f"{output_file}_repair.{args.save_type}に保存中")
    if args.save_type=="pt":
        output_file = f"{output_file}_repair.pt"
        torch.save({
                "state_dict": theta_a
                    }, output_file)
    else:
        output_file = f"{output_file}_repair.safetensors"
        safetensors.torch.save_file(theta_a, output_file)
print("完了")