import os
from tkinter import filedialog

def get_files(dirs: list, dots: list, include_symlinks=True, print_log=False):
    # カレントディレクトリからの階層を順番に探索する
    files = []
    i = 0
    for dir in dirs:
        try:
            for root, dirs, f in os.walk(dir, followlinks=include_symlinks):
                # ファイルを格納
                for file in f:
                    for dot in dots:
                        if dot in os.path.splitext(file)[-1]:
                            files.append(os.path.join(root, file))
                            i+=1
        except:
            print(f"{dir} is can't get files")
            continue
    print(f"ファイル数：　{len(files)}")
    return files

def dirdialog_clicked(initial_dir=None):
    if initial_dir==None:
        iDir = os.path.abspath(os.path.dirname(__file__))
    else:
        iDir = initial_dir
    iDirPath = filedialog.askdirectory(initialdir = iDir)
    return iDirPath
def filedialog_clicked(dots, initial_dir=None, mode="open"):
    fTyp = []
    for dot in dots:
        fTyp.append(("", dot))
    if initial_dir==None:
        iFile = os.path.abspath(os.path.dirname(__file__))
    else:
        iFile = initial_dir
    if mode=="open":
        iFilePath = filedialog.askopenfilename(filetype = fTyp, initialdir = iFile)
    elif mode=="save":
        iFilePath = filedialog.asksaveasfilename(filetype = fTyp, initialdir = iFile)
    return iFilePath
