# os.environ["CUDA_VISIBLE_DEVICES"] = "0"    # choose GPU if you are on a multi GPU server
import numpy as np
import torch
# import tqdm

from os.path import join
# from datasets import load_dataset
import glob
import os
import shutil
import tqdm
import argparse
import clip
from PIL import Image, ImageFile
from score_module.rewardfunction_score import get_image_preprocess
from common_modules import dirdialog_clicked, filedialog_clicked

parser = argparse.ArgumentParser(description="run")
#parser.add_argument("--auto_dir_step", type=str, nargs="*", default=["worst quality", "low quality", "normal quality", "high quality", "best quality", "masterpiece"])
parser.add_argument("--prompt_path", type=str, default=None)
parser.add_argument("--auto_dir_path", type=str, default="output")
parser.add_argument("--pth_path", type=str, default=None)
parser.add_argument("--img_dir", type=str, default=None)
parser.add_argument("--output_info", action="store_true")
parser.add_argument("--version", type=int, default=0)
parser.add_argument("--algo", type=str, default="rf", choices=["rf", "clip"])

args = parser.parse_args()
print(f"モデル：{args.algo}({args.version})")
if args.prompt_path is None:
    print("選別区分を記述したテキストファイルを指定してください")
    args.prompt_path = filedialog_clicked([".txt"])
if args.img_dir is None:
    print("選別対象となるディレクトリを指定してください")
    args.img_dir = dirdialog_clicked()
if args.algo == "rf":
    if args.pth_path is None:
        print("読み込むモデルファイルを選択してください")
        args.pth_path = filedialog_clicked([".pth"])
        args.pth_path = os.path.splitext(args.pth_path)[0]

# This script will predict the aesthetic score for this image file:

img_dir = args.img_dir  # 自動分類対象のフォルダ
img_path_list = glob.glob(os.path.join(img_dir, "*.png")) + glob.glob(os.path.join(img_dir, "*.jpg")) + glob.glob(os.path.join(img_dir, "*.jpeg")) + glob.glob(os.path.join(img_dir, "*.webp"))
pth_path = args.pth_path  # 読み込むpthファイル名
auto_dir_base = args.auto_dir_path  # 自動分類する時の出力先
auto_dir_steps = []
with open(args.prompt_path, "rt", encoding="utf-8") as f:
    for line in f.readlines():
        if line == "" or line[0] == "#" or line=="\n":
            continue
        auto_dir_steps.append(line.replace("\n", ""))
auto_dir_list = []
if len(img_path_list) == 0:
    print("画像ファイルが見つかりませんでした｡処理を中断します")
    exit()
print(f"target filecount: {len(img_path_list)}")

if not os.path.exists(auto_dir_base):
    # os.mkdir(auto_dir_base)
    os.makedirs(auto_dir_base, exist_ok=True)
for step_name in auto_dir_steps:
    auto_dir_list.append(f"{auto_dir_base}/{step_name}")
print(auto_dir_list)

# if you changed the MLP architecture during training, change it also here:
token_chunk_size= 77
if args.version == 0:
    image_size = 1
    token_max_size = 77
elif args.version == 1:
    image_size = 1
    token_max_size = 231
elif  args.version == 2:
    image_size = 5
    token_max_size = 77
elif  args.version == 3:
    image_size = 5
    token_max_size = 231
token_split_num = token_max_size // token_chunk_size

if args.algo == "rf":
    from score_module.rewardfunction_score import MLP
    model = MLP(768 * (image_size+token_split_num))

def normalized(a, axis=-1, order=2):
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2 == 0] = 1
    return a / np.expand_dims(l2, axis)

if args.algo == "rf":
    for _ in range(2):
        try:
            s = torch.load(f"{pth_path}.pth")   # load the model you trained previously or the model available in this repo
            model.load_state_dict(s)
            break
        except RuntimeError:
            if args.version == 1:
                if os.path.isfile(f"{pth_path}_225.pth"):
                    pth_path = f"{pth_path}_225"
                continue
            elif args.version == 2:
                if os.path.isfile(f"{pth_path}_x4.pth"):
                    pth_path = f"{pth_path}_x4"
            elif args.version == 3:
                if os.path.isfile(f"{pth_path}_x4_225.pth"):
                    pth_path = f"{pth_path}_x4_225"
            else:
                print(f"pthファイルを読み込めませんでしたファイルのパスを確認してください({pth_path})")

device = "cuda" if torch.cuda.is_available() else "cpu"
if args.algo == "rf":
    model.to(device)
    model.eval()

clip_model, clip_preprocess = clip.load("ViT-L/14", device=device)  # RN50x64
tokenizer = clip.tokenize
preprocess_size = clip_model.visual.input_resolution
if args.version >= 2:
    image_preprocess = get_image_preprocess(preprocess_size, 2)

prompt_emb_lists = []
score_log = {}
if args.algo == "clip":
    for step_name in auto_dir_steps:
        score_log[step_name] = 0
    prompt_ids = tokenizer(auto_dir_steps, token_max_size, True)
    with torch.no_grad():
        text_embs = clip_model.encode_text(prompt_ids.to(device))
else:
    for step_name in auto_dir_steps:
        score_log[step_name] = 0
        prompt_ids = tokenizer(step_name, token_max_size, True)
        text_embs = []
        with torch.no_grad():
            for j in range(token_split_num):
                text_emb = clip_model.encode_text(prompt_ids[:, 77*j:77*(j+1)].to(device))
                text_embs.append(text_emb)
            if token_split_num>1:
                text_emb = torch.concat(text_embs, dim=1)
        prompt_emb_lists.append(text_emb)

# model2, preprocess = clip.load("ViT-L/14", device=device)  # RN50x64
output_str_tr = ""
output_str_img = ""
output_str_score = ""
total_score = 0.0
score_count = 0
i = 0
for img_path in tqdm.tqdm(img_path_list):
    pil_image = Image.open(img_path)
    image = clip_preprocess(pil_image).unsqueeze(0).to(device)

    with torch.no_grad():
        image_features = clip_model.encode_image(image)

    if args.version >= 2:
        _pre_image = image_preprocess(pil_image).unsqueeze(0).to(device)
        for x in range(2):
            for y in range(2):
                with torch.no_grad():
                    image_features = torch.concat([image_features, clip_model.encode_image(_pre_image[:,:,x*preprocess_size:(x+1)*preprocess_size, y*preprocess_size:(y+1)*preprocess_size])], dim=1)


    prediction_list = []
    max_score_id = -1
    max_score = -9999.
    if args.algo == "clip":
        predict = (100.0 * image_features @ text_embs.T).softmax(dim=-1)
        max_score_id = torch.argmax(predict)
        for prec in predict[0]:
            prediction_list.append(prec)
    else:
        for text_emb in prompt_emb_lists:
            im_emb_arr = torch.concat([image_features.float(), text_emb], dim=1)
            prediction_list.append(model(im_emb_arr).data.cpu()[0][0])
            if prediction_list[-1] >= max_score:
                max_score_id = len(prediction_list)-1
                max_score = prediction_list[-1]
    score_log[auto_dir_steps[max_score_id]] += 1

    if not os.path.exists(auto_dir_list[max_score_id]):
        os.mkdir(auto_dir_list[max_score_id])
    copy_path = f"{auto_dir_list[max_score_id]}/{os.path.basename(img_path)}"
    excount = 0
    while True:
        if os.path.exists(copy_path):
            copy_path = f"{auto_dir_list[max_score_id]}/{excount}_{os.path.basename(img_path)}"
        else:
            break
        excount += 1
    shutil.move(img_path, copy_path)
    txt_path = os.path.splitext(img_path)[0] + ".txt"
    copy_path = os.path.splitext(copy_path)[0] + ".txt"
    if os.path.exists(txt_path):
        shutil.move(txt_path, copy_path)

    if args.output_info:
        info_log = f"{img_path}: {auto_dir_steps[max_score_id]}\n"
        for i, step_name in enumerate(auto_dir_steps):
            info_log += f"{step_name}: {prediction_list[i]}\n"
            log_path = os.path.splitext(copy_path)[0] + "_info.txt"
            with open(log_path, 'w') as f:
                f.write(info_log)

for k, v in score_log.items():
    if v == 0: continue
    print(f"{k}: {v}")

print("done!!")
