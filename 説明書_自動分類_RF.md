# 環境構築

**前提条件:** sd-scriptsの環境構築まで終了していること。

1. このフォルダ内のファイルを全て `sd-scripts`フォルダにコピーする。
2. コマンドプロンプトを起動し、以下のコマンドを実行する。

   ```bash
   cd sd-scriptsのディレクトリ
   venv\scripts\activate
   pip install -e git+https://github.com/openai/CLIP.git@main#egg=clip
   ```

   **コマンドプロンプトの起動方法がわからない場合:**
   `sd-scripts`フォルダを開き、アドレスバーに `cmd`と入力。

# 高速化のための追加設定

**必要な環境:**

- torch2.0.1（動作確認環境）
- diffusers[torch] == 0.13.1（動作確認環境）

sdp attentionを適用したい場合、上記の環境を構築する必要があります。
デフォルトのスクリプトはtorch1.12やdiffuserのバージョンが0.10.2環境を前提としています。
そのため、sdp用のvenvを作成し、使用するスクリプトに合わせてvenvを使い分ける方法を推奨します。

# 学習を始めるまでの手順

1. **Step1:** `config_sample`フォルダを開き、`config_sample.toml`と `dataset/dataset_sample.toml`を編集して学習設定に合わせる。tomlファイルはテキストエディタで編集可能。（推奨エディタ: VS Code）
2. **Step1.5:** スコア評価を用いた学習をする場合、"評価用画像生成promptのサンプルファイル"フォルダを参考にして画像生成promptの設定ファイルを作成する。面倒な場合は、フォルダ名を日本語を含まない名前に変更しておく。
3. **Step2:** `1_start_finetuning.bat`ファイルをテキストエディタで開き、`--config_file "config_sample\config_sample 学習設定ファイルのパス"`の部分をStep1で作成したtomlファイルのパスに変更する。
4. **Step3:** `1_start_finetuning.bat`ファイルをダブルクリックして学習を開始する。

# 自分用のスコアリングAIを学習させる

1. **Step1:** "rf_score_datasetのサンプル"フォルダを参考にして、正しい内容になっているpromptが設定されている画像を `true`フォルダに配置する。（promptは画像と同名のtxtファイルに記述）
2. **Step2:** `0_make_score_model.bat`ファイルをテキストエディタで開き、必要な部分を変更する。
3. **Step3:** `0_make_score_model.bat`ファイルをダブルクリックして学習を開始する。

# 自分用のスコアリングAIを学習させる（画像のみを使って美的評価をするモデル）

## 学習準備

1. `cas_dataset`フォルダを開いて、自分の主観に基づいたスコアに合わせたフォルダに画像を入れていく。（デフォルトでは1.0刻みでフォルダが用意されているが、小数点以下の値を細かく刻んでもOK）

## 学習

1. コマンドプロンプトを起動し、以下のコマンドを実行して学習を開始する。

```bash
venv\scripts\activate
python train_cas.py --dataset cas_dataset --output score_module\cas_model_name --epoch 100 --version 0 --resume "score_module\cas_dataset"
```

オプションの説明:

- `--dataset`: データセットフォルダ
- `--output`: 出力するモデルファイル名
- `--epoch`: 学習を回すエポック数（基本的に100も回せば十分）
- `--version`: 0 ～ 2（0だとCLIPデフォの画像サイズで判別、1だと縦横それぞれx2、2だと縦横それぞれx3）
- `--resume`: データセット用にまとめられたファイルを読み込む場合に使用（複数指定可）

## 自動分類を使ってみる

1. コマンドプロンプトで以下のコマンドを実行する。

```bash
venv\scripts\activate
python score_run_cas.py --version 1
```

オプションの説明:

- `--auto_dir_path`: 分類したファイルの出力先
- `--img_dir`: 分類したい画像ファイルを入れてあるフォルダ（デフォだとtestset）
- `--pth_path`: モデルファイルのパス
- `--version`: 0 ～ 2（0だとCLIPデフォの画像サイズで判別、1だと縦横それぞれx2、2だと縦横それぞれx3）

# 自分用のスコアリングAIを学習させる（オリジナルサイズの画像のみを使って美的評価をするモデル）

基本的な使い方は上記のrfモデルと同じだぜ。

**メリット:**

- オリジナルサイズで処理するため、画像データの劣化が少ない。
- マルチアス比に対応したTransformerで、CLIP依存がない。

**注意点:**

- マルチアス比を前提としたTransformerの学習のため、データセットの規模がある程度必要。
- VRAM制約と演算リソースの問題があるため、上限サイズの設定推奨。

付属モデルは832x832解像度を上限サイズとして学習してある。自動マージにはまだ未対応。
