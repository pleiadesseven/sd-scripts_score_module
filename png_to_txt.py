import glob
import os
from PIL import Image
import tqdm

from common_modules import dirdialog_clicked

def main():
    print("画像ディレクトリを指定してください：")
    img_dir = dirdialog_clicked()
    print(f"target: {img_dir}")

    img_list = glob.glob(img_dir+"/**/*.png", recursive=True)

    for img_path in tqdm.tqdm(img_list):
        txt_path = os.path.splitext(img_path)[0] + ".txt"
        if os.path.isfile(txt_path): continue
        image = Image.open(img_path)
        meta = image.info
        
        prompt = meta["parameters"].split("Negative prompt:")[0].strip()
        
        with open(txt_path, mode="w", encoding="utf-8") as f:
            f.write(prompt)

if __name__ == "__main__":
    main()