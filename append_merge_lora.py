#import sd_webui_bayesian_merger.optimiser
#import sd_webui_bayesian_merger.scorer
#import sd_webui_bayesian_merger.merger
#from common_modules import filedialog_clicked
import torch
import re
#import os
import numpy as np
import tqdm

def convert_diffusers_name_to_compvis(du_name):
    """
    convert diffusers's LoRA name to CompVis
    """
    import re
    cv_name = None
    if "lora_unet_" in du_name:
        m = re.search(r"_down_blocks_(\d+)_attentions_(\d+)_(.+)", du_name)
        if m:
            du_block_index = int(m.group(1))
            du_attn_index = int(m.group(2))
            du_suffix = m.group(3)

            cv_index = 1 + du_block_index * 3 + du_attn_index      # 1,2, 4,5, 7,8
            cv_name = f"model.diffusion_model.input_blocks.{cv_index}.1.{du_suffix}"
            if "transformer_blocks_" in cv_name: cv_name = cv_name.replace("transformer_blocks_", "transformer_blocks.")
            if "_attn1_" in cv_name: cv_name = cv_name.replace("_attn1_", ".attn1.")
            if "_attn2_" in cv_name: cv_name = cv_name.replace("_attn2_", ".attn2.")
            if ".to_out_" in cv_name: cv_name = cv_name.replace(".to_out_", ".to_out.")
            if "_ff_net_" in cv_name: cv_name = cv_name.replace("_ff_net_", ".ff.net.")
            if "_proj" in cv_name: cv_name = cv_name.replace("_proj", ".proj")
            if "_norm" in cv_name: cv_name = cv_name.replace("_norm", ".norm")
            return cv_name
        
        m = re.search(r"_mid_block_attentions_(\d+)_(.+)", du_name)
        if m:
            du_suffix = m.group(2)
            cv_name = f"model.diffusion_model.middle_block.1.{du_suffix}"
            if "transformer_blocks_" in cv_name: cv_name = cv_name.replace("transformer_blocks_", "transformer_blocks.")
            if "_attn1_" in cv_name: cv_name = cv_name.replace("_attn1_", ".attn1.")
            if "_attn2_" in cv_name: cv_name = cv_name.replace("_attn2_", ".attn2.")
            if ".to_out_" in cv_name: cv_name = cv_name.replace(".to_out_", ".to_out.")
            if "_ff_net_" in cv_name: cv_name = cv_name.replace("_ff_net_", ".ff.net.")
            if "_proj" in cv_name: cv_name = cv_name.replace("_proj", ".proj")
            if "_norm" in cv_name: cv_name = cv_name.replace("_norm", ".norm")
            return cv_name
        
        m = re.search(r"_up_blocks_(\d+)_attentions_(\d+)_(.+)", du_name)
        if m:
            du_block_index = int(m.group(1))
            du_attn_index = int(m.group(2))
            du_suffix = m.group(3)

            cv_index = du_block_index * 3 + du_attn_index      # 3,4,5, 6,7,8, 9,10,11
            cv_name = f"model.diffusion_model.output_blocks.{cv_index}.1.{du_suffix}"
            if "transformer_blocks_" in cv_name: cv_name = cv_name.replace("transformer_blocks_", "transformer_blocks.")
            if "_attn1_" in cv_name: cv_name = cv_name.replace("_attn1_", ".attn1.")
            if "_attn2_" in cv_name: cv_name = cv_name.replace("_attn2_", ".attn2.")
            if ".to_out_" in cv_name: cv_name = cv_name.replace(".to_out_", ".to_out.")
            if "_ff_net_" in cv_name: cv_name = cv_name.replace("_ff_net_", ".ff.net.")
            if "_proj" in cv_name: cv_name = cv_name.replace("_proj", ".proj")
            if "_norm" in cv_name: cv_name = cv_name.replace("_norm", ".norm")
            return cv_name
        
        m = re.search(r"_down_blocks_(\d+)_resnets_(\d+)_(.+)", du_name)
        if m:
            du_block_index = int(m.group(1))
            du_res_index = int(m.group(2))
            du_suffix = m.group(3)
            cv_suffix = {
                'conv1': 'in_layers.2',
                'conv2': 'out_layers.3',
                'time_emb_proj': 'emb_layers.1',
                'conv_shortcut': 'skip_connection',
                'norm1': "in_layers.0",
                'norm2': "out_layers.0"
            }[du_suffix]

            cv_index = 1 + du_block_index * 3 + du_res_index      # 1,2, 4,5, 7,8
            cv_name = f"model.diffusion_model.input_blocks.{cv_index}.0.{cv_suffix}"
            return cv_name
        
        m = re.search(r"_down_blocks_(\d+)_downsamplers_0_conv", du_name)
        if m:
            block_index = int(m.group(1))
            cv_index = 3 + block_index * 3
            cv_name = f"model.diffusion_model.input_blocks.{cv_index}.0.op"
            return cv_name
        
        m = re.search(r"_mid_block_resnets_(\d+)_(.+)", du_name)
        if m:
            index = int(m.group(1))
            du_suffix = m.group(2)
            cv_suffix = {
                'conv1': 'in_layers.2',
                'conv2': 'out_layers.3',
                'time_emb_proj': 'emb_layers.1',
                'conv_shortcut': 'skip_connection',
                'norm1': "in_layers.0",
                'norm2': "out_layers.0"
            }[du_suffix]
            cv_name = f"model.diffusion_model.middle_block.{index*2}.{cv_suffix}"
            return cv_name
        
        m = re.search(r"_up_blocks_(\d+)_resnets_(\d+)_(.+)", du_name)
        if m:
            du_block_index = int(m.group(1))
            du_res_index = int(m.group(2))
            du_suffix = m.group(3)
            cv_suffix = {
                'conv1': 'in_layers.2',
                'conv2': 'out_layers.3',
                'time_emb_proj': 'emb_layers.1',
                'conv_shortcut': 'skip_connection',
                'norm1': "in_layers.0",
                'norm2': "out_layers.0"
            }[du_suffix]

            cv_index = du_block_index * 3 + du_res_index      # 1,2, 4,5, 7,8
            cv_name = f"model.diffusion_model.output_blocks.{cv_index}.0.{cv_suffix}"
            return cv_name
        
        m = re.search(r"_up_blocks_(\d+)_upsamplers_0_conv", du_name)
        if m:
            block_index = int(m.group(1))
            cv_index = block_index * 3 + 2
            cv_name = f"model.diffusion_model.output_blocks.{cv_index}.{bool(block_index)+1}.conv"
            return cv_name
        
        if "lora_unet_conv_in" in du_name:
            cv_name = "model.diffusion_model.input_blocks.0.0"
            return cv_name
        if "lora_unet_conv_out" in du_name:
            cv_name = "model.diffusion_model.out.2"
            return cv_name
        if "lora_unet_time_embedding_linear_1" in du_name:
            cv_name = "model.diffusion_model.time_embed.0"
            return cv_name
        if "lora_unet_time_embedding_linear_2" in du_name:
            cv_name = "model.diffusion_model.time_embed.2"
            return cv_name
        if "lora_unet_conv_norm_out" in du_name:
            cv_name = "model.diffusion_model.out.0"
            return cv_name
        
    elif "lora_te_" in du_name:
        m = re.search(r"_model_encoder_layers_(\d+)_(.+)", du_name)
        if m:
            du_block_index = int(m.group(1))
            du_suffix = m.group(2)

            cv_index = du_block_index
            cv_name = f"cond_stage_model.transformer.text_model.encoder.layers.{cv_index}.{du_suffix}"
            if ".self_attn_" in cv_name: cv_name = cv_name.replace(".self_attn_", ".self_attn.")
            if ".mlp_" in cv_name: cv_name = cv_name.replace(".mlp_", ".mlp.")

    assert cv_name is not None, f"conversion failed: {du_name}. the model may not be trained by `sd-scripts`."
    return cv_name

def merge_conv(lora_down, lora_up, device="cpu"):
    in_rank, in_size, kernel_size, k_ = lora_down.shape
    out_size, out_rank, _, _ = lora_up.shape
    assert in_rank == out_rank and kernel_size == k_, f"rank {in_rank} {out_rank} or kernel {kernel_size} {k_} mismatch"
    
    lora_down = lora_down.to(device)
    lora_up = lora_up.to(device)

    merged = lora_up.reshape(out_size, -1) @ lora_down.reshape(in_rank, -1)
    weight = merged.reshape(out_size, in_size, kernel_size, kernel_size)
    del lora_up, lora_down
    return weight


def merge_linear(lora_down, lora_up, device="cpu"):
    in_rank, in_size = lora_down.shape
    out_size, out_rank = lora_up.shape
    assert in_rank == out_rank, f"rank {in_rank} {out_rank} mismatch"
    
    lora_down = lora_down.to(device)
    lora_up = lora_up.to(device)
    
    weight = lora_up @ lora_down
    del lora_up, lora_down
    return weight
def cp_weight(wa, wb, t):
    temp = torch.einsum('i j k l, j r -> i r k l', t, wb)
    return torch.einsum('i j k l, i r -> r j k l', temp, wa)

def convet_lora_to_sd(state_dict, algo):
    for key in list(state_dict.keys()):
        # dtype convert
        half_flag = None
        if type(state_dict[key]) == torch.Tensor:
            if half_flag is None:
                if state_dict[key].dtype == torch.float:
                    half_flag = False
                else:
                    half_flag = True
                my_device = state_dict[key].device
            state_dict[key] = state_dict[key].to(torch.float)
    
    print(f"algorithm is {algo} / half? {half_flag}")
    new_sd = {}

    # common var
    weights_loaded = False
    block_down_name = None
    block_up_name = None
    dim = None
    # lora to sd weight var
    lora_down_weight = None
    lora_up_weight = None
    # loha to sd weight var
    loha_w1a_weight = None
    loha_w1b_weight = None
    loha_w2a_weight = None
    loha_w2b_weight = None
    loha_hada_t1 = None
    loha_hada_t2 = None
    # lokr to sd weight var
    lokr_w1_weight = None
    lokr_w2_weight = None

    for key, value in state_dict.items():
        weight_name = None

        if ("b_norm" in key) or ("w_norm" in key):
            block_down_name = key.split(".")[0]
            weight_name = key.split(".")[-1]
            if "w_norm" in key:
                new_key = block_down_name + ".weight"
            else:
                new_key = block_down_name + ".bias"
            new_sd[new_key] = value
            continue
        
        if "lora_up.bias" in key:
            block_down_name = key.split(".")[0]
            new_key = block_down_name + ".bias"
            dim = value.size()[0]
            lora_alpha = state_dict.get(block_down_name + '.alpha', None)
            if lora_alpha is None:
                scale = 1.0
            else:
                scale = lora_alpha/dim
            new_sd[new_key] = scale * value 
            
            continue

        if algo == "lora":
            # lora weight get
            if not "lora_down" in key: continue
            block_down_name = key.split(".")[0]
            weight_name = key.split(".")[-1]
            lora_down_weight = value
            
            block_up_name = block_down_name
            lora_up_weight = state_dict.get(block_up_name + '.lora_up.' + weight_name, None)
            lora_mid_weight = state_dict.get(block_down_name + ".lora_mid."+weight_name, None)
            dim = lora_down_weight.size()[0]

            weights_loaded = (lora_down_weight is not None and lora_up_weight is not None)
        
        elif algo == "loha":
            # loha weight get
            if not "hada_w1_a" in key: continue
            block_down_name = key.split(".")[0]
            weight_name = key.split(".")[-1]
            loha_w1a_weight = value

            block_up_name = block_down_name
            loha_w1b_weight = state_dict.get(block_up_name + ".hada_w1_b", None)
            loha_w2a_weight = state_dict.get(block_up_name + ".hada_w2_a", None)
            loha_w2b_weight = state_dict.get(block_up_name + ".hada_w2_b", None)
            loha_hada_t1 = state_dict.get(block_up_name + ".hada_t1", None)
            loha_hada_t2 = state_dict.get(block_up_name + ".hada_t2", None)
            dim = loha_w1b_weight.shape[0]

            weights_loaded = (loha_w1a_weight is not None and loha_w1b_weight is not None and loha_w2a_weight is not None and loha_w2b_weight is not None)
        
        elif algo == "ia3":
            # ia3 weight get
            if not "weight" in key: continue
            block_down_name = key.split(".")[0]
            weight_name = key.split(".")[-1]
            lora_down_weight = value

            block_up_name = block_down_name
            lora_up_weight = state_dict.get(block_up_name + ".on_input", False)

            weights_loaded = (lora_down_weight is not None)

        elif algo == "lokr":
            # lokr weight get
            if not "lokr_w1_a." in key: continue
            block_down_name = key.split(".")[0]
            #weight_name = key.split(".")[-1]
            loha_w1a_weight = value

            block_up_name = block_down_name
            lokr_w1_weight = state_dict.get(block_up_name + ".lokr_w1", None)
            loha_w1b_weight = state_dict.get(block_up_name + ".lokr_w1_b", None)
            lokr_w2_weight = state_dict.get(block_up_name + ".lokr_w2", None)
            loha_w2a_weight = state_dict.get(block_up_name + ".lokr_w2_a", None)
            loha_w2b_weight = state_dict.get(block_up_name + ".hada_w2_b", None)
            loha_hada_t1 = state_dict.get(block_up_name + ".lokr_t1", None)
            loha_hada_t2 = state_dict.get(block_up_name + ".lokr_t2", None)
            if loha_w1b_weight:
                dim = loha_w1b_weight.shape[0]
            else:
                dim = loha_w2b_weight.shape[0]
            
            weights_loaded = (((loha_w1a_weight is not None and loha_w1b_weight is not None) or lokr_w1_weight is not None) and ((loha_w2a_weight is not None and loha_w2b_weight is not None) or lokr_w2_weight is not None))

        # common
        lora_alpha = state_dict.get(block_down_name + '.alpha', None)
        if not weights_loaded:
            print(f"can't loaded {block_down_name}")
            continue
    
        if lora_alpha is None:
            scale = 1.0
        else:
            scale = lora_alpha/dim
        
        if algo == "lora":
            conv2d = (len(lora_down_weight.size()) == 4)
            if conv2d:
                if lora_mid_weight is not None:
                    lora_down_weight = merge_conv(lora_mid_weight.transpose(0,1), lora_down_weight.transpose(0,1), my_device).transpose(0,1)
                full_weight_matrix = merge_conv(lora_down_weight, lora_up_weight, my_device)
            else:
                full_weight_matrix = merge_linear(lora_down_weight, lora_up_weight, my_device)
        elif algo == "loha":
            if loha_hada_t1 is not None:
                rebuild1 = cp_weight(loha_w1a_weight, loha_w1b_weight, loha_hada_t1)
            else:
                rebuild1 = loha_w1a_weight @ loha_w1b_weight
            if loha_hada_t2 is not None:
                rebuild2 = cp_weight(loha_w2a_weight, loha_w2b_weight, loha_hada_t2)
            else:
                rebuild2 = loha_w2a_weight @ loha_w2b_weight
            full_weight_matrix = rebuild1 * rebuild2
        elif algo == "ia3":
            if not lora_up_weight:
                full_weight_matrix = lora_down_weight.reshape(-1, 1)
            else:
                full_weight_matrix = lora_down_weight
        elif algo == "lokr":
            if loha_w1a_weight is not None and loha_w1b_weight is not None:
                if loha_hada_t1:
                    lokr_w1_weight = cp_weight(loha_w1a_weight, loha_w1b_weight, loha_hada_t1)
                else:
                    lokr_w1_weight = loha_w1a_weight @ loha_w1b_weight
            if loha_w2a_weight is not None and loha_w2b_weight is not None:
                if loha_hada_t2:
                    lokr_w2_weight = cp_weight(loha_w2a_weight, loha_w2b_weight, loha_hada_t2)
                else:
                    lokr_w2_weight = loha_w2a_weight @ loha_w2b_weight

            full_weight_matrix = torch.kron(lokr_w1_weight, lokr_w2_weight)
        
        new_key = block_down_name + ".weight"
        new_sd[new_key] = scale * full_weight_matrix 

        # common
        block_down_name = None
        block_up_name = None
        weights_loaded = False
        dim = None
        # lora to sd weight var
        lora_down_weight = None
        lora_up_weight = None
        # loha to sd weight var
        loha_w1a_weight = None
        loha_w1b_weight = None
        loha_w2a_weight = None
        loha_w2b_weight = None
        loha_hada_t1 = None
        loha_hada_t2 = None
        # lokr to sd weight var
        lokr_w1_weight = None
        lokr_w2_weight = None

    _new_sd = {}
    for key, value in new_sd.items():
        tokens = key.split('.')
        compvis_name = convert_diffusers_name_to_compvis(tokens[0])
        new_key = compvis_name + '.' + '.'.join(tokens[1:])

        if half_flag:
            _new_sd[new_key] = value.half()
        else:
            _new_sd[new_key] = value

    return _new_sd
def check_lora(state_dict):
    keys = state_dict.keys()
    flag = None
    for key in keys:
        if "first_stage_model" in key:
            flag = "sd"
        elif "lora" in key:
            if "lora_down" in key:
                flag="lora"
            elif "hada" in key:
                flag="loha"
            elif "lokr" in key:
                flag="lokr"
        if flag is not None: break
    if flag == None: flag = "ia3"
    if flag == "sd": return state_dict, None
    print("start lora key convert to comvis keys")
    return convet_lora_to_sd(state_dict, flag), flag

def check_keys(state_dict, global_keys):
    MAX_TOKENS = 77
    NUM_INPUT_BLOCKS = 12
    NUM_MID_BLOCK = 1
    NUM_OUTPUT_BLOCKS = 12
    NUM_TOTAL_BLOCKS = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + NUM_OUTPUT_BLOCKS
    NUM_TE_BLOCKS = 12
    keys = state_dict.keys()
    te_flag = [False] * (NUM_TE_BLOCKS + 1)
    unet_flags = [False] * NUM_TOTAL_BLOCKS
    block_name_flags = {k: False for k in global_keys} # elementなんとかマージ用の確認フラグは一応用意しておく
    for key in keys:
        # VAE部分のマージ処理は無駄というか行わないほうがいいくらいなので飛ばす
        if "first_stage_model" in key: continue
        if "cond_stage_model" in key and "embeddings" in key: continue
        if key in block_name_flags:
            block_name_flags[key] = True
        
        if "cond_stage_model" in key:
            if "embeddings" in key: continue
            te_flag[0] = True
            if "final_layer_norm" in key:
                te_flag[12] = True
            else:
                re_te = re.compile(r"\.encoder.layers\.(\d+)\.") #12
                m = re_te.search(key)
                weight_index = int(m.groups()[0])
                te_flag[weight_index+1] = True

        elif "model.diffusion_model." in key:
            weight_index = -1

            re_inp = re.compile(r"\.input_blocks\.(\d+)\.")  # 12
            re_mid = re.compile(r"\.middle_block\.(\d+)\.")  # 1
            re_out = re.compile(r"\.output_blocks\.(\d+)\.")  # 12

            if "time_embed" in key:
                weight_index = 0  # before input blocks
            elif ".out." in key:
                weight_index = NUM_TOTAL_BLOCKS - 1  # after output blocks
            elif m := re_inp.search(key):
                weight_index = int(m.groups()[0])
            elif re_mid.search(key):
                weight_index = NUM_INPUT_BLOCKS
            elif m := re_out.search(key):
                weight_index = NUM_INPUT_BLOCKS + NUM_MID_BLOCK + int(m.groups()[0])

            if weight_index >= NUM_TOTAL_BLOCKS:
                raise ValueError(f"illegal block index {key}")
            
            unet_flags[weight_index] = True
    return te_flag, unet_flags, block_name_flags

def culc_cosine(sim, theta_0, theta_1):
    sims = np.array([], dtype=np.float64)
    for key in (tqdm.tqdm(theta_0.keys(), desc="culc cosine")):
        # skip VAE model parameters to get better results
        if "first_stage_model" in key: continue
        if "model" in key and key in theta_1:
            simab = sim(theta_0[key].to(torch.float32), theta_1[key].to(torch.float32))
            dot_product = torch.dot(theta_0[key].view(-1).to(torch.float32), theta_1[key].view(-1).to(torch.float32))
            magnitude_similarity = dot_product / (torch.norm(theta_0[key].to(torch.float32)) * torch.norm(theta_1[key].to(torch.float32)))
            combined_similarity = (simab + magnitude_similarity) / 2.0
            sims = np.append(sims, combined_similarity.numpy())
    sims = sims[~np.isnan(sims)]
    sims = np.delete(sims, np.where(sims < np.percentile(sims, 1, method='midpoint')))
    sims = np.delete(sims, np.where(sims > np.percentile(sims, 99, method='midpoint')))
    return sims

'''
class Optimiser(sd_webui_bayesian_merger.optimiser.Optimiser):
    def __init__(self, url, batch_size, model_a, model_b, device,
                 payloads_dir, wildcards_dir, scorer_model_dir, init_points, n_iters, skip_position_ids,
                 best_format, best_precision, save_best, method):
        hack_score_get_model()
        if model_a=="" or model_a == None:
            print("model_aに指定するモデルを選択してください")
            model_a = filedialog_clicked(["ckpt", "safetensors"])
        if model_b=="" or model_b == None:
            print("model_bに指定するモデルを選択してください")
            model_b = filedialog_clicked(["ckpt", "safetensors"], os.path.split(model_a)[0])
        super().__init__(url, batch_size, model_a, model_b, device,
                         payloads_dir, wildcards_dir, scorer_model_dir, init_points, n_iters, skip_position_ids,
                         best_format, best_precision, save_best, method)

def hack_score_get_model():
    import glob
    import os
    def get_models(self):
        # TODO: let user pick model
        state_name = "sac+logos+ava1-l14-linearMSE.pth"
        file_list = [os.path.basename(f) for f in glob.glob(f"{self.model_dir}/*.pth")]
        if len(file_list)>=1+(state_name in file_list):
            print(f"{state_name}以外のScoreモデルが検出されました 使用するモデルファイルを番号で選択してください")
            for i, _f in enumerate(file_list):
                print(f"{i} | {_f}")
            _sel = input("使用するモデルを番号で選択してください: ")
            try:
                _sel = int(_sel)
                state_name = file_list[_sel]
            except:
                print(f"{_sel}の指定が正しくないため{state_name}を使用します")
            print(f"use model is {state_name}")
        if not sd_webui_bayesian_merger.scorer.Path(self.model_dir, state_name).is_file():
            print(
                "You do not have an aesthetic model ckpt, let me download that for you"
            )
            url = f"https://github.com/christophschuhmann/improved-aesthetic-predictor/blob/main/{state_name}?raw=true"
            r = sd_webui_bayesian_merger.scorer.requests.get(url)
            self.model_path = sd_webui_bayesian_merger.scorer.Path("./models", state_name).absolute()
            with open(self.model_path, "wb") as f:
                print(f"saved into {self.model_path}")
                f.write(r.content)
        else:
            self.model_path = sd_webui_bayesian_merger.scorer.Path(self.model_dir, state_name).absolute()
    
    try:
        sd_webui_bayesian_merger.scorer.Scorer.get_models = get_models
        print("success change get_models")
    except:
        print("get_modelsの置き換えに失敗しました")

    from common_modules import filedialog_clicked
    import os
    if kwargs["model_a"] == "" or kwargs["model_a"] == None:
        kwargs["model_a"] = filedialog_clicked(["ckpt", "safetensors"])
    if kwargs["model_b"] == "" or kwargs["model_b"] == None:
        kwargs["model_b"] = filedialog_clicked(["ckpt", "safetensors"], os.path.split(kwargs["model_a"])[0])


        
        ids_key = "cond_stage_model.transformer.text_model.embeddings.position_ids"
        fix_ids_list = []
        for i in range(77):
            fix_ids_list.append(i)
        fix_ids_list = torch.tensor([fix_ids_list], dtype=torch.int64)
        check_ids = []
        chekc_ids_from_theta = theta_0[ids_key].to(torch.int64)
        for i in range(77):
            if chekc_ids_from_theta[0,i]==i: continue
            check_ids.append(i)
        print(f"error ids: {check_ids}")
        del chekc_ids_from_theta
        print(f"pre shape:{theta_0[ids_key].size()}")
        theta_0[ids_key] = fix_ids_list
        print(f"fix shape:{theta_0[ids_key].size()}")

            show_progressbar=False,
     
    if "first_stage_model" in key: continue
    if "model" in key and key in theta_0:
        simab = sim(theta_0[key].to(torch.float32), theta_1[key].to(torch.float32))
        dot_product = torch.dot(theta_0[key].view(-1).to(torch.float32), theta_1[key].view(-1).to(torch.float32))
        magnitude_similarity = dot_product / (torch.norm(theta_0[key].to(torch.float32)) * torch.norm(theta_1[key].to(torch.float32)))
        combined_similarity = (simab + magnitude_similarity) / 2.0
        k = (combined_similarity - sims.min()) / (sims.max() - sims.min())
        k = k - current_alpha
        k = k.clip(min=.0,max=1.)
        caster(f"model A[{key}] +  {1-k} + * (model B)[{key}]*{k}",hear)
        theta_0[key] = theta_0[key] * (1 - k) + theta_1[key] * k
'''