import os
import glob
import tqdm
import argparse
import time
import shutil

import clip
import numpy as np
from PIL import Image, ImageFile

import pytorch_lightning as pl
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.nn.functional as F

from torch.utils.data import TensorDataset, DataLoader

import common_modules
import numpy as np
from score_module.clip_aethetic_score import MLP, get_image_preprocess, normalized, get_property

def train():
    parser = argparse.ArgumentParser(description= "train predictor")
    parser.add_argument("--version", type=int, default=0)
    parser.add_argument("--output", type=str, default="cas_model")
    parser.add_argument("--dataset", type=str, default=None)
    parser.add_argument("--batch_size", type=int, default=128)
    parser.add_argument("--eval_per", type=float, default=0.1)
    parser.add_argument("--epoch", type=int, default=50)
    parser.add_argument("--num_workers", type=int, default=0)
    parser.add_argument("--npz_disable", action="store_true")
    parser.add_argument("--npz_output", type=str, default=None)
    parser.add_argument("--resume", type=str, nargs="*", default=None)
    args = parser.parse_args()  

    ###############################################################
    img_dot = ["jpg", "png", "webp"]
    ###############################################################
    # 起動設定

    if args.dataset is None:
        print("学習に使用するデータセットフォルダを選択してください")
        args.dataset = common_modules.dirdialog_clicked()
    if not os.path.isdir(args.dataset):
        print(f"データセットフォルダの指定が正しくありません: {args.dataset}")
        return
    print(f"dataset dir is {args.dataset}")

    device = "cuda" if torch.cuda.is_available() else "cpu"
    scale_size, image_size, args.output, npy_dot, add_word = get_property(args.version, args.output)
    # load the training data 
    batch_size = args.batch_size

    clip_model, clip_preprocess = clip.load("ViT-L/14", device=device)  #RN50x64
    preprocess_size = clip_model.visual.input_resolution
    if args.version >= 1:
        image_preprocess = get_image_preprocess(preprocess_size, scale_size)

    base_dir_lsit = glob.glob(args.dataset + "/*")

    dir_list = glob.glob(args.dataset+"/*")
    print(dir_list)
    bucket_log = ""
    start = time.time()
    images_list = []
    scores_list = []
    count = {}
    for dir in dir_list:
        if not os.path.isdir(dir): continue
        score = float(os.path.split(dir)[-1])
        files = glob.glob(dir+"/*.png") + glob.glob(dir+"/*.jpg") + glob.glob(dir+"/*.jpeg") + glob.glob(dir+"/*.webp")
        if len(files)==0:
            print(f"{dir} 内に画像がありません")
            continue
        else:
            print(f"{dir} 内の画像読み込み中...")
        imgnum = 0
        for img_name in tqdm.tqdm(files):
            base_name = os.path.splitext(img_name)[0]
            npy_name = f"{base_name}{npy_dot}"
            get_img_flag = False
            if not args.npz_disable:
                if os.path.isfile(npy_name):
                    image = np.load(npy_name)
                    if image.shape[1] == 768 * image_size:
                        get_img_flag = True

            if not get_img_flag:
                raw_image = Image.open(img_name)
                pre_image = clip_preprocess(raw_image).unsqueeze(0).to(device)
                with torch.no_grad():
                    image_features = clip_model.encode_image(pre_image)
                if args.version >= 1:
                    _pre_image = image_preprocess(raw_image).unsqueeze(0).to(device)
                    for i in range(scale_size):
                        for j in range(scale_size):
                            with torch.no_grad():
                                image_features = torch.concat([image_features, clip_model.encode_image(_pre_image[:,:,i*preprocess_size:(i+1)*preprocess_size, j*preprocess_size:(j+1)*preprocess_size])], dim=1)
                image = normalized(image_features.cpu().detach().numpy())
                if not args.npz_disable:
                    np.save(npy_name, image)

            y_ = np.zeros((1))
            y_[0] = score
            images_list.append(image.squeeze(0))
            scores_list.append(y_)
            imgnum += 1
        bucket_log += f"score {score} nums: {imgnum}\n"
    
    if args.resume is not None:
        for resume_name in args.resume:
            if add_word != "":
                img_npz_name = f"{resume_name}_{add_word}_img.npy"
                score_npz_name = f"{resume_name}_{add_word}_score.npy"
            else:
                img_npz_name = f"{resume_name}_img.npy"
                score_npz_name = f"{resume_name}_score.npy"
            if os.path.exists(img_npz_name):
                images_list += np.load(img_npz_name).tolist()
                scores_list += np.load(score_npz_name).tolist()
                c = len(images_list)
                for i in range(c):
                    k = scores_list[i][0]
                    if str(k) in count:
                        count[str(k)] += 1
                    else:
                        count[str(k)] = 1
                print(f"load {resume_name}")
                for k in count.keys():
                    print(f"{k} : ({count[k]})")

    datalen = len(images_list)
    train_datalen = int(datalen * (1-args.eval_per))
    eval_datalen = int(datalen- train_datalen)
    print(f"total image num: {datalen} / train len: {train_datalen} / eval len: {eval_datalen}")
    print(f"add image score nums\n{bucket_log}")
    images_list = np.stack(images_list)
    scores_list = np.stack(scores_list)
    if args.npz_output is not None:
        print(f"save dataset {args.npz_output}_{add_word}")
        if add_word != "":
            np.save(f'{args.npz_output}_{add_word}_img.npy', images_list)
            np.save(f'{args.npz_output}_{add_word}_score.npy', scores_list)
        else:
            np.save(f'{args.npz_output}_img.npy', images_list)
            np.save(f'{args.npz_output}_score.npy', scores_list)
    images_list = torch.Tensor(images_list)
    scores_list = torch.Tensor(scores_list)

    _dataset = TensorDataset(images_list, scores_list)
    train_dataset, val_dataset = torch.utils.data.random_split(_dataset, [train_datalen, eval_datalen])

    num_workers = min(args.num_workers, os.cpu_count() - 1)  # cpu_count-1 ただし最大で指定された数まで
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True,  num_workers=num_workers) # create your dataloader
    val_loader = DataLoader(val_dataset, batch_size=args.batch_size, shuffle=True, num_workers=num_workers) # create your dataloader

    del clip_model

    # model / optimizer作成
    model = MLP(768*image_size)
    model.to(device)
    optimizer = torch.optim.Adam(model.parameters()) 

    # choose the loss you want to optimze for
    criterion = nn.MSELoss()
    criterion2 = nn.L1Loss()

    epochs = args.epoch

    model.train()
    best_loss =999
    best_l1_loss = 999
    best_epoch = 0
    best_output = ""
    max_l1_loss = 0
    save_name = f"{args.output}.pth"
    save_last_name = f"{args.output}_last.pth"

    for epoch in range(epochs):
        losses = []
        losses2 = []
        for batch_num, input_data in enumerate(tqdm.tqdm(train_loader)):
            optimizer.zero_grad()
            x, y = input_data
            x = x.to(device).float()
            y = y.to(device)

            output = model(x)
            loss = criterion(output, y)
            loss.backward()
            losses.append(loss.item())

            optimizer.step()

            if batch_num % 1000 == 0:
                print('\tEpoch %d | Batch %d | Loss %6.2f' % (epoch, batch_num, loss.item()))
                #print(y)

        print('Epoch %d | Loss %6.2f' % (epoch, sum(losses)/len(losses)))
        losses = []
        losses2 = []
        print_outputs = None
        print_scores = None
        
        for batch_num, input_data in enumerate(val_loader):
            optimizer.zero_grad()
            x, y = input_data
            x = x.to(device).float()
            y = y.to(device)

            output = model(x)
            loss = criterion(output, y)
            lossMAE = criterion2(output, y)
            #loss.backward()
            losses.append(loss.item())
            losses2.append(lossMAE.item())
            #optimizer.step()

            if batch_num % 1000 == 0:
                print('\tValidation - Epoch %d | Batch %d | MSE Loss %6.2f' % (epoch, batch_num, loss.item()))
                print('\tValidation - Epoch %d | Batch %d | MAE Loss %6.2f' % (epoch, batch_num, lossMAE.item()))
                
                #print(y)
            if print_outputs is None:
                output_len = min(y.size(0), 5)
                print_outputs = output[:output_len,0]
                print_scores = y[:output_len,0]
            else:
                if print_outputs.size(0) < 5:
                    output_len = min(y.size(0), 5-print_outputs.size(0))
                    print_outputs = torch.concat([print_outputs, output[:output_len,0]], dim=0)
                    print_scores = torch.concat([print_scores, y[:output_len,0]], dim=0)

        print('Validation - Epoch %d | MSE Loss %6.2f' % (epoch, sum(losses)/len(losses)))
        print('Validation - Epoch %d | MAE Loss %6.2f' % (epoch, sum(losses2)/len(losses2)))
        print(f"scores: {print_scores.data}")
        print(f"output: {print_outputs.data}")
        if sum(losses)/len(losses) < best_loss:
            print("Best MAE Val loss so far. Saving model")
            best_loss = sum(losses)/len(losses)
            best_l1_loss = sum(losses2)/len(losses2)
            best_output = f"score: {print_scores.data}\noutput: {print_outputs.data}\nmax loss: {max(losses2)}"
            print( f"loss: {best_loss} / l1 loss: {best_l1_loss} / max l1 loss: {max(losses2)}" ) 
            best_epoch = epoch

            torch.save(model.state_dict(), save_name)

    torch.save(model.state_dict(), save_last_name)

    print( f"best loss: {best_loss} \t savename: {save_name}" ) 
    print(f"[epoch:{best_epoch}]best loss: {best_loss} l1 loss: {best_l1_loss}")
    print(best_output)

    print("training done")
    # inferece test with dummy samples from the val set, sanity check
    print( "inferece test with dummy samples from the val set, sanity check")
    model.eval()
    output = model(x[:5].to(device))
    print(output.size())
    print(output)

if __name__ == "__main__":
    train()