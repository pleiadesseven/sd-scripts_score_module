import torch
import math
import warnings

class CustomCosineAnnealingLR(torch.optim.lr_scheduler.LRScheduler):
    def __init__(self, optimizer, T_max, U_max=0, eta_min=0, warmup=0, last_epoch=-1, verbose=False):
        self.T_max = T_max
        self.U_max = U_max if U_max > 0 else T_max
        self.Total_len = T_max + U_max
        self.eta_min = eta_min
        self.warmup = warmup
        super().__init__(optimizer, last_epoch, verbose)

    def get_lr(self):
        if self.warmup > self.last_epoch:
            return [base_lr * (self.last_epoch / self.warmup) for base_lr in self.base_lrs]

        target_epoch = (self.last_epoch-self.warmup) % self.Total_len
        target_len = self.T_max
        if target_epoch > self.T_max:
            target_len = self.U_max
            target_epoch = target_epoch - self.T_max +self.U_max
        if self.last_epoch == 0:
            return [group['lr'] for group in self.optimizer.param_groups]
        elif self._step_count == 1 and self.last_epoch > 0:
            return [self.eta_min + (base_lr - self.eta_min) *
                    (1 + math.cos((self.last_epoch) * math.pi / self.T_max)) / 2
                    for base_lr, group in
                    zip(self.base_lrs, self.optimizer.param_groups)]
        elif (target_epoch - 1 - target_len) % (2 * target_len) == 0:
            return [group['lr'] + (base_lr - self.eta_min) *
                    (1 - math.cos(math.pi / target_len)) / 2
                    for base_lr, group in
                    zip(self.base_lrs, self.optimizer.param_groups)]
        return [(1 + math.cos(math.pi * target_epoch / target_len)) /
                (1 + math.cos(math.pi * (target_epoch - 1) / target_len)) *
                (group['lr'] - self.eta_min) + self.eta_min
                for group in self.optimizer.param_groups]

    def _get_closed_form_lr(self):
        print(self.last_epoch)
        return [self.eta_min + (base_lr - self.eta_min) *
                (1 + math.cos(math.pi * self.last_epoch / (self.T_max + self.U_max))) / 2
                for base_lr in self.base_lrs]