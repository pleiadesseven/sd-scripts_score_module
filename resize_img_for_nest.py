'''
説明書

# sd-scriptsのvenv環境実行
venv\scrpts\activate

# 以下のコマンドのみで起動する
python resize_img_for_nest.py

resolution設定を指定したい場合
python resize_img_for_nest.py --resolution="704,704"

nest_dim設定を指定したい場合
python resize_img_for_nest.py --nest_dim="512,704"

キャプションファイルの拡張子を指定する場合(例：.txtと.caption両方使用する場合)
python resize_img_for_nest.py --txt_dot="txt,caption"

出力先を指定したい場合
python resize_img_for_nest.py --output="出力先に指定したいディレクトリ"

'''

# os.environ["CUDA_VISIBLE_DEVICES"] = "0"    # choose GPU if you are on a multi GPU server
import numpy as np
import math
# from datasets import load_dataset
import glob
import os
import shutil
import tqdm
import argparse
from PIL import Image, ImageFile
import cv2
from common_modules import dirdialog_clicked, filedialog_clicked

def create_dirs(dir_path):
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
        #print(f"create: {dir_path}")

parser = argparse.ArgumentParser(description="run")
parser.add_argument("--img_dir", type=str, default=None)
parser.add_argument("--resolution", type=str, default="512,512")
parser.add_argument("--nest_dim", type=str, default=None)
parser.add_argument("--output", type=str, default=None)
parser.add_argument("--txt_dot", type=str, default="txt")
args = parser.parse_args()

if args.img_dir is None:
    print("選別対象となるディレクトリを指定してください")
    args.img_dir = dirdialog_clicked()
img_dir = args.img_dir  # 自動分類対象のフォルダ

if args.output is None:
    print("outputが指定されていないためimg_dirで設定したディレクトリ下に出力フォルダを作成します")
    args.output = img_dir
if os.path.isabs(args.output):
    args.output = os.path.relpath(args.output)

txt_dots = [s for s in args.txt_dot.split(",")]

resolution = [int(s) for s in args.resolution.split(",")]
max_area = resolution[0] * resolution[1]
if args.nest_dim is None:
    nest_dim = [None, None]
    print("nest_dimが設定されていないためresolution設定から自動計算します")
    w = int(np.sqrt(max_area))
    _w = (w//4)//64*64
    nest_dim[0] = _w * 2
    nest_dim[1] = _w * 3
else:
    nest_dim = [int(s) for s in args.nest_dim.split(",")]

print(f"画像ディレクトリ：{img_dir}")
print(f"出力ディレクトリ：{args.output}")
print(f"キャプションファイルの拡張子リスト：{txt_dots}")
print(f"nest_dim=[{nest_dim[0]},{nest_dim[1]}]")
# This script will predict the aesthetic score for this image file:

dirs = ["small", "medium"]
output_dirs = []
for i in range(2):
    output_dirs.append(f"{os.path.relpath(os.path.join(args.output, dirs[i]))}")
    create_dirs(output_dirs[-1])

_img_path_list = glob.glob(os.path.join(img_dir, "**\*.png"), recursive=True) + glob.glob(os.path.join(img_dir, "**\*.jpg"), recursive=True)
img_path_list = []
for _path in _img_path_list:
    filename = os.path.splitext(_path)[0]
    for _check_dir in output_dirs:
        if _check_dir in filename:
            print(f"exit: {filename}")
    for dot in txt_dots:
        if os.path.isfile(filename + "." + dot):
            img_path_list.append(_path)
            break
print(f"target filecount: {len(img_path_list)}")


def load_img(img_path):
    image = Image.open(img_path)
    if not image.mode == "RGB":
        image=image.convert("RGB")
    return np.array(image, np.uint8)

def create_resize(img, resized_area, aspect_ratio, i=None):
    _resized_w = math.sqrt(resized_area * aspect_ratio)
    _resized_h = resized_area / _resized_w
    _resized_w = int(_resized_w)
    _resized_h = int(_resized_h)
    assert abs(_resized_w / _resized_h - aspect_ratio) < 1e-2, "aspect is illegal"
    return cv2.resize(img, (_resized_w, _resized_h), interpolation=cv2.INTER_AREA)       # INTER_AREAでやりたいのでcv2でリサイズ

resized_areas = [nest_dim[0] * nest_dim[0], nest_dim[1] * nest_dim[1]]
for img_path in tqdm.tqdm(img_path_list):
    image_org = load_img(img_path)
    img_dir = os.path.relpath(os.path.split(img_path)[0])[len(args.output):]
    img_name = os.path.split(img_path)[-1]
    img_base_path = os.path.splitext(img_path)[0]
    if not type(image_org)==np.ndarray:continue
    org_size = image_org.shape[0:2]
    org_area = org_size[0] * org_size[1]
    aspect_ratio = org_size[1] / org_size[0]

    for i in range(2):
        _output_dir = f"{output_dirs[i]}{img_dir}"
        output_path = os.path.join(_output_dir, img_name)
        output_base_path = os.path.splitext(output_path)[0]
        create_dirs(_output_dir)
        if org_area <= resized_areas[i]:
            #print(org_area, resized_areas[i])
            continue
        image_new = create_resize(image_org, resized_areas[i], aspect_ratio, i)
        Image.fromarray(image_new).save(output_path)

        for dot in txt_dots:
            txt_path = f"{output_base_path}.{dot}"
            org_txt_path = f"{img_base_path}.{dot}"
            if os.path.isfile(org_txt_path):
                shutil.copyfile(org_txt_path, txt_path)
